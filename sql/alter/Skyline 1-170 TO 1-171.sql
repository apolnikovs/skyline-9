# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.170');

# ---------------------------------------------------------------------- #
# Add table "status_permission"                                                  #
# ---------------------------------------------------------------------- #
CREATE TABLE `status_permission` (
	`StatusPermissionID` INT(11) NOT NULL AUTO_INCREMENT,
	`PermissionType` ENUM('user','role','network','client','brand','branch') NOT NULL,
	`EntityID` INT(11) NOT NULL,
	`StatusID` INT(11) NOT NULL,
	PRIMARY KEY (`StatusPermissionID`),
	INDEX `status_permission_TO_status` (`StatusID`),
	CONSTRAINT `status_permission_TO_status` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.171');
