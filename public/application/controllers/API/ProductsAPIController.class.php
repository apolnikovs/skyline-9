<?php
/**
 * Settings.class.php
 * 
 * Implementatrion of Products API for SkyLine
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.0
 */

require_once('CustomRESTController.class.php');
include_once(APPLICATION_PATH.'/controllers/API/SkylineAPI.class.php');

class ProductsAPI extends SkylineAPI {
   
   
    
    /**
     * Description
     * 
     * Deal with get  
     * 
     * @param array $args   Associative array of arguments passed
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     */
    
    public function get( $args ) {
        $api_products_model = $this->loadModel('APIProducts');
                            
        /* Check if Product set */
        if (isset($args['ProductNumber'])) {
            /* Yes - send the response back which relates to the Client with the
             *  same ClientAccountNo. */ 
            $rec_products = $api_products_model->getProductDetails($this->user->UserID, $args['ProductNumber']);
            
        } else {
            $this->sendResponse(406);
            exit();
        }
        
        $response = array(
                         'Product' =>  $rec_products
                         );
        
        if ( count($rec_products) > 0 ) {
            $response['Code'] = "SC0001";                                       /* Record found */
            $response['Response'] = "Success";
        } else {
            $response['Code'] = "SC0003";
            $response['Response'] = "Product Not Found";
        }
        $this->sendResponse(200, $response);
    }
    
    public function put( $args ) {
        
        $this->sendResponse(501);
                
    }
    
    public function post( $args ) {
        
        $this->sendResponse(501);
                
    }
    
    public function delete( $args ) {
        
        $this->sendResponse(501);
        
        
    }
}

?>