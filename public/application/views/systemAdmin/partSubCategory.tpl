{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $PartSubCategoryPage}
    {$fullscreen=true}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
{/block}

{block name=scripts}


   
 <script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
    <script type="text/javascript">
         var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
                 
    
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    
    
    function showTablePreferences(){
$.colorbox({ 
 
                       href:"{$_subdomain}/LookupTables/tableDisplayPreferenceSetup/page=partCategories/table=service_provider_part_sub_category",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}
      
 $(document).ready(function() {

 var oTable = $('#PartSubCategoryResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',

"bServerSide": true,

		fnRowCallback:  function(nRow, aData){
               
                
                },
    "sAjaxSource": "{$_subdomain}/LookupTables/loadPartSubCategoryTable",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#PartSubCategoryResults').show();
                               
			} );
                        },
                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 25, 50, 100 , -1], [25, 50, 100, "All"]],
"iDisplayLength" : 25,
 "aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            { "bVisible":{$vis} },
			 
                           {/for} 
                               { "bVisible":1,"bSortable":false }
                               
                            
		] 
   
 
        
          
});//datatable end
  
   /* Add a click handler to the rows - this could be used as a callback */
	$("#PartSubCategoryResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});

 /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



    /* Add a click handler for the edit row */
	$('button[id^=edit]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
        console.log(anSelected);
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                           href:"{$_subdomain}/LookupTables/processPartSubCategory/id="+anSelected[0].id,
                        title: "Edit PartSubCategory",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );                            
             
 /* Add a click handler for the delete row */
	$('button[id^=delete]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
if (confirm('Are you sure you want to delete this entry from database?')) {
    window.location="{$_subdomain}/LookupTables/deletePartSubCategory/id="+anSelected[0].id
} else {
    // Do nothing!
}


    }else{
    alert("Please select row first");
    }
		
	} );                            
             

/* Add a dblclick handler to the rows - this could be used as a callback */
	$("#PartSubCategoryResults  tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processPartSubCategory/id="+anSelected[0].id,
                        title: "Edit Part Fault Codes",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );  




      
        $('#manufacturerFilter').combobox();
        $('#serviceProviderSelect').combobox({
        selected: function( event, ui ) {
   
    $('#serviceProviderSelect').trigger("change");                            
}
        });

$('#serviceProviderSelect, #serviceProviderSelectCombo').change( function() {
        
        if($(this).val()!="0"){
                 $('#tt-Loader').show();
                                $('#PartSubCategoryResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadPartSubCategoryTable/supplierid="+$(this).val()+"/");
                 }else{
                  $('#tt-Loader').show();
                                $('#PartSubCategoryResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadPartSubCategoryTable/");
                 }
	} );  
$('input[id^=inactivetick]').click( function() {
         $('#unaprovedtick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadPartSubCategoryTable/inactive=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadPartSubCategoryTable/");
                 }
	} );  

    });//doc ready end
function PartSubCategoryInsert()
{
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processPartSubCategory/",
                       
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}
function PartSubCategoryEdit()
{
    var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); 
               
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processPartSubCategory/id="+aData,
                        title: "Edit Part Fault Codes",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}


                    
                    
                
                
             
function filterTable(name){
$('input[type="text"]','#PartSubCategoryResults_filter').val(name);
e = jQuery.Event("keyup");
e.which = 13;
$('input[type="text"]','#PartSubCategoryResults_filter').trigger(e);


}

    </script>

    
{/block}


{block name=body}
<div style="float:right">
         <a href="#" onclick="showTablePreferences();">Display Preferences</a>
            </div>
    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >Lookup Tables</a> {if isset($backtomodel)}/<a href="{$_subdomain}SystemAdmin/index/lookupTables/models" > Models</a> {/if}/ Part Sub Categories

        </div>
    </div>



    <div class="main" id="home" style="width:100%">

               <div class="ServiceAdminTopPanel" >
                    <form id="PartSubCategoryTopForm" name="PartSubCategoryTopForm" method="post"  action="#" class="inline">
                         
                        <fieldset>
                        <legend title="" >Part Sub-Categories</legend>
                        <p>
                            <label>Part Categories are a means for users to group parts in Stock Control.</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                  <div class="ServiceAdminResultsPanel" id="PartSubCategoryResultsPanel" >
                    
                  
                 
                     <div style="text-align:center" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                      <p> <button onclick="window.location='{$_subdomain}/LookupTables/partCategories'" type="button" id="partCategoryLink"  class="gplus-blue">Part Categories</button></p>
                      {if isset($splist)}
                          
                      <p>
                        <select id="serviceProviderSelect" >
                            <option value="0">Select Service Provider</option>
                        {foreach $splist as $s}
                            <option value="{$s.ServiceProviderID}">{$s.Acronym}</option>
                        {/foreach}
                        </select>
                        </p>
                        {/if}
                        
                    <form method="POST" action="" id="PartSubCategoryResultsForm" class="dataTableCorrections">
                        <table style="display:none" id="PartSubCategoryResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            
                                        {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                           
                                <th style="width:10px;text-align: center"><input type="checkbox"></th>
                                    </tr>
                            </thead>
                            <tbody>
                              
                            
                            </tbody>
                        </table>  
                           
                           
                     </form>
                </div>        

                  <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:200px">
                            <button type="button" id="edit"  class="gplus-blue">Edit</button>
                            <button type="button" onclick="PartSubCategoryInsert()" class="gplus-blue">Insert</button>
                            <button type="button" id="delete" class="gplus-red">Delete</button>
                    </div>
                </div>  
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               

            <div style="width:100%;text-align: right">
                        <input id="inactivetick"  type="checkbox" > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                   
                    </div>
                 
                 

<hr>
                <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/lookupTables'">Finish</button>
    
    </div>
                 



{/block}



