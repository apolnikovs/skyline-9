<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    
    {* #################################################################
     # 
     #  Define Global Varables
     #
     ################################################################### *}
     
        {$HomePage = 0}
        {$OutstandingJobsPage = 1}
        {$OverdueJobsPage = 2}
        {$PerformancePage = 3}
        {$LinksPage = 4}
        {$HelpPage = 5}
        {$ErrorPage = 6}
        
        {$Title = "Argos"}
        {$PageTitle = ""}
        
        {block name=config}
        {/block} 
        
    {* ###################################################################
     #  SMARTY Functions:
     # 
     #   name:  menu
     #   args:  $menu_items
     #
     ##################################################################### *}
     
    {function menu}
    {strip}
         {foreach $menu_items as $item}
             <button type="button">
                 <span class="label"><a href="{$_subdomain}{$item[2]}">{$item[1]}</a></span>
             </button>
         {/foreach}   
    {/strip}
    {/function}
       
    {* #################################################################### *}
   
    <head>
       
        <title>{$Title|escape:"html"}</title>
        
        {* #################################################################
         #
         # Add your meta tags here  
         #
         ################################################################### *}
        <meta name="title" content="" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="language" content="en" />
        <meta name="author" content="BookArt" />        
        
        <link rel="stylesheet" href="{$_subdomain}/css/styles.php?skin=default&size=950" type="text/css" media="screen" charset="utf-8" />                      
        
        {* printing styles *}
        <link rel="stylesheet" href="{$_subdomain}/css/lessblue/print.css" type="text/css" media="print" charset="utf-8" />  
        
        <link rel="stylesheet" href="{$_subdomain}/css/jquery_themes/base/jquery.ui.all.css" type="text/css" media="print" charset="utf-8" />  
        
       
        
        
        {* IE only styles *}
	<!--[if lte IE 9]><link rel="stylesheet" href="{$_subdomain}/css/lessblue/ie.css" type="text/css" media="screen" /><![endif]-->

        {* include the jquery, jquery ui packages *}
	{* <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script> *}
        
       
        {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
        {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
        {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
        {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.datatables.min.js"></script>
        {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
        {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.ui.datepicker.js"></script>
    
        
        
    </head>
    <body>

        <div class="container">
            
            <div id="header">
                <p>
                    <img src="/images/argos_logo.jpg" alt="" id="logo" class="top left"/>
                    <h1>After Sales Support Toolkit</h1>
                    <h2>{$PageTitle|escape:"html"}</h2>
                </p>
            </div> 
            
            <div id="menu">
                {menu menu_items = [[$OutstandingJobsPage, 'Outstanding&nbsp;Jobs','/index/outstandingjobs'],
                                    [$OverdueJobsPage,'Overdue&nbsp;Jobs','/index/overduejobs'],
                                    [$PerformancePage,'Performance','/index/performance'],
                                    [$LinksPage,'Links','/index/links'],
                                    [$HelpPage,'Help/Legal','/index/help'],
                                    [$HomePage,'Sign&nbsp;In','/index/index'],
				    [$DiaryInterface,'Diary Interface','/Diary/default',''],
				    ]}

            </div>
            
            {block name=body}{/block} 
           
        </div>
        
    </body>
                
</html>
