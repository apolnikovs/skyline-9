# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.253');

# ---------------------------------------------------------------------- #
# Add Table inbound_fault_code                                           #
# ---------------------------------------------------------------------- # 
CREATE TABLE `inbound_fault_code` (
	`InboundFaultCodeID` INT(11) NOT NULL AUTO_INCREMENT,
	`NetworkID` INT(11) NOT NULL,
	`Code` CHAR(4) NOT NULL,
	`Description` VARCHAR(250) NULL DEFAULT NULL,
	`Comment` VARCHAR(250) NULL DEFAULT NULL,
	`CreatedByUserID` INT(11) NULL DEFAULT NULL,
	`CreatedDate` TIMESTAMP NULL DEFAULT NULL,
	`ModifiedUserID` INT(11) NULL DEFAULT NULL,
	`ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`InboundFaultCodeID`),
	INDEX `user_TO_inbound_fault_code_Mod` (`ModifiedUserID`),
	INDEX `user_TO_inbound_fault_code_Create` (`CreatedByUserID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.254');
