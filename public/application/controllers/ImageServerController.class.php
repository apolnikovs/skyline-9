<?php

/**
 * Short Description of ImageServerController.
 * 
 * Long description of ImageServerController.
 *
 * @version    1.0
 */

require_once('CustomController.class.php');

class ImageServerController extends CustomController {
    
    
    public function __construct() { 
        parent::__construct(); 
        
        // set IS_IMAGE_SEVER so that index.php does NOT try to append
        // page performace data !!!!!
        $GLOBALS['IS_IMAGE_SERVER'] = true;
        
    }
    
    public function indexAction( $args ) {
               
        $url = implode('/',$args);
        
        $url = str_replace(" ","%20",$url); // to properly format the url 
        
        $mime = $this->getMimeType($url);
        if ($mime === false) {
            throw new Exception('Invalid file Type: '.$url);
        }
        
        $fileContents = $this->getRemoteFile($url);
        
        $this->log($fileContents);
        
        header('Content-type: '.$mime);
        echo $fileContents;

    }
    
    private function getRemoteFile($url) {
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_BINARYTRANSFER, 1);
        //curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 0);;     
        $fileContents = curl_exec($ch);
        if(curl_exec($ch) === false) {
            throw new Exception(curl_error($ch));
        }
        curl_close($ch);
        return $fileContents;
    }
    
    private function getMimeType($url) {
        $ext = substr($url, -3);
        switch ($ext) {
            case 'jpg':
            $mime = 'image/jpeg';
            break;
        case 'gif':
            $mime = 'image/gif';
            break;
        case 'png':
            $mime = 'image/png';
            break;
        default:
            $mime = false;
        }
        return $mime;
    }
 
}
