<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Colours Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0
 */

class Colours extends CustomModel {
    
   
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
   ////colour functions 
    public function getColourList($utID=false){
       
            $sql="select * from colour where Status='Active'";
     
        $res=$this->query( $this->conn, $sql); 
        return $res;
    }
    
    
    public function insertColour($p){
        $sql="insert into colour (ColourName)
    values
    (:ColourName)
    ";
        $params=array(
            'ColourName'=>$p['ColourName']
        
            
        );
       $this->execute( $this->conn, $sql,$params);  
    }
    
    public function updateColour($p){
          $sql="update colour set ColourName=:ColourName, Status=:Status
    where ColourID=:ColourID;
    ";
        $params=array(
            'ColourName'=>$p['ColourName'],
            'Status'=>$p['Status'],
            
          
            'ColourID'=>$p['ColourID']
        );
       $this->execute( $this->conn, $sql,$params); 
    }
    
    public function getData($id,$table){
       ($table=="colour")?$idcol="ColourID":$idcol="ServiceProviderColourID";
       $sql="select * from $table where $idcol=$id";
       $res=$this->Query($this->conn, $sql);
    
     if(isset($res[0])){
         return $res[0];
     }else{
         false;
     }
   }
  
    public function deleteColour($id){
        $sql="update colour set Status='In-Active' where ColourID=$id";
        $this->execute( $this->conn, $sql); 
    }
    
     public function getIDFromMain($spid){
      $sql="select ServiceProviderID from service_provider_colour ss where ss.ColourID=$spid";
      $res=$this->Query($this->conn, $sql);
    
     if(isset($res[0])){
         return $res[0]['ServiceProviderID'];
     }else{
         false;
     }
  }  
    
    
    ////colour functions 
    
  
   public function getAllSPColours($SpID){
         $sql="select * from service_provider_colour where Status='Active' and ServiceProviderID=$SpID";
           $result = $this->Query($this->conn, $sql);
           return $result;
     }
     
     
     public function saveColour($p,$table,$secondaryTable=false){
         $extraCol="";
         $extraToken="";
         $extraParam=array();
        
          if(!isset($p['Status'])){$p['Status']="Active";}
         
         ($table=="colour")?$idcol="ColourID":$idcol="ServiceProviderColourID";
       
          $fields=[
    "ColourName",
 "Status"  ,  
       ];
         
         
     
     
     
     
         if($p['mode']=="insert" ||$p['mode']=="copyNew"){
             
             
             if($p['mode']=="copyNew"){
                
                 $extraCol=",ServiceProviderID,ColourID";
                 $extraToken=",:ServiceProviderID,:ColourID";
                 $extraParam= $params=array(
     "ServiceProviderID" =>$p["ServiceProviderID"], 
     "ColourID" =>$p["ColourID"], 
                     );
             }
       //      "ManufacturerID",
    //"",
            
             
         $sql="insert into $table 
        (
    ";
         for($i=0;$i<sizeof($fields);$i++){
             $sql.=" $fields[$i]";
            if($i!=  sizeof($fields)-1){
                $sql.=",";
            }
         }
    $sql.="$extraCol";
        
    $sql.=") values(";
    
    for($i=0;$i<sizeof($fields);$i++){
             $sql.=":$fields[$i]";
            if($i!=  sizeof($fields)-1){
                $sql.=",";
            }
         }
         
    $sql.="$extraToken";
     $sql.= ")"; 

         
         
         }
        //this is used when new entry is created it must be insertedfirst in main table, then secondary
         if($p['mode']=="New"){
            
              $extraCol=",ApproveStatus";
                 $extraToken=",:ApproveStatus";
                 $extraParam= $params=array(
     "ApproveStatus" =>"Pending", 
     
                     );
               $sql="insert into $secondaryTable 
        (";
               $fields2=$fields;
               
       for($i=0;$i<sizeof($fields);$i++){
             $sql.=" $fields[$i]";
            if($i!=  sizeof($fields)-1){
                $sql.=",";
            }
         }
   
    $sql.=$extraCol;
        
     $sql.=") 
        values
         (
         ";
        for($i=0;$i<sizeof($fields);$i++){
             $sql.=":$fields[$i]";
            if($i!=  sizeof($fields)-1){
                $sql.=",";
            }
         }
         
   
          $sql.=   $extraToken;
      $sql.="  ) 
"; 
               
               
               
      $params=array(
    "ColourName" => $p['ColourName'],
    "Status" => $p['Status'],
   
          );
      
    $params1=  array_merge($params,$extraParam);
    
   
             $this->Execute($this->conn, $sql,$params1);  
             
          $fields=$fields2;   
           $mainID=$this->conn->lastInsertId();   
            $extraCol=",ServiceProviderID,ColourID";
                 $extraToken=",:ServiceProviderID,:ColourID";
                 $extraParam=array(
     "ServiceProviderID" =>$p["ServiceProviderID"], 
     "ColourID" =>$mainID, 
                     );
                 
              $sql="insert into $table 
        (
    ";
         for($i=0;$i<sizeof($fields);$i++){
             $sql.=" $fields[$i]";
            if($i!=  sizeof($fields)-1){
                $sql.=",";
            }
         }
    $sql.="$extraCol";
        
    $sql.=") values(";
    
    for($i=0;$i<sizeof($fields);$i++){
             $sql.=":$fields[$i]";
            if($i!=  sizeof($fields)-1){
                $sql.=",";
            }
         }
    $sql.="$extraToken";
     $sql.= ")"; 
              
              
            
              $params=  array_merge($params,$extraParam);
             
        
           $this->Execute($this->conn, $sql,$params);       
              
         }
        
          
             
             
         
      if($p['mode']=="update"){
    
          $id=$p['ColourID'];
          
          
          if(isset($p['ApproveStatus'])){
              $st=$p['ApproveStatus'];
              $ApproveStatus=",ApproveStatus='$st'";
          }else{
              $ApproveStatus="";
          }
          $sql="
             update $table set";
             
            for($i=0;$i<sizeof($fields);$i++){
             $sql.=" $fields[$i]=:$fields[$i]";
            if($i!=  sizeof($fields)-1){
                $sql.=",";
            }
         }
         
   $sql.=" $ApproveStatus
        where $idcol=$id

            ";
      }
      
//      "
//    "UnitTypeID" => $p['UnitTypeID'],
      
      
    $params=array(
    "ColourName" => $p['ColourName'],
    "Status" => $p['Status'],
   
          );
    
    
      if($p['mode']!="New"){
           
          $params=  array_merge($params,$extraParam);
echo $sql;
          $this->Execute($this->conn, $sql,$params);  
      }
     }
    
    
    
    
}
?>