{extends "Performance/MasterLayout.tpl"}

{block name=scripts append}


<script type="text/javascript">
    
google.load('visualization', '1');
function drawPanels() {
	var h = $('#container').height()-$('#menu-panel').height()-25;
	var chart_height = h-$('#gauge-panel').height()-5;
    $('#left-panel').css("height",h);
    $('#right-panel').css("height",h);
    $('#chart-panel').css("height",chart_height);
    $('#graph').css("height",chart_height);
    $('#legend').css("height",chart_height-10);
    $('.overflow').css("height",h-40);
}
function drawVisualization() {
    var wrapper = new google.visualization.ChartWrapper({
    	'chartType': '{$graphtype}',
        'dataTable': [   
        {if $graphtype == 'ColumnChart'} 
            ['',{foreach $graph as $graphdata}"{$graphdata.Name|lower|capitalize}"{if not $graphdata@last},{/if}{/foreach}],
                   ['',{foreach $graph as $graphdata}{$graphdata.Count}{if not $graphdata@last},{/if}{/foreach}]
        {elseif $graphtype == 'PieChart'} ['',0],
            {foreach $graph as $graphdata}
                ["{$graphdata.Name|lower|capitalize}", {$graphdata.Count} ]{if not $graphdata@last},{/if}
            {/foreach}    
        {elseif $graphtype == 'Table'}
            ['{$graphtitle|replace:' by ':''}','Count'],
            {foreach $graph as $graphdata}
                ["{$graphdata.Name|lower|capitalize}", {$graphdata.Count} ]{if not $graphdata@last},{/if}
            {/foreach} 
        {/if}],
        'options': { 'title': "{$tab|replace:'Jobs':' Jobs'}{$graphtitle}{if $sp_name != ""} for {$sp_name|lower|capitalize|replace:'Tv ':'TV '}{/if}",
        {if isset($graphcolour) }
            colors: [  
                {foreach $graphcolour key=k item=i}
                    '#{$i}' {if not $i@last},{/if}
                {/foreach}    
            ],
        {/if} 
                  'backgroundColor': 'transparent',
                  'colour':'#1F5676',                  
                  'chartArea': { 'left':10,'top':10, 'width': '78%', 'height': '85%'},
                  'legend': { 'position': 'none', 'textStyle': { 'color': '#1F5676', 'fontSize': 10 } },
                  'titlePosition': 'in', 
                  'axisTitlesPosition': 'in',
                  'hAxis': { 'textPosition': 'in' }, 
                  'vAxis': { 'textPosition': 'in' }
                {if $graphtype == 'Table'}, width:'50%'{/if}
          },
          'containerId': 'graph'
        });
    wrapper.draw();
    
    var wrapperlarge = new google.visualization.ChartWrapper({
    	'chartType': '{$graphtype}',
        'dataTable': [   
        {if $graphtype == 'ColumnChart'}  
        ['',{foreach $graph as $graphdata}"{$graphdata.Name|lower|capitalize}"{if not $graphdata@last},{/if}{/foreach}],
                   ['',{foreach $graph as $graphdata}{$graphdata.Count}{if not $graphdata@last},{/if}{/foreach}]
        {elseif $graphtype == 'PieChart'} ['',0],
            {foreach $graph as $graphdata}
                ["{$graphdata.Name|lower|capitalize}", {$graphdata.Count} ]{if not $graphdata@last},{/if}
            {/foreach} 
        {elseif $graphtype == 'Table'}
            ['{$graphtitle|replace:' by ':''}','Count'],
            {foreach $graph as $graphdata}
                ["{$graphdata.Name|lower|capitalize}", {$graphdata.Count} ]{if not $graphdata@last},{/if}
            {/foreach}                 
        {/if}],
        'options': { 'title': "{$tab|replace:'Jobs':' Jobs'}{$graphtitle}{if $sp_name != ""} for {$sp_name|lower|capitalize|replace:'Tv ':'TV '}{/if}",
                {if isset($graphcolour) }
                    colors: [  
                        {foreach $graphcolour key=k item=i}
                            '#{$i}' {if not $i@last},{/if}
                        {/foreach}    
                    ],
                {/if}
                  'backgroundColor': 'transparent',
                  'colour':'#1F5676',
                  'chartArea': { 'left':10,'top':50, 'width': '95%', 'height': '90%'},
                  'legend': { 'position': 'left', 'textStyle': { 'color': '#1F5676', 'fontSize': 10 } },
                  'titlePosition': 'out', 
                  'axisTitlesPosition': 'in',
                  'hAxis': { 'textPosition': 'in' }, 
                  'vAxis': { 'textPosition': 'in' },
                  'width' : 800,
                  'height' : 500
          },
          'containerId': 'enlargegraph'
        });
    wrapperlarge.draw();
}
$(document).ready(function(){
	drawPanels();
	
	// Instantiate our gauge object.
    var gauge = new pccs.Gauge('gauge');
    var gauge1 = new pccs.Gauge('gauge1');
    var gauge2 = new pccs.Gauge('gauge2');
    var gauge3 = new pccs.Gauge('gauge3');
    var gauge4 = new pccs.Gauge('gauge4');
    var gauge5 = new pccs.Gauge('gauge5');
        
    // set up gauge options
    var gauge_options = { scale_start: {$gauge0.min},
                           scale_inc: {$gauge0.inc},
                           scale_divisions: {$gauge0.div},
                           red_start: {$gauge0.red},
                           red_end: {$gauge0.inc*$gauge1.div},
                           amber_start: {$gauge0.yellow},
                           amber_end: {$gauge0.red},
                          show_minor_ticks: false };
    var gauge1_options = { scale_start: {$gauge1.min},
                           scale_inc: {$gauge1.inc},
                           scale_divisions: {$gauge1.div},
                           red_start: {$gauge1.red},
                           red_end: {$gauge1.inc*$gauge1.div},
                           amber_start: {$gauge1.yellow},
                           amber_end: {$gauge1.red},
                           show_minor_ticks: false };                  
    var gauge2_options = { scale_start: {$gauge2.min},
                           scale_inc: {$gauge2.inc},
                           scale_divisions: {$gauge2.div},
                           red_start: {$gauge2.red},
                           red_end: {$gauge2.inc*$gauge2.div},
                           amber_start: {$gauge2.yellow},
                           amber_end: {$gauge2.red},
                           show_minor_ticks: false };                          
    var gauge3_options = { scale_start: {$gauge3.min},
                           scale_inc: {$gauge3.inc},
                           scale_divisions: {$gauge3.div},
                           red_start: {$gauge3.red},
                           red_end: {$gauge3.inc*$gauge3.div},
                           amber_start: {$gauge3.yellow},
                           amber_end: {$gauge3.red},
                               show_minor_ticks: false };   
    var gauge4_options = { scale_start: {$gauge4.min},
                           scale_inc: {$gauge4.inc},
                           scale_divisions: {$gauge4.div},
                           red_start: {$gauge4.red},
                           red_end: {$gauge4.inc*$gauge4.div},
                           amber_start: {$gauge4.yellow},
                           amber_end: {$gauge4.red},
                               show_minor_ticks: false }; 
    var gauge5_options = { scale_start: {$gauge5.min},
                           scale_inc: {$gauge5.inc},
                           scale_divisions: {$gauge5.div},
                           red_start: {$gauge5.red},
                           red_end: {$gauge5.inc*$gauge5.div},
                           amber_start: {$gauge5.yellow},
                           amber_end: {$gauge5.red},
                               show_minor_ticks: false };
                               
    // Draw our gauge.
    gauge.draw({$gauge0.value}, gauge_options );
    gauge1.draw({$gauge1.value}, gauge1_options );
    gauge2.draw({$gauge2.value}, gauge2_options );
    gauge3.draw({$gauge3.value}, gauge3_options );
    gauge4.draw({$gauge4.value}, gauge4_options );
    gauge5.draw({$gauge5.value}, gauge5_options );
                               
    $("#datefrom").datepicker({ 
        dateFormat: 'dd/mm/yy', 
        showOn: "button",
        buttonImage: "{$_subdomain}/css/Skins/skyline/images/date-picker.gif",
        buttonImageOnly: true ,
        altField: "#altdatefrom",
        altFormat: "yy-mm-dd"
    });
    $("#altdatefrom").val('{$datefrom|date_format:'%Y-%m-%d'}');
    $("#dateto").datepicker({ 
        dateFormat: 'dd/mm/yy', 
        showOn: "button",
        buttonImage: "{$_subdomain}/css/Skins/skyline/images/date-picker.gif",
        buttonImageOnly: true ,
        altField: "#altdateto",
        altFormat: "yy-mm-dd"
    });
    $("#altdateto").val('{$dateto|date_format:'%Y-%m-%d'}');       
                               
    drawVisualization();
	    
    //navigation bar
    $('#myTab a').click(function (e) {
    		e.preventDefault();
    		$(this).tab('show');
    	});
    
    // scrolling plugin
    //$('#myTab').scrollspy();
    
                            $("#jncClientID").attr('disabled', '');
                            $("#jncBranchID").attr('disabled', '');

                            $(document).on('click', '#filter_btn', 
                                function() {

                                    $('#NetworkClientForm').validate({


                                                rules:  {
                                                            NetworkID:
                                                            {
                                                                    required: true

                                                            },
                                                            ClientID:
                                                            {
                                                                required: true
                                                            },
                                                            BranchID:
                                                            {
                                                                required: true
                                                            },
                                                            JobTypeID:
                                                            {
                                                                required: true
                                                            }

                                                },
                                                messages: {
                                                            NetworkID:
                                                            {
                                                                    required: "Please select Network."
                                                            },
                                                            ClientID:
                                                            {
                                                                    required: "Please select Client."
                                                            },
                                                            BranchID:
                                                            {
                                                                    required: "Please select Branch."
                                                            },
                                                            JobTypeID:
                                                            {
                                                                    required: "Please select Job Type."
                                                            }
                                                },

                                                errorPlacement: function(error, element) {

                                                        error.insertAfter( element );

                                                },
                                                errorClass: 'fieldError',
                                                onkeyup: false,
                                                onblur: false,
                                                errorElement: 'label'



                                                });




                                });





                            /* Add a change handler to the network dropdown - strats here*/
                            $(document).on('change', '#jncNetworkID', 
                                function() {

                                    $clientDropDownList  = '<option value="">Select from drop down</option>';

                                        var $NetworkID = $("#jncNetworkID").val();
                                        
                                        if( (!$NetworkID || $NetworkID=='') && ({$nid} != 0)) {
                                            $NetworkID = {$nid};
                                        }

                                        if($NetworkID && $NetworkID!='')
                                        {

                                            //Getting clients for selected network.   
                                            $.post("{$_subdomain}/Data/getClients/"+urlencode($NetworkID),        

                                            '',      
                                            function(data){
                                                    var $networkClients = eval("(" + data + ")");

                                                    if($networkClients)
                                                    {

                                                        for(var $i=0;$i<$networkClients.length;$i++)
                                                        {

                                                            $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                                                        }
                                                    }

                                                    $("#jncClientID").removeAttr('disabled');
                                                    $("#jncClientID").html('');
                                                    $("#jncClientID").html($clientDropDownList);

                                            });
                                        }    
                                        else
                                        {
                                            $("#jncClientID").attr('disabled', '');
                                        }
                                }      
                            );
                        /* Add a change handler to the network dropdown - ends here*/

                        /* Add a change handler to the client dropdown - strats here*/
                            $(document).on('change', '#jncClientID', 
                                function() {

                                    $branchDropDownList  = '<option value="">Select from drop down</option>';

                                        var $ClientID = $("#jncClientID").val();

                                        if($ClientID && $ClientID!='')
                                        {

                                            //Getting clients for selected network.   
                                            $.post("{$_subdomain}/Data/getBranches/"+urlencode($ClientID)+"/"+$("#jncNetworkID").val(),        

                                            '',      
                                            function(data){
                                                    var $clientBranches = eval("(" + data + ")");

                                                    if($clientBranches)
                                                    {
                                                        for(var $i=0;$i<$clientBranches.length;$i++)
                                                        {

                                                            $branchDropDownList += '<option value="'+$clientBranches[$i]['BranchID']+'" >'+$clientBranches[$i]['BranchName']+'</option>';
                                                        }
                                                    }

                                                    $("#jncBranchID").removeAttr('disabled');
                                                    $("#jncBranchID").html('');
                                                    $("#jncBranchID").html($branchDropDownList);

                                            });
                                        } 
                                        else
                                        {
                                            $("#jncBranchID").attr('disabled', '');
                                        }

                                }      
                            );
                        /* Add a change handler to the client dropdown - ends here*/


});
$(window).resize(function() {
	drawPanels();
	drawVisualization();
});
$(document).on('click', "#graphfullscreen", function() {
    $.colorbox( {   inline:true,
                                            href:"#biggraph",
                                            title: 'Big Graph',
                                            opacity: 0.75,
                                            height:650,
                                            width:900,
                                            overlayClose: false,
                                            escKey: false
                                         });
});

$(document).on('click', "#filter", function() {
    $.colorbox( {   inline:true,
                                            href:"#FilterForm",
                                            title: 'Filter by Type',
                                            opacity: 0.75,
                                            height:450,
                                            width:500,
                                            overlayClose: false,
                                            escKey: false
                                         });
});

$(document).on('click', "#filter_btn", function() {
    var filter;
    filter = ''; 
    
    if ($('#jncNetworkID').val() != '') {
        filter = filter + '/nid=' + $('#jncNetworkID').val();
    }
    
    if ($('#jncClientID').val() != '') {
        filter = filter + '/cid=' + $('#jncClientID').val();
    }
    
    if ($('#jncBranchID').val() != '') {
        filter = filter + '/bid=' + $('#jncBranchID').val();
    }
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}{if $spid != 0}/spid={$spid}{/if}/graphtype={$graphtype}/graph={$graphdatasrc}/showguage=1'+filter;    
});

$(document).on('change', "#graphdatasource", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}{if $spid != 0}/spid={$spid}{/if}/graphtype={$graphtype}/graph='+$('#graphdatasource').val();
});

$(document).on('click', "#openbysp", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby=sp{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage={$showguage}';
}); 

$(document).on('click', "#openbybranch", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby=branch{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage={$showguage}';
});

{if $tab == 'ClosedJobs'}
$(document).on('click', "#display30", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}/display=30{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage={$showguage}{if $display == 'dr'}/datefrom=' + $("#altdatefrom").val() + '/dateto=' + $("#altdateto").val() + '{/if}';
});

$(document).on('click', "#displaymtd", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}/display=mtd{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage={$showguage}{if $display == 'dr'}/datefrom=' + $("#altdatefrom").val() + '/dateto=' + $("#altdateto").val() + '{/if}';
});

$(document).on('click', "#displayytd", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}/display=ytd{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage={$showguage}{if $display == 'dr'}/datefrom=' + $("#altdatefrom").val() + '/dateto=' + $("#altdateto").val() + '{/if}'
});

$(document).on('click', "#displaydr", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}/display=dr/graph={$graphdatasrc}/datefrom=' + $("#altdatefrom").val() + '/dateto=' + $("#altdateto").val();
});

$(document).on('click', "#cmd_date_go", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}/display=dr/graph={$graphdatasrc}/datefrom=' + $("#altdatefrom").val() + '/dateto=' + $("#altdateto").val();
});
{/if}
</script>

    
{/block}

{block name=filters append}
<li><a href="#" class="colorbox" id="filter">Filter</a></li>
<li><a href="{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}/graphtype={$graphtype}/graph={$graphdatasrc}"  class="colorbox" >Clear Filter</a></li>
<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
<li>
    Open Jobs :
    <input type="radio" name="openbysp" id="openbysp" value="sp" {if $openby == 'sp'}checked="checked"{/if}> By Service Provider
    &nbsp;&nbsp;
    <input type="radio" name="openbybranch" id="openbybranch" value="branch" {if $openby == 'branch'}checked="checked"{/if}> By Branch&nbsp;&nbsp;&nbsp;
</li>
{if $tab == 'ClosedJobs'}

<li style="padding:18px">
    Show Data for :   
    <input type="radio" name="display30" id="display30" value="30" {if $display == '30'}checked="checked"{/if}> Last 30 days
    &nbsp;&nbsp;
    <input type="radio" name="displaymtd" id="displaymtd" value="mtd" {if $display == 'mtd'}checked="checked"{/if}> MTD
    &nbsp;&nbsp;
    <input type="radio" name="displayytd" id="displayytd" value="ytd" {if $display == 'ytd'}checked="checked"{/if}> YTD
    &nbsp;&nbsp;
    <input type="radio" name="displaydr" id="displaydr" value="dr" {if $display == 'dr'}checked="checked"{/if}> Date Range
    &nbsp;&nbsp;
        <div style="padding-right: 5px;padding-left:5px;padding-top: 5px;padding-bottom:5px; border:1px solid #c9c9c9;{if $display != 'dr'} display:none;{/if}">
            <table>
                <tr>
                    <td>
                        <label style="display: inline; width:200px;">Date From:</label>
                    </td>
                    <td>
                        <input type="hidden" name="altdatefrom" id="altdatefrom">
                        <input type="text" name="datefrom" id="datefrom" style="width:80px;" value="{$datefrom|date_format:'%d/%m/%Y'}">
                    </td>
                </tr>
                <tr>  
                    <td style="background-color: #ffffff"><label  style="display: inline; width:200px;">Date To:</label> </td>
                    <td style="background-color: #ffffff">
                        <input type="hidden" name="altdateto" id="altdateto" style="width:80px;">
                        <input type="text" name="datetp" id="dateto" style="width:80px;" value="{$dateto|date_format:'%d/%m/%Y'}">
                        <a id="cmd_date_go" style="float:right" class="colorbox" href="#">Go!</a>
                    </td>
                </tr>
            </table>
        </div>

</li>
{/if}
{/block}

{block name=PerformanceBody}



	<div id="left-panel">
		<div id="gauge-panel" class="corners gradient">
        	<h3 class="blue-letters-large" style="padding:7px 0px 0px 23px;">{$tab|replace:'Jobs':' Jobs'}{if $sp_name != ""} for {$sp_name|lower|capitalize|replace:'Tv ':'TV '}{/if}</h3>
        	<div class="clearfix"><!-- first row of gauges -->
          	
		  		<div class="round">
           			<div id="gauge"></div>
           			<h4 class="center blue-letters">Service Centre<br />Repair Capacity</h4>
          		</div>
		   
           		<div class="round">
          			<div id="gauge1"></div>
           			<h4 class="center blue-letters">Jobs Awaiting Parts<br />&nbsp</h4>
          		</div>
		   
           		<div class="round">
          			<div id="gauge2"></div>
           			<h4 class="center blue-letters">% Jobs Over {$gauge2.daysTarget} Days<br />&nbsp</h4>
          		</div>
          	</div>
          	<div class="clearfix"><!-- second gauge row -->
          	
		  		<div class="round">
          			<div id="gauge3"></div>
           			<h4 class="center blue-letters">Average Days<br />from Booking Date</h4>
          		</div>
          		
		   		<div class="round">
          			<div id="gauge4"></div>
           			<h4 class="center blue-letters">Average Days<br />Estimates Outstanding</h4>
          		</div>
          		
		   		<div class="round">
         			<div id="gauge5"></div>
           			<h4 class="center blue-letters">Jobs Awaiting an<br />Appointment</h4>
          		</div>
          
			</div><!-- end of second gauge row -->
		</div>
		<div id="chart-panel" class="corners gradient">
			<div id="graph"></div>
        	<div id="legend_chart">
     			<ul class="nav nav-list">
          			<li><a href="{$_subdomain}/Performance/JobsGauges/{$tab}/openby=branch/display={$display}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graphtype=ColumnChart/graph={$graphdatasrc}"><img src="{$_subdomain}/demo/img/glyphicons/glyphicons_041_charts.png"  style="margin-right:5px;">Bar</a></li>
          			<li><a href="{$_subdomain}/Performance/JobsGauges/{$tab}/openby=branch/display={$display}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graphtype=PieChart/graph={$graphdatasrc}"><img src="{$_subdomain}/demo/img/glyphicons/glyphicons_042_pie_chart.png"  style="margin-right:5px;">Pie</a></li>
                                <li><a href="{$_subdomain}/Performance/JobsGauges/{$tab}/openby=branch/display={$display}{if $display == 'dr'}/datefrom={$datefrom}/dateto={$dateto}{/if}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graphtype=Table/graph={$graphdatasrc}"><img src="{$_subdomain}/demo/img/glyphicons/glyphicons_090_table.png"  style="margin-right:5px;">Table</a></li>
                                <p>
                                <li>
                                    By <br>
                                    <select id="graphdatasource" name="graphdatasource" class="colorbox" style="width:90px;">
                                        {foreach from=$graphdataopts key=i item=opt }
                                        <option value="{$i}" {if $graphdatasrc == $i}Selected{/if}>{$opt}</option>
                                        {/foreach}
                                    </select>
                                </li>
                                <p>
          			<li><a href="#" class="colorbox" id="graphfullscreen">Enlarge</a></li>
        		</ul>
         	</div><!-- end of legend -->
		</div>
	</div>
	<div id="right-panel" class="corners gradient">
		<h3 class="blue-letters-large" style="top:0px;padding:7px 0px 0px 23px;">Repair Capacity</h3>
           <ul class="nav nav-list">  
              <!--li class="nav-header"></li-->  
              
              <table>
              {foreach $service_centre_repair_capacities as $scrp}
                  <tr><td><a href="{$_subdomain}/Performance/JobsGauges/{$tab}/spid={$scrp.ServiceProviderID}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graphtype={$graphtype}/graph={$graphdatasrc}">{$scrp.CompanyName|cat:' '|lower|capitalize|replace:'Tv ':'TV '|truncate:36:'...'}</td><td><span style="float:right;width:40px;text-align:center;" class="label {if $scrp.Capacity > $rc_colour_settings.red}label-important{elseif $scrp.Capacity > $rc_colour_settings.yellow}label-warning{else}label-success{/if}" style="float:right;">{$scrp.Capacity}</span></a></td></tr>
              {/foreach}
              </table>
            
           </ul> 
          
	</div>


                                    
</div>

<div style="display:none;" >        
    <div id="biggraph"  class="SystemAdminFormPanel">
        <div id="enlargegraph"></div>
    </div>
</div>
              
<div style="display:none;">
    <div id="FilterForm" class="SystemAdminFormPanel">

        <form id="NetworkClientForm" name="NetworkClientForm" method="post" >
            <fieldset>
                <legend title="">Filter by :-</legend>


                {if $showNetworkDropDown}

                    <p>
                        <label for="NetworkID" >Network:<sup>*</sup></label>
                        &nbsp;&nbsp;  <select name="NetworkID" id="jncNetworkID" class="text" >

                                <option value="" {if $nid == 0}selected="selected"{/if}>Select from drop down</option>

                            {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $nid == $network.NetworkID}selected="selected"{/if} >{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                    </p>
                {else}
                    <input type="hidden" name="NetworkID" id="jncNetworkID"  value="{$NetworkID|escape:'html'}" >
                {/if}    




                {if $ClientID}
                    <input type="hidden" name="ClientID" id="jncClientID"  value="{$ClientID|escape:'html'}" >
                {else}
                <p>
                    <label for="ClientID" >Client:<sup>*</sup></label>
                    &nbsp;&nbsp;  <select name="ClientID" id="jncClientID" class="text" >

                                    <option value="" {if $cid == 0}selected="selected"{/if}>Select from drop down</option>

                                    {foreach $clients as $client}

                                        <option value="{$client.ClientID}" {if $cid == $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                    {/foreach}
                                    </select>
                </p>
                {/if}



                <p>
                    <label for="BranchID" >Branch:<sup>*</sup></label>
                    &nbsp;&nbsp;  <select name="BranchID" id="jncBranchID" class="text" >

                                    <option value="" {if $bid == 0}selected="selected"{/if}>Select from drop down</option>

                                    {foreach $branches as $branch}

                                        <option value="{$branch.BranchID}" {if $bid == $branch.BranchID}selected="selected"{/if}>{$branch.BranchName|escape:'html'}</option>

                                    {/foreach}
                                    </select>
                </p>

                <p>
                    <label  >&nbsp;</label>

                    <input type="button" name="filter_btn" id="filter_btn" class="textSubmitButton"  value="Go" >


                </p>
            </fieldset>
        </form>
    </div>
</div>    


{/block}
