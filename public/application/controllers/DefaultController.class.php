<?php

require_once('CustomSmartyController.class.php');
require_once('Constants.class.php');
require_once('Functions.class.php');

/**
 * Short Description of DefaultController.
 * 
 * Long description of DefaultController.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.14
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version
 * 31/07/2012  1.01    Nageswara Rao Kanteti Changes to Timeline
 * 06/09/2012  1.02    Andrew J. Williams    Test Log 103 - E-Mail job functionality
 * 23/09/2012  1.02A   Nageswara Rao Kanteti Fixed bug: sending email again when we reload the page after mail has sent and job_booking email preparation.
 * 24/09/2012  1.03    Brian Etherington     Removed Timeline Product Location condition
 *                                           for Samsung Demo.
 * 26/09/2012  1.03A   Andrew J. Williams    Integrated grpahical analysis
 * 02/10/2012  1.04    Brian Etherington     Use DefaultBrandID if no Brand cookie
 * 24/10/2012  1.05    Brian Etherington     Updated TimeLine Status Codes
 *                                           Added date/time for historic events
 * 12/11/2012  1.06    Brian Etherington     Fixed syntax problem fetching Brand row from Brands model
 * 27/11/2012  1.07    Andrew J. Williams    Graphical Analysis - Added real data to dials for open jobs and closed jobs
 * 04/12/2012  1.08    Andrew J. Williams    BRS 120 - Graphical Analysis - Open TAT
 * 05/12/2012  1.09    Andrew J. Williams    Fixed issue with Get Jobs By Status 
 * 07/12/2012  1.10    Andrew J. Williams    Added filter by Network for Performance
 * 10/12/2012  1.11    Andrew J. Williams    Added filter by Branch / Client for Oprn Jobs / Open TAT / Closed Jobs
 * 14/12/2012  1.12    Andrew J. Williams    Active performance pages moved to the performance controller.
 * 21/01/2013  1.13    Brian Etherington     Added redirect to login from index method if user is not logged in and does not have
 *                                           catalogue search enabled.
 * 04/02/2013  1.14    Brian Etherington     Used Booked date on timeline instaed of the relevant status history date/time
 * 17/05/2013  1.15    Brian Etherington     Added Smarty variable DPDTrackerURL to JobDetails.tpl    
 ******************************************************************************/

class DefaultController extends CustomSmartyController {
    
    public  $config;  
    public  $session;
    public  $skyline;
    public  $user;
    public  $messages;
    private $lang = 'en';   
    public  $smarty;
    public  $usrModel;
    private $debug = false;
    
    public function __construct() { 
        
        parent::__construct(); 
                               
       /* ==========================================
        * Read Application Config file.
        * ==========================================
        */
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
        *  Initialise Session Model
        * ==========================================
        */
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
        $this->messages = $this->loadModel('Messages'); 
        
        /* =========== smarty assign =============== */
        $this->smarty->assign('ref', '');
        $this->smarty->assign('Search', '');
        $this->smarty->assign('ErrorMsg', '');
       //$this->smarty->assign('_theme', 'skyline');
             
        /* ==========================================
         *  Initialise User Class
         * ==========================================
         */
        
        $this->usrModel = $this->loadModel('Users');
       
        if (isset($this->session->UserID)) {
            
            $this->user = $this->usrModel->GetUser( $this->session->UserID );
	    
	    //echo("<pre>"); print_r($this->user); echo("</pre>");
	    
            $this->smarty->assign('name',$this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);  
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("l jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;
            //$this->smarty->assign('loggedin_user', $this->user);
            $this->smarty->assign('_theme', $this->user->Skin);
            
        } else {
            
            $this->user = $this->usrModel->GetUser(''); // initialise user variable as not logged in user
            
            $topLogoBrandID = isset($_COOKIE['brand']) ? $_COOKIE['brand'] : 0;
            
	    if(isset($_COOKIE['brand'])) {
		$brandModel = $this->loadModel("Brands");
		$skin = $brandModel->getBrandByID($_COOKIE['brand'])["Skin"];
		if($skin == "" || $skin == null) {
		    $skin = "skyline";
		}
		$this->smarty->assign('_theme', $skin);  
	    } else {
		$this->smarty->assign('_theme', 'skyline');  
	    }
	    
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('name','');
            $this->smarty->assign('last_logged_in','');  
            $this->smarty->assign('logged_in_branch_name', '');            
	    
        }
        
        $this->smarty->assign('loggedin_user', $this->user);
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);

	//Access permissions and smarty variables are initiated/assigned here.	
	$this->usrModel->initiatePermissions($this);
	
	//$this->log("USER: " . var_export($this->user, true));

    }
       
    public function RMATrackerAction( $args ) {
        
        /*
         * RMATracker API test method
         */
        
        //$rma = $this->loadModel('RMATrackerRESTClient');
        //$rma->PutNewJob();
        
        $model = $this->loadModel('APIJobs');
        $model->putNewJob(1234);
        
    }

    public function addresslookupAction( $args ) {
        
        $firstArg = isset($args[0])?$args[0]:'';
        
        $postcode = $this->loadModel('Postcode');
       
        $this->smarty->assign('page', $this->messages->getPage('AddressLookup'));
       
        $addresses = $postcode->getAddress($_POST['postcode']);
        
        
        if(!is_array($addresses))
        {
            $addresses = array();
        }
        
        foreach ($addresses as $ak=>$av)
        {
            
            $add_str = '';
            
            
              if($addresses[$ak][4] || $addresses[$ak][6]  || $addresses[$ak][7])
              {
                  $add_str =  trim($addresses[$ak][4]." ".$addresses[$ak][6]." ".$addresses[$ak][7]);
              }
             
            
              if($addresses[$ak][5]  || $addresses[$ak][0])
              {
                  if($add_str)
                  {
                     $add_str .=  ", "; 
                  }
                  $add_str .=  trim($addresses[$ak][5].' '.$addresses[$ak][0]);

              }
              
              
              if($addresses[$ak][2])
              {
                  if($add_str)
                  {
                     $add_str .=  ", "; 
                  }
                  $add_str .=  $addresses[$ak][2];
              }    
              
              if($addresses[$ak][3])
              {
                  if($add_str)
                  {
                     $add_str .=  ", "; 
                  }
                  $add_str .=  $addresses[$ak][3];
              }
            
            $addresses[$ak][11]  = $add_str;
            
        }
        
        //$this->log(var_export($addresses,true));
        
        $this->smarty->Assign('addresses',  $addresses);
        
        $dropdown_size   = 35;
        $dropdown_height = (($dropdown_size+1)*16 + 10); //16 is the heigh of line.
        if(count($addresses)<$dropdown_size)
        {
            $dropdown_size = count($addresses);
            
            if($dropdown_size<=10)//To make dropdown looks better.
            {    
                $dropdown_size   = $dropdown_size+3;
            }
            $dropdown_height = (($dropdown_size+1)*16)+10;
        } 
               
        $this->smarty->Assign('dropdown_size',  $dropdown_size);
        $this->smarty->Assign('dropdown_height',  $dropdown_height);
        $this->smarty->Assign('multiAddressLookupFlag',  $firstArg);
        //$this->smarty->display('AddressLookup.tpl');
        $output = $this->smarty->fetch('AddressLookup.tpl');
        //$this->log('postcode html: '.$output);
        echo $output;
    }    

    
    
    public function indexAction($args) { 
                                  
	//cookies
	//$this->log('DefaultController > indexAction -> cookies : ' . var_export($_COOKIE, true));

	//$this->log("USER: " . var_export($this->user, true));
	
	if(isset($this->user->ServiceProviderID)) {

	    $diary = $this->loadModel("Diary");           
	    $useAllocationOnly = $diary->getDiaryType($this->user->ServiceProviderID);
	    if($useAllocationOnly == "AllocationOnly") {
		$this->redirect('AppointmentDiary'); 
	    }
            if($useAllocationOnly == "NoViamente") {
		$this->redirect('Diary'); 
	    }

	}
                
	if(isset($this->user->ClientID)) {
	    $client_model = $this->loadModel("Clients");
	    $enableCatalogueSearch = $client_model->getEnabledCatalogueSearch($this->user->ClientID);
	    
	}
               
        if(isset($this->session->UserID))
        {    
            $this->smarty->assign('AP7003', Functions::hasPermission($this->user, 'AP7003')); // Display Job Search Box
            $this->smarty->assign('AP11000', Functions::hasPermission($this->user, 'AP11000')); // Display Booking Panel on Home Page
        }
        
        $this->smarty->assign('productNoResultsFlag', false);
        
        //This is for testing purpose.
        if(isset($args['cookieclear']))
        {
            setcookie("branch", 0, time()-31536000, '/'); 
            setcookie("brand", 0, time()-31536000, '/'); 
        }
                               
        if((isset($_COOKIE['brand']) && $_COOKIE['brand']!='' && $_COOKIE['brand']!=0) || isset($this->session->UserID))
        { 
            
            $skyline_model = $this->loadModel('Skyline');
            
            //Checking product database enable or not for client. - starts here..
            $ProductDatabaseEnabled  = false;
            $enableCatalogueSearch  = false;
            $ShowJobBookingPanel     = false;
            $JobTypesList            = array();
            if(isset($this->session->UserID))  $JobTypesList = $skyline_model->getJobBookingButtons(null);
            
           // $this->log($JobTypesList);
            
            if(isset($this->session->UserID) && $this->user->ClientID)
            {
               $ClientID  = $this->user->ClientID;
               $NetworkID = $this->user->NetworkID;
            }
            else
            {    
                $brands_model  = $this->loadModel('Brands');
                $BrandsDetails = $brands_model->fetchRow(array("BrandID"=>isset($_COOKIE['brand'])?$_COOKIE['brand']:$this->user->DefaultBrandID));
                
                $ClientID  = isset($BrandsDetails['ClientID'])?$BrandsDetails['ClientID']:0;
            }
                        
            if($ClientID)
            {    
                $client_model = $this->loadModel('Clients');
                $ProductDatabaseEnabled = $client_model->isProductDatabaseEnabled($ClientID);
                $enableCatalogueSearch = $client_model->getEnabledCatalogueSearch($ClientID);
                               
                if($ProductDatabaseEnabled==false)
                {
                    if(isset($this->session->UserID))
                    {    
                        
                        $ShowJobBookingPanel = true;
                        
                                                
                    }
                    else
                    {
                        $this->redirect('Login'); 
                    }
                }
                else
                {
                    $ProductDatabaseEnabled = true;
                    $this->smarty->assign('productNoResultsFlag', false);
                }
                
               if(isset($this->user) && $this->user->NetworkID && $this->user->ClientID)
               {    
                    $JobTypesListTemp  = $JobTypesList;
                    $JobTypesList = array();    
                    for($j=0;$j<count($JobTypesListTemp);$j++)
                    {
                        $service_type_rows_temp  = $skyline_model->getServiceTypes(null, $JobTypesListTemp[$j]['JobTypeID'], $this->user->NetworkID, $this->user->ClientID);
                    
                        if(count($service_type_rows_temp)>0)
                        {
                            $JobTypesList[] = $JobTypesListTemp[$j];
                        }
                    } 
               }
            }
            //Checking product database enable or not for client. - ends here..           

            //Checking product database enable or not for client. - starts here..
            /*$ProductDatabaseEnabled  = false;
            $ShowJobBookingPanel     = false;
            
            
            if(isset($this->session->UserID) && $this->user->ClientID)
            {
               $ClientID = $this->user->ClientID;
            }
            else
            {    
                $brands_model  = $this->loadModel('Brands');
                $BrandsDetails = $brands_model->fetchRow(array("BrandID"=>isset($_COOKIE['brand'])?$_COOKIE['brand']:$this->user->DefaultBrandID));
                
                $ClientID = isset($BrandsDetails['ClientID'])?$BrandsDetails['ClientID']:0;
            }
            
            if($ClientID)
            {    
                $client_model = $this->loadModel('Clients');
                $ProductDatabaseEnabled = $client_model->isProductDatabaseEnabled($ClientID);
                if($ProductDatabaseEnabled==false)
                {
                    if(isset($this->session->UserID))
                    {    
                        
                        $ShowJobBookingPanel = true;
                       // $JobTypesList = $skyline_model->getJobTypes();
                    }
                    else
                    {
                        $this->redirect('Login'); 
                    }
                }
                else
                {
                    $ProductDatabaseEnabled = true;
                    $this->smarty->assign('productNoResultsFlag', false);
                }
            }

            //Checking product database enable or not for client. - ends here..
            */
            
            if (isset($args['lang'])) { 
                $this->session->lang = $args['lang'];  
            }

            if(isset($args[0])){
                switch($args[0]){
                    case 'cancelAdvancedSearch':
                        $this->smarty->assign('ref', 'P7101');
                        break;
                    default:
                        $this->smarty->assign('ref', '');
                }
            }
                        
            $product_model = $this->loadModel( 'Product' );
            $this->smarty->assign('Product', $product_model);
            $job_model = $this->loadModel( 'Job' );
            
            $this->smarty->assign('Job', $job_model);
            $this->smarty->assign('productSearch', '');
            $this->smarty->assign('policySearch', '');
            $this->smarty->assign('productGroup', 'ProductNo');  

            $this->smarty->assign('RadioCatagories', $product_model->search_fields);  
            
            $this->smarty->assign('page',$this->messages->getPage('home',$this->lang));
            $branches = $skyline_model->getBranches();
            $this->smarty->assign('branches',$branches);
            // get branch cookie if it is set....
            if (isset($_COOKIE['branch'])) {
                $this->smarty->assign('branch',$_COOKIE['branch']);
            } else {
                $this->smarty->assign('branch','');
            }
            
            if(isset($args['error']))
            {
               $error  = $args['error'];
            }
            else
            {
                $error = "";
            }  
                        	    
	    //$displayOpenOverdueJobs = 1;
            $displayOpenOverdueJobs = 0;
            if(isset($this->user) && ($this->user->SuperAdmin || isset($this->user->Permissions['AP7004']))) {
                $displayOpenOverdueJobs = 1;
            }
                       
	    if(isset($this->user) && isset($this->user->UserID) && $this->user->UserID != "") {	    
		
		$jobsAppraisalRequiredNumber = $job_model->getJobsAppraisalRequiredNumber($this->user);
		$this->smarty->assign("jobsAppraisalRequiredNumber", $jobsAppraisalRequiredNumber);
		
		$jobsBranchRepairNumber = $job_model->getJobsBranchRepairNumber($this->user);
		$this->smarty->assign("jobsBranchRepairNumber", $jobsBranchRepairNumber);
                		
		$jobsAuthRequiredNumber = $job_model->getJobsAuthRequiredNumber($this->user);
		$this->smarty->assign("jobsAuthRequiredNumber", $jobsAuthRequiredNumber);
                		
		$jobsAwaitingPartsNumber = $job_model->getJobsByStatusNumber($this->user,"awaiting_parts");
		$this->smarty->assign("jobsAwaitingPartsNumber", $jobsAwaitingPartsNumber);
                
                $jobsInStoreNumber = $job_model->getJobsByStatusNumber($this->user,"in_store");
		$this->smarty->assign("jobsInStoreNumber", $jobsInStoreNumber);
                
		$jobsWithSupplierNumber = $job_model->getJobsByStatusNumber($this->user,"with_supplier");
		$this->smarty->assign("jobsWithSupplierNumber", $jobsWithSupplierNumber); 
                
		$jobsAwaitingCollectionNumber = $job_model->getJobsByStatusNumber($this->user,"awaiting_collection");
		$this->smarty->assign("jobsAwaitingCollectionNumber", $jobsAwaitingCollectionNumber);
                                
		$jobsCustomerNotifiedNumber = $job_model->getJobsByStatusNumber($this->user,"customer_notified");
		$this->smarty->assign("jobsCustomerNotifiedNumber", $jobsCustomerNotifiedNumber);
                
                $jobsResponseRequiredNumber = $job_model->getJobsQueryNumber($this->user);
		$this->smarty->assign("jobsResponseRequiredNumber", $jobsResponseRequiredNumber);
                               
                $openJobsTotal = $jobsAppraisalRequiredNumber + $jobsBranchRepairNumber + $jobsAuthRequiredNumber + $jobsInStoreNumber + $jobsWithSupplierNumber + $jobsAwaitingCollectionNumber + $jobsCustomerNotifiedNumber + $jobsResponseRequiredNumber;
		$this->smarty->assign("openJobsTotal", $openJobsTotal);
                                
		$jobsOverdue1to5 = $job_model->getOverdueJobsNumber($this->user,"1to5");
		$this->smarty->assign("jobsOverdue1to5", $jobsOverdue1to5);
                		
		$jobsOverdue6to10 = $job_model->getOverdueJobsNumber($this->user,"6to10");
		$this->smarty->assign("jobsOverdue6to10", $jobsOverdue6to10);
                		
		$jobsOverdueOver10 = $job_model->getOverdueJobsNumber($this->user,"over10");
		$this->smarty->assign("jobsOverdueOver10", $jobsOverdueOver10);
                                
                $overdueJobsTotal = $jobsOverdue1to5 + $jobsOverdue6to10 + $jobsOverdueOver10;
		$this->smarty->assign("overdueJobsTotal", $overdueJobsTotal);
                              
                $this->smarty->assign('AP7037', Functions::hasPermission($this->user, 'AP7037')); // Display Policy Search Panel
		
	    } else {
		
                $this->smarty->assign('AP7037', false); // Display Policy Search Panel
		
            }
            	    
            $this->smarty->assign('displayOpenOverdueJobs', $displayOpenOverdueJobs);
            $this->smarty->assign('error', is_numeric($error) ? $this->messages->getError($error) : $error );
            $this->smarty->assign('ProductDatabaseEnabled', $ProductDatabaseEnabled);
            $this->smarty->assign("enableCatalogueSearch",$enableCatalogueSearch);
            
            $this->smarty->assign('JobTypesList', $JobTypesList);
            $this->smarty->assign('ShowJobBookingPanel', $ShowJobBookingPanel); 
            
            // redirect to login if not currently logged in and 
            // catalogue search is not enabled
           
            
            if (!$enableCatalogueSearch && !$this->session->UserID) {
                $this->redirect('Login');
            }
                                    
            $this->smarty->display('home.tpl');
            
        } else {
            	    
             $this->redirect('Login'); 
	     
        }
        
    }

    
    
    public function jobsAction( $args ){
        $model = $this->loadModel('Job');
  
        $function = isset($args[0]) ? $args[0] : '';
        
        #$this->log('DefaultController -> jobsAction | args :  ' . var_export($args, true) );
        
        if( method_exists($model, $function) ){
            $result = $model->$function($_POST);
            
        }else{
            $result = array('status' => 'ERROR', 'message' => 'Unrecognised argument');
        }        
    }

    
    
    public function jobSearchAction($args) { 
        	
	$Skyline = $this->loadModel('Skyline');
	$this->smarty->assign("userType",$this->user->UserType);
	$this->smarty->assign("post",$_POST);

        $page = $this->messages->getPage('Job', $this->lang);
	$this->smarty->assign("page",$page);
	
	
	//FILTERING
	
	switch($this->user->UserType) {

	    //Admin
	    
	    case "Admin" : {
		
		$networks = $Skyline->getNetworks();
		
		if(empty($_POST)) { 
		    
		    $clients = array();
		    $branches = array();
		    $this->smarty->assign("activeFilter","''");
		    $this->smarty->assign("filterID","''");
		    if(count($networks)==1) {
			$clients = $Skyline->getNetworkClients($networks[0]["NetworkID"]);
		    }
		    
		} else {
		    
		    if(isset($_POST["networkID"])) {
			if($_POST["networkID"]!="") {
			    $clients = $Skyline->getNetworkClients($_POST["networkID"]);
			} else {
			    if(count($networks)==1) {
				$clients = $Skyline->getNetworkClients($networks[0]["NetworkID"]);
			    } else {
				$clients = array();
			    }
			}
			$branches = array();
		    }
		    
		    if(isset($_POST["clientID"])) {
			if($_POST["clientID"]!="") {
			    $branches = $Skyline->GetClientBranches($_POST["clientID"]);
			} else {
			    if(count($clients)==1) {
				$branches = $Skyline->GetClientBranches($clients[0]["clientID"]);
			    } else {
				$branches = array();
			    }
			}
		    }
		    
		    switch ($_POST["activeFilter"]) {
			
			case "network" : {
			    $this->smarty->assign("activeFilter","'network'");
			    $this->smarty->assign("filterID",$_POST["networkID"]);
			    break;
			}
			
			case "client" : {
			    if($_POST["clientID"]=="") {
				$this->smarty->assign("activeFilter","'network'");
				$this->smarty->assign("filterID",$_POST["networkID"]);
			    } else {
				$this->smarty->assign("activeFilter","'client'");
				$this->smarty->assign("filterID",$_POST["clientID"]);
			    }
			    break;
			}
			
			case "branch" : {
			    if($_POST["branchID"]=="") {
				$this->smarty->assign("activeFilter","'client'");
				$this->smarty->assign("filterID",$_POST["clientID"]);
			    } else {
				$this->smarty->assign("activeFilter","'branch'");
				$this->smarty->assign("filterID",$_POST["branchID"]);
			    }
			    break;
			}
			
			case "service" : {
			    $this->smarty->assign("activeFilter","'service'");
			    $this->smarty->assign("filterID",$_POST["serviceProviderID"]);
			    $clients=array();
			    $branches=array();
			}
			
		    }
		    
		}
		
		$this->smarty->assign('networks', $networks);
		$this->smarty->assign('clients', $clients);
		$this->smarty->assign('branches', $branches);
		break;
	    
	    }
	    
	    
	    //Network
	    
	    case "Network" : {
		
		$clients = $Skyline->getNetworkClients($this->user->NetworkID);
                $this->log($clients);

		if(empty($_POST)) {
		    
		    $branches = array();
		    $this->smarty->assign("activeFilter","''");
		    $this->smarty->assign("filterID","''");
		    if(count($clients)==1) {
			$branches = $Skyline->GetClientBranches($clients[0]["ClientID"]);
		    }
		    
		} else {
		    
		    if(isset($_POST["clientID"])) {
			if($_POST["clientID"]!="") {
			    $branches = $Skyline->GetClientBranches($_POST["clientID"]);
			} else {
			    if(count($clients)==1) {
				$branches = $Skyline->GetClientBranches($clients[0]["ClientID"]);
			    } else {
				$branches = array();
			    }
			}
		    }
		    
		    switch ($_POST["activeFilter"]) {
			
			case "client" : {
			    if($_POST["clientID"]=="") {
				$this->smarty->assign("activeFilter","'network'");
				$this->smarty->assign("filterID",$this->user->NetworkID);
			    } else {
				$this->smarty->assign("activeFilter","'client'");
				$this->smarty->assign("filterID",$_POST["clientID"]);
			    }
			    break;
			}
			
			case "branch" : {
			    if($_POST["branchID"]=="") {
				$this->smarty->assign("activeFilter","'client'");
				$this->smarty->assign("filterID",$_POST["clientID"]);
			    } else {
				$this->smarty->assign("activeFilter","'branch'");
				$this->smarty->assign("filterID",$_POST["branchID"]);
			    }
			    break;
			}
			
			case "service" : {
			    $this->smarty->assign("activeFilter","'service'");
			    $this->smarty->assign("filterID",$_POST["serviceProviderID"]);
			    $branches=array();
			}
			
		    }
		    
		}
		
		$this->smarty->assign('clients', $clients);
		$this->smarty->assign('branches', $branches);
		break;
		
	    }

	    
	    //Client
	    
	    case "Client" : {

		$branches = $Skyline->GetClientBranches($this->user->ClientID);
		$this->smarty->assign('branches', $branches);
		
		if(empty($_POST)) {
		
		    $this->smarty->assign("activeFilter","''");
		    $this->smarty->assign("filterID","''");
		    break;
		
		} else {
		    
		    switch ($_POST["activeFilter"]) {
		    
			case "branch" : {
			    $this->smarty->assign("activeFilter","'branch'");
			    $this->smarty->assign("filterID",$_POST["branchID"]);
			    break;
			}
			
			case "service" : {
			    $this->smarty->assign("activeFilter","'service'");
			    $this->smarty->assign("filterID",$_POST["serviceProviderID"]);
			    break;
			}
			
		    }
		    
		}
		
		break;
	    
	    }
	    
	   
	    //Branch
	    
	    case "Branch" : {
		$this->smarty->assign("activeFilter","''");
		$this->smarty->assign("filterID","''");
	    }
	    

	}
        
	//END OF FILTERING
	
	
	$this->smarty->assign("serviceProviders", $Skyline->getNetworkServiceProviders($this->user->NetworkID));
	//$this->smarty->assign("serviceProviders", $Skyline->getUserServiceProviders($this->user));
	
	$serviceProviderID=(isset($_POST["serviceProviderID"])) ? $_POST["serviceProviderID"] : "";
	$this->smarty->assign("serviceProviderID",$serviceProviderID);
	
	$this->smarty->assign('nId', isset($_POST["networkID"]) ? $_POST["networkID"] : null);
	$this->smarty->assign('cId', isset($_POST["clientID"]) ? $_POST["clientID"] : null);
	$this->smarty->assign('bId', isset($_POST["branchID"]) ? $_POST["branchID"] : null);
	
	
        $job_model = $this->loadModel( 'Job' );

        $jobSearch = ( isset($_GET['jobSearch']) ? $_GET['jobSearch'] : '');
       
        $batchID = (isset($_GET["batchID"]) ? $_GET["batchID"] : "");
	
        
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                       
	    if(!isset($_GET['jobSearch'])){
                $this->smarty->assign('ErrorMsg', 'Missing Search Term');
            } else {               
		$job_model->search_value = $jobSearch;
		$sResults = $job_model->search(array("jobSearch" => $jobSearch, "batchID" => $batchID, "searchTypeCount"=>1));
                             
		if(isset($sResults['resultRows']) && $sResults['resultRows']=='1' && isset($sResults['JobID'])) {
		    $this->redirect('index/jobupdate/'.$sResults['JobID']);
		}
               
            }
            
        }
                      
        $this->smarty->assign('Job', $job_model);
                
        $this->smarty->display('JobMatches.tpl');
        #$this->smarty->display('jobDetails.tpl');
    }
    
    
    
   /**
    * The is to display performance page. 
    *
    * @param array $args
    * @return void
    * 
    * Initial Static Demo
    * @author Nageswara Rao Kanteti nag.phpdeveloper@gmail.com
    * 
    *  
    ********************************************/
     public function performanceAction( $args ) {

         if( isset($args[0]) ) {
             $tab = $args[0];
         } else {
            $tab = 'report';
         }
        
        $page = $this->messages->getPage('performance', $this->lang);
        $this->smarty->assign('page', $page);  
            $this->smarty->assign('tab', $tab);
 
                $this->smarty->display('performance_demo.tpl');

    }
    
    
    
    
     /**
    * The is to display site map page.
    *
    * @param array $args
    * @return void
    * @author Nageswara Rao Kanteti nag.phpdeveloper@gmail.com
    * 
    * 
    ********************************************/
     public function siteMapAction( $args ) {
         
           
         
          if (!isset($this->session->UserID)) 
          {
             $this->redirect('index'); 
          }
          else
          {    
             $page = $this->messages->getPage('siteMap', $this->lang);
             $this->smarty->assign('page', $page); 
          
             $displayWallBoardLink = false;
             
             if($this->user->SuperAdmin || $this->user->NetworkID)
             {
                 $displayWallBoardLink = true;
             }         
              
             $this->smarty->assign('displayWallBoardLink', $displayWallBoardLink); 
             
             $this->smarty->assign('AP11002', Functions::hasPermission($this->user, 'AP11002')); // Site Map - Reports
             $this->smarty->assign('AP11003', Functions::hasPermission($this->user, 'AP11003')); // Site Map - Reports – Samsung
             $this->smarty->assign('AP11004', Functions::hasPermission($this->user, 'AP11004')); // Site Map - Request Authorisation
             
             
             $this->smarty->assign('AP11006', Functions::hasPermission($this->user, 'AP11006')); // Site Map – Appointment Diary
             $this->smarty->assign('AP11007', Functions::hasPermission($this->user, 'AP11007')); // Site Map – Booking Performance Wallboard
             $this->smarty->assign('AP11008', Functions::hasPermission($this->user, 'AP11008')); // Site Map – Diary Wall Board
             $this->smarty->assign('AP8101' , Functions::hasPermission($this->user, 'AP8101'));  // Site Map – Part Table
             $this->smarty->assign('AP8001' , Functions::hasPermission($this->user, 'AP8001'));  // Site Map – System Admin Service Provider Table
             
             
             $this->smarty->display('siteMap.tpl');
          }  
    }

    
     /**
    * The is to display ASTR Report page.
    *
    * @param array $args
    * @return void
    * @author Vic v.rutkunas@pccsuk.com
    * 
    * 
    ********************************************/
     public function astrReportAction( $args ) {
         
//	 print_r($this->user->Permissions);
//	 throw new Exception("test");
	 
          $page = $this->messages->getPage('siteMap', $this->lang);
          $this->smarty->assign('page', $page);  
         
          if (!isset($this->session->UserID)) 
          {
             $this->redirect('index'); 
          }
          else
          {    
            $this->smarty->display('astrReport.tpl');
          }  
    }
    
    
    
     /**
    * The is to display help page. 
    *
    * @param array $args
    * @return void
    * @author Nageswara Rao Kanteti nag.phpdeveloper@gmail.com
    * 
    * 
    ********************************************/
     public function helpAction( $args ) {
         
          $page = $this->messages->getPage('help', $this->lang);
          $this->smarty->assign('page', $page);  
         
             
          $this->smarty->display('help.tpl');
            
    }
    
    
    
    /**
    * 
    * jobUpdateAction
    * @param array $args
    * @return void
    * @author Simon Tsang s.tsang@pccsuk.com	
    * @version     1.01
    *   
    * Changes
    * Date        Version Author                Reason
    * ??/??/????  1.00    Simon Tsang           Initial Version
    * 31/07/2012  1.01    Nageswara Rao Kanteti Changes to Timeline
    ********************************************/
    
    public function jobUpdateAction($args) { 
        
        if(!isset($this->session->UserID)) {

	    $this->redirect('index'); 
	    
        } else {
        
            $JobID = isset($args[0]) ? $args[0] : '';
            $goto = isset($args['goto']) ? $args['goto'] : false;
            
            if($goto) {
                $this->redirect('index/jobupdate/' . $JobID . '#' . $goto); 
            } 
            
            /*
             * Strore JobID in Session to Show pre-Selected in 
             * Open,Overdue, and Closed jobs Data Table.
             */
            
            if($JobID && isset($_REQUEST['ref'])){
                switch ($_REQUEST['ref']){
                    case "openJobs":
                        $_SESSION['openjob']['detail']['JobID'] = $JobID;
                    break;
                    case "overdueJobs":
                        $_SESSION['overduejob']['detail']['JobID'] = $JobID;
                    break;
                    case "closedJobs":
                        $_SESSION['closedjobs']['detail']['JobID'] = $JobID;
                    break;
                }
            }
            
	    
	    //This is to be rectified, because even though this code is not supposed to be here, absence causes issues.
	    if($_SESSION["UserID"] != "sa") {
		if(array_key_exists("AP7020", $this->user->PermissionsURLSegments)) {
		    $this->smarty->assign("englishFlag", true);
		} else {
		    $this->smarty->assign("englishFlag", false);
		}
		if(array_key_exists("AP7021", $this->user->PermissionsURLSegments)) {
		    $this->smarty->assign("germanFlag", true);
		} else {
		    $this->smarty->assign("germanFlag", false);
		}
	    } else {
		$this->smarty->assign("englishFlag", true);
		$this->smarty->assign("germanFlag", true);
	    }
	    //End of changes

	    
            $job_model = $this->loadModel( 'Job' );
            //$job_model->debug = true;
            $skyline_model = $this->loadModel( 'Skyline' );
            //$skyline_model->debug = true;
            $service_providers_model = $this->loadModel('ServiceProviders');
            //$service_providers_model->debug = true;
	    //$manufacturers_model = $this->loadModel('Manufacturers');
            $branches_model = $this->loadModel( 'Branches' );
            //$branches_model->debug = true;
            $RAStatusTypes_model = $this->loadModel('RAStatusTypes');
            //$RAStatusTypes_model->debug = true;
            
            $job_model->fetch($JobID);
            
//            echo '<pre>';
//            print_r($job_model);
//            echo '</pre>';
//            exit;
//            
            
            if($job_model->current_record['Accessories'] == "-1") {
                $job_model->current_record['Accessories'] = 'None';
            }    
            
            if(!$job_model->current_record['Accessories']) {
                //Getting Job's Accessories.
                $accessories_model = $this->loadModel('Accessories');
                
                $jobAccessories = $accessories_model->fetchJobAccessories($JobID);
                
                if(is_array($jobAccessories)) {    
                    $job_model->current_record['Accessories'] = '';
                    foreach($jobAccessories as $ja) {    
			$job_model->current_record['Accessories'] .= $ja['AccessoryName'].', ';
                    }
                    $job_model->current_record['Accessories'] = substr($job_model->current_record['Accessories'], 0, strlen($job_model->current_record['Accessories'])-2);
                }
            }
            

            $page = $this->messages->getPage('jobDetails', $this->lang);
            $service_providers_page = $this->messages->getPage('serviceProviders', $this->lang);            

	    /*
            //Changing following lables for DSGi for demo purpose, we must remove when Vic implemented alternative fields names work.
            if($job_model->current_record['ClientID'] == 133) {
               $page['Labels']['policy_no'] = 'Agreement Ref';
               $page['Labels']['authorisation_no'] = 'Client ID';
               $page['Labels']['referral_no'] = 'Referral No.';
            }
	    */
            /**********************/
            
            
            
            $this->smarty->assign('page', $page);  
            $this->smarty->assign('service_providers_page', $service_providers_page);

            $this->smarty->assign('FullAddress', $job_model->getFullAddress($JobID)); 
            $this->smarty->assign('FullDeliveryAddress', $job_model->getFullAddress($JobID, true));            
            //$this->smarty->assign('PaymentType', $skyline_model->getPaymentType());      

            #$this->smarty->assign('JobTurnaroundTime', $job_model->getTurnaroundTimeline() );

            $product_details = $job_model->getProductDetails( $JobID );
            $this->smarty->assign('ProductDetails', $product_details[0] ); 
            
            
            $mfg_rows = $skyline_model->getManufacturer("", null, $job_model->current_record["ClientID"]);
            
            if(!count($mfg_rows)) {
                $mfg_rows = $skyline_model->getManufacturer("", $job_model->current_record["NetworkID"]);
            }
            if(!count($mfg_rows)) {
                $mfg_rows = $skyline_model->getManufacturer();
            }
            
            $this->smarty->assign("manufacturerList", $mfg_rows); 
            

            $additional_information = [];
            $additional_information = $job_model->getAdditionalInformation($JobID);
            $additional_information["PurchaseDate"] = date("d/m/Y", strtotime($additional_information["PurchaseDate"]));
            $this->smarty->assign('AdditionalInformation', $additional_information); 

            $this->smarty->assign("NumPartsUsed", $job_model->getNumPartsUsed($JobID));
            $this->smarty->assign("NumContactHistory", $job_model->getNumContactHistory($JobID));
            $this->smarty->assign("NumStatusChanges", $job_model->getNumStatusChanges($JobID));

	    
	    $auditModel = $this->loadModel("Audit");
            //$auditModel->debug = true;
	    $NumAuditTrail = $auditModel->getAuditTrailCountByJobID($JobID);
	    $this->smarty->assign("NumAuditTrail", $NumAuditTrail);
	    
            $this->smarty->assign("productTypeList", $skyline_model->getUnitTypes(null, $this->user->ClientID, null));
            $this->smarty->assign("job_type_rows", $skyline_model->getJobTypes());

            if(is_null($job_model->current_record["ServiceProviderID"]) ) { 
                $scc[] = ["Title" => "", "Name" => "", "WorkPhone" => "", "ContactMobile" => ""];
                $this->smarty->assign("ServiceCentreContacts", $scc);                 /* No service centre set so can't get contacts */
                $this->smarty->assign("ServiceCentreName", "None assigned");
                $serviceProviderName = "";
            } else {
                $this->smarty->assign("ServiceCentreContacts", $service_providers_model->getServiceCentreContactNameRole($job_model->current_record["ServiceProviderID"]));
                $serviceProviderDetails = $skyline_model->getServiceCenterDetails($job_model->current_record["ServiceProviderID"]);
                $serviceProviderName = (isset($serviceProviderDetails["CompanyName"]) && $serviceProviderDetails["CompanyName"]) ? $serviceProviderDetails['CompanyName'] : "";
            }

            $InvoiceCosts = $job_model->getInvoiceCosts($JobID);
            $VATRatesModel = $this->loadModel("VATRates");
            //$VATRatesModel->debug = true;
            
            $vatRateReult = $VATRatesModel->fetchVatRate("T1");
            
            $InvoiceCosts["VATRate"] = $vatRateReult;
            
            $this->smarty->assign("InvoiceCosts", $InvoiceCosts);
            
            
            $BranchName = $branches_model->name($job_model->current_record["BranchID"]);
	    
	    
	    
            //Timeline part starts here...
                
            $TimeLine = [];
                
            $status_history_model = $this->loadModel('StatusHistory');
            //$status_history_model->debug = true;
            
            $statusHistory = $status_history_model->getJobStatusHistory($JobID);
                
            if (count($statusHistory) > 0) {
                $job_status_date = $statusHistory[0]['StatusDate'];
            } else {
                $job_status_date = null;
            }
                
            $job_status = $job_model->current_record['StatusName'];
                
            // Build up TimeLine array - adding stages as necessary
                
            // Job Booked Stage
            // ============================================================
            $stage = ["Name"=>"Job Booked", "Tense"=>"future", "Date"=>"", "Time"=>""];
            $status_array = [	'00 REPAIR AUTHORISATION',
				'01 UNALLOCATED JOB',
				'02 ALLOCATED TO AGENT',
				'03 TRANSMITTED TO AGENT'
			    ];
            
            $stage['Time']    = date("H:i", strtotime($job_model->current_record['TimeBooked']));
            $stage['Date']    = date("d/m/Y", strtotime($job_model->current_record['DateBooked'])); 
            
            if (in_array($job_status, $status_array) ) {
                $stage['Tense']   = "present";
                //$stage['Time']    = date("H:i", strtotime($job_status_date));
                //$stage['Date']    = date("d/m/Y", strtotime($job_status_date));
            } else {
                $stage['Tense']   = "past";
                /*$status_date = $this->getStatusDate($status_array, $statusHistory);
                if ($status_date == null) {
                    $stage['Tense']   = "past";
                    $stage['Time']    = date("H:i", strtotime($job_model->current_record['TimeBooked']));
                    $stage['Date']    = date("d/m/Y", strtotime($job_model->current_record['DateBooked'])); 
                } else {
                    $stage['Tense']   = "past";
                    $stage['Time']    = date("H:i", strtotime($status_date));
                    $stage['Date']    = date("d/m/Y", strtotime($status_date));                   
                }*/
            }
            $TimeLine[] = $stage;
            $max_stage = count($TimeLine)-1;
                
            // Job Viewed stage
            // ============================================================
            $stage = array("Name"=>"Job Viewed", "Tense"=>"future", "Date"=>"", "Time"=>""); 
            $status_array = array( '04 JOB VIEWED BY AGENT',
                                   '05 FIELD CALL REQUIRED',
                                   '34 PACKAGING SENT',
                                   '39 CX NOT AVAIL - MESSAGE LEFT',
                                   '40 CX NOT AVAIL - NO RESPONSE',
                                   '41 CX NOT AVAIL - WRONG NUM',
                                   '16 AUTHORITY REQUIRED',
                                   '17 AUTHORITY ISSUED' );
                        
            if (in_array($job_status, $status_array)) {
                $stage['Tense']   = "present";
                $stage['Time']    = date("H:i", strtotime($job_status_date));
                $stage['Date']    = date("d/m/Y", strtotime($job_status_date));
                foreach($TimeLine as &$tl) $tl['Tense'] = 'past';
            } else {
                $status_date = $this->getStatusDate($status_array, $statusHistory);
                if ($status_date != null) {
                    $stage['Tense']   = "past";
                    $stage['Time']    = date("H:i", strtotime($status_date));
                    $stage['Date']    = date("d/m/Y", strtotime($status_date));                   
                }                
            }
            $TimeLine[] = $stage;
            $max_stage = count($TimeLine)-1;
                
            // Site Visit stage
            // ============================================================
            $appointmentDetails   =  $skyline_model->getAppointmentDetails($JobID);
            if(isset($appointmentDetails['AppointmentDate'])) {
                
                $stage = array("Name"=>"Site Visit", "Tense"=>"future", "Date"=>"", "Time"=>"");
                $status_array = array( '06 FIELD CALL ARRANGED',
                                       '30 OUT CARD 0LEFT' );
                
                if (in_array($job_status, $status_array)) {  
                    $stage['Tense']   = "present";
                    $stage['Time']    = $appointmentDetails['AppointmentTime'];
                    $stage['Date']    = date("d/m/Y", strtotime($appointmentDetails['AppointmentDate']));
                    foreach($TimeLine as &$tl) $tl['Tense'] = 'past';                      
                } else {
                    $status_date = $this->getStatusDate($status_array, $statusHistory);
                    if ($status_date != null) {
                        $stage['Tense']   = "past";
                        $stage['Time']    = $appointmentDetails['AppointmentTime'];
                        $stage['Date']    = date("d/m/Y", strtotime($appointmentDetails['AppointmentDate']));                   
                    }                   
                }
                $TimeLine[] = $stage;
                $max_stage = count($TimeLine)-1;
            }
                
            // Job In Progress stage
            // ============================================================
            $stage = array("Name"=>"Job in Progress", "Tense"=>"future", "Date"=>"", "Time"=>"");
            $status_array = array( '07 UNIT IN REPAIR',
                                   '31 WAITING FOR INFO',
                                   '32 INFO RECEIVED',
                                   '08 ESTIMATE SENT',
                                   '09 ESTIMATE ACCEPTED',
                                   '10 ESTIMATE REFUSED',
                                   '11 PARTS REQUIRED',
                                   '18 FIELD CALL COMPLETE' );
                        
            if (in_array($job_status, $status_array)) {   
                $stage['Tense']   = "present";
                //if($job_model->current_record['DateUnitReceived']) {   
                //    $stage['Date']    = date("d/m/Y", strtotime($job_model->current_record['DateUnitReceived']));
                //} else {
                    $stage['Time']    = date("H:i", strtotime($job_status_date));
                    $stage['Date']    = date("d/m/Y", strtotime($job_status_date));                            
                //}
                foreach($TimeLine as &$tl) $tl['Tense'] = 'past';
            } else {
                $status_date = $this->getStatusDate($status_array, $statusHistory);
                if ($status_date != null) {
                    $stage['Tense']   = "past";
                    //if($job_model->current_record['DateUnitReceived']) {   
                    //    $stage['Date']    = date("d/m/Y", strtotime($job_model->current_record['DateUnitReceived']));
                    //} else {
                        $stage['Time']    = date("H:i", strtotime($status_date));
                        $stage['Date']    = date("d/m/Y", strtotime($status_date));                            
                    //}                   
                }                
            }
            $TimeLine[] = $stage;
            $max_stage = count($TimeLine)-1;
                
            // Parts Ordered stage
            // ============================================================
            $stage = array("Name"=>"Parts Ordered", "Tense"=>"future", "Date"=>"", "Time"=>"");
            $status_array = array( '12 PARTS ORDERED',
                                   '33 PART NO LONGER AVAILABLE' );
                        
            if (in_array($job_status, $status_array)) { 
                
                $stage['Tense']   = "present";  
                
                $firstPartOrderDate = $skyline_model->getFirstPartOrderDate($JobID);
                            
                if($firstPartOrderDate!='') {
                    $stage['Time']    = date("H:i", strtotime($firstPartOrderDate));
                    $stage['Date']    = date("d/m/Y", strtotime($firstPartOrderDate));
                }                           
                            
                // Order Due Date.
                $lastPartDueDate = $skyline_model->getLastPartDueDate($JobID);

                if($lastPartDueDate && $lastPartDueDate!='0000-00-00') {    
                    $stage['Date']    = date("d/m/Y", strtotime($lastPartDueDate));
                    $stage['Name']    = "Parts Ordered Delivery Due";
                }
                    
                foreach($TimeLine as &$tl) $tl['Tense'] = 'past';
                
            } else {
                $status_date = $this->getStatusDate($status_array, $statusHistory);
                if ($status_date != null) {
                    $stage['Tense']   = "past";
                    $firstPartOrderDate = $skyline_model->getFirstPartOrderDate($JobID);
                            
                    if($firstPartOrderDate!='') {
                        $stage['Time']    = date("H:i", strtotime($firstPartOrderDate));
                        $stage['Date']    = date("d/m/Y", strtotime($firstPartOrderDate));
                    }                           
                            
                    // Order Due Date.
                    $lastPartDueDate = $skyline_model->getLastPartDueDate($JobID);

                    if($lastPartDueDate && $lastPartDueDate!='0000-00-00') {    
                        $stage['Date']    = date("d/m/Y", strtotime($lastPartDueDate));
                        $stage['Name']    = "Parts Ordered Delivery Due";
                    }                   
                }                
            }      
            $TimeLine[] = $stage;
            $max_stage = count($TimeLine)-1;

                
            // Parts Received stage
            // ============================================================
            $stage = array("Name"=>"Parts Received", "Tense"=>"future", "Date"=>"", "Time"=>"");
            $status_array = array( '13 PARTS RECEIVED',
                                   '14 2ND FIELD CALL REQ.',
                                   '19 DELIVER BACK REQ.' );
            
            if (in_array($job_status, $status_array)) { 
                $stage['Tense']   = "present";
                $lastPartReceivedDate = $skyline_model->getLastPartReceivedDate($JobID);
                if($lastPartReceivedDate && $lastPartReceivedDate!='0000-00-00') {                        
                    if(strlen($lastPartReceivedDate)>10) {    
                        $stage['Time']    = date("H:i", strtotime($lastPartReceivedDate));
                        $stage['Date']    = date("d/m/Y", strtotime($lastPartReceivedDate));
                    } else {
                        $stage['Date']    = date("d/m/Y", strtotime($lastPartReceivedDate));
                    }   
                } else {
                    $stage['Time']    = date("H:i", strtotime($job_status_date));
                    $stage['Date']    = date("d/m/Y", strtotime($job_status_date));                    
                }               
                foreach($TimeLine as &$tl) $tl['Tense'] = 'past';
            } else {
                $status_date = $this->getStatusDate($status_array, $statusHistory);
                if ($status_date != null) {
                    $stage['Tense']   = "past";
                    $lastPartReceivedDate = $skyline_model->getLastPartReceivedDate($JobID);
                    if($lastPartReceivedDate && $lastPartReceivedDate!='0000-00-00') {                        
                        if(strlen($lastPartReceivedDate)>10) {    
                            $stage['Time']    = date("H:i", strtotime($lastPartReceivedDate));
                            $stage['Date']    = date("d/m/Y", strtotime($lastPartReceivedDate));
                        } else {
                            $stage['Date']    = date("d/m/Y", strtotime($lastPartReceivedDate));
                        }   
                    } else {
                        $stage['Time']    = date("H:i", strtotime($status_date));
                        $stage['Date']    = date("d/m/Y", strtotime($status_date));                    
                    }                  
                }                 
            }              
            $TimeLine[] = $stage;
            $max_stage = count($TimeLine)-1;
                
            // Re-visit stage
            // ============================================================
            if(isset($appointmentDetails['AppointmentDate'])) {
                $stage = array("Name"=>"Re-visit", "Tense"=>"future", "Date"=>"", "Time"=>"");
                $status_array = array( '15 2ND FIELD CALL BKD.',
                                       '20 DELIVER BACK ARR.' );
                if (in_array($job_status, $status_array)) {
                    $stage['Tense']   = "present";
                    if($job_model->current_record['RepairCompleteDate']) { 
                        $appointmentDetails   =  $skyline_model->getAppointmentDetails($JobID, $job_model->current_record['RepairCompleteDate']);
                        if(isset($appointmentDetails['AppointmentDate'])) {                            
                            $stage['Time']    = $appointmentDetails['AppointmentTime'];
                            $stage['Date']    = date("d/m/Y", strtotime($appointmentDetails['AppointmentDate']));                                                    
                        }
                    }
                    foreach($TimeLine as &$tl) $tl['Tense'] = 'past';
                } else {
                    $status_date = $this->getStatusDate($status_array, $statusHistory);
                    if ($status_date != null) {
                        $stage['Tense']   = "past";
                        if($job_model->current_record['RepairCompleteDate']) { 
                            $appointmentDetails   =  $skyline_model->getAppointmentDetails($JobID, $job_model->current_record['RepairCompleteDate']);
                            if(isset($appointmentDetails['AppointmentDate'])) {                            
                                $stage['Time']    = $appointmentDetails['AppointmentTime'];
                                $stage['Date']    = date("d/m/Y", strtotime($appointmentDetails['AppointmentDate']));                                                    
                            }
                        }
                    }
                }
                $TimeLine[] = $stage;
                $max_stage = count($TimeLine)-1;
            }
                
            // Job Closed stage
            // ============================================================
            $stage = array("Name"=>"Job Closed", "Tense"=>"future", "Date"=>"", "Time"=>""); 
            $status_array = array( '18 REPAIR COMPLETED',
                                   '21 E-INVOICE SENT',
                                   '22 E-INVOICE RECEIVED',
                                   '23 E-INVOICE REJECTED',
                                   '25 INVOICE PAID',
                                   '26 INVOICE RECONCILED',
                                   '27 JOB ARCHIVED',
                                   '24 RESUBMIT E-INVOICE',
                                   '28 RESUBMIT CLAIM',
                                   '21A E-INVOICE PENDING',
                                   '29 JOB CANCELLED',
                                   '21C INVOICED',
                                   '21W CLAIMED',
                                   '35 EINVOICE PENDING',
                                   '36 EINVOICE REJECTED',
                                   '36 EINVOCIE REJECTED',
                                   '37 EINVOICE RESUBMITTED',
                                   '38 EINVOICE APPROVED' );
                        
            if (in_array($job_status, $status_array)
                    || $job_model->current_record['ClosedDate'] != null) { 
                $stage['Tense']   = "present";                
                if ($job_status == '29 JOB CANCELLED') {
                    $stage['Name'] = 'Cancelled';
                    $stage['Time']    = date("H:i", strtotime($job_status_date));
                    $stage['Date']    = date("d/m/Y", strtotime($job_status_date));                    
                } else {
                    if($job_model->current_record['ClosedDate']) {
                        $stage['Date']    = date("d/m/Y", strtotime($job_model->current_record['ClosedDate']));
                    } else {
                        $stage['Time']    = date("H:i", strtotime($job_status_date));
                        $stage['Date']    = date("d/m/Y", strtotime($job_status_date));    
                    }
                }
                foreach($TimeLine as &$tl) $tl['Tense'] = 'past';
            }
            $TimeLine[] = $stage;
   
            $this->smarty->assign('Timeline', $TimeLine);                   
            //Timeline part ends here..

	    
	    
            //Getting Stats for Timeline bar - starts here..
            $GeneralDefaultModel = $this->loadModel('GeneralDefault');
            //$GeneralDefaultModel->debug = true;
            
            //$job_model->current_record['JobTurnaroundTime'] = $GeneralDefaultModel->getValue( $this->user->DefaultBrandID, 1); //Here 1 is IDNo for the Turnaround Time in general defualt table.
            $job_model->current_record['JobTurnaroundTime'] = $job_model->getTurnaroundTime($job_model->current_record);
	    
            $job_model->current_record['DaysRemaining'] = $job_model->current_record['JobTurnaroundTime']-$job_model->current_record['DaysFromBooking'];
            
            if($job_model->current_record['DaysRemaining']>=0)
            {
                $job_model->current_record['TurnaroundTimeType'] = "";
                if($job_model->current_record['JobTurnaroundTime']>0)
                {    
                    $job_model->current_record['TurnaroundTimeValue'] = round(($job_model->current_record['DaysFromBooking']*100)/$job_model->current_record['JobTurnaroundTime'])."%";
                }
                else
                {
                    $job_model->current_record['TurnaroundTimeValue'] = "0%";
                }
            }   
            else
            {
                $job_model->current_record['TurnaroundTimeType'] = "-RED";
                
                
                $greeenValue = $job_model->current_record['DaysFromBooking']+$job_model->current_record['DaysRemaining'];
                
                if($job_model->current_record['DaysFromBooking']>0)
                {    
                    $job_model->current_record['TurnaroundTimeValue'] = round(($greeenValue*100)/$job_model->current_record['DaysFromBooking'])."%";
                }
                else
                {
                    $job_model->current_record['TurnaroundTimeValue'] = "0%";
                }
            }
            
            
            //Getting Stats for Timeline bar - ends here..
            

            if($JobID != ''){
                if ($this->debug) {
                    $this->log('Job fetch -> data : ' . var_export($job_model->current_record,true));
                    $this->log('Job fetch -> data : ' . var_export($product_details,true));
                }
                /*if(is_null($job_model->current_record['ServiceBaseManufacturer'])) {
                    $this->smarty->assign( 'service_base_manufacturer',
                                        $skyline_model->getFieldFromTable(
                                                    array(
                                                            'want' => 'ManufacturerName', 
                                                            'got' => 'ManufacturerID',
                                                            'from' => 'manufacturer'
                                                            ), 
                                                    $product_details[0]['ManufacturerID']
                                                    )
                                        );
                } else {
                    $this->smarty->assign( 'service_base_manufacturer', $job_model->current_record['ServiceBaseManufacturer'] );
                }*/
                
                $this->smarty->assign( 'service_base_manufacturer', $job_model->current_record['ServiceBaseManufacturer'] );
                $this->smarty->assign( 'service_base_model', $job_model->current_record['ServiceBaseModel'] );
		
                if(is_null($job_model->current_record['ServiceBaseUnitType'])) {
                    $this->smarty->assign( 'service_base_unit_type', $job_model->current_record['UnitTypeName'] );
                } else {
                    $this->smarty->assign( 'service_base_unit_type', $job_model->current_record['ServiceBaseUnitType']);
                }
		
		$deliveryAddress = [   
		    "companyName" => $job_model->current_record["ColAddCompanyName"],
		    "buildingNameNumber" => $job_model->current_record["ColAddBuildingNameNumber"],
		    "street" => $job_model->current_record["ColAddStreet"],
		    "localArea" => $job_model->current_record["ColAddLocalArea"],
		    "townCity" => $job_model->current_record["ColAddTownCity"],
		    "postCode" => $job_model->current_record["ColAddPostcode"],
		    "ColAddEmail" => $job_model->current_record["ColAddEmail"],
		    "ColAddPhone" => $job_model->current_record["ColAddPhone"],
		    "ColAddPhoneExt" => $job_model->current_record["ColAddPhoneExt"]
		];
		$this->smarty->assign("deliveryAddress",$deliveryAddress);
		
		
		$j = $job_model->current_record;
		
		//converting all fields to upper case
		$job_model->current_record = $job_model->toCaps($job_model->current_record);
		
		//echo("<pre>"); print_r($job_model->current_record); echo("</pre>");
		
		//This will assign alternative service provider contact details for this particular client
		$altDetails = $job_model->getALtSCDetails($job_model->current_record["ServiceProviderID"],$job_model->current_record["ClientID"]);
		if($altDetails) {
		    $job_model->current_record["ServiceCentreEmail"] = $altDetails["ContactEmail"];
		    $job_model->current_record["ServiceCentreTelephone"] = $altDetails["ContactPhone"];
		}
		
                $this->smarty->assign($job_model->current_record);
                
		
		$job = $job_model->current_record;
		$authRequired = $job_model->checkRARequired($job["ServiceTypeID"], $job["ManufacturerID"], $job["AuthorisationNo"]);
		$this->smarty->assign("authRequired", $authRequired);
		
		//$serviceTypes = $job_model->getServiceTypes();
                
                $serviceTypes = $skyline_model->getServiceTypes($job['BrandID'], $job['JobTypeID'], $job['NetworkID'], $job['ClientID']);
		$this->smarty->assign("serviceTypes", $serviceTypes);
		
		$courierModel = $this->loadModel("Couriers");
                //$courierModel->debug = true;
                
		$couriers = $courierModel->getBrandCouriers($this->user->DefaultBrandID);
		$this->smarty->assign("couriers", $couriers);
		
                $AuthorisationTypes = $skyline_model->getAuthorisationTypes();
                $this->smarty->assign("AuthorisationTypes", $AuthorisationTypes);
                
                $RAStatusTypesList = $RAStatusTypes_model->getRAStatusTypes($job_model->current_record["RAStatusID"], false);
                $this->smarty->assign("RAStatusTypesList", $RAStatusTypesList);
                
                //Getting count of ra history for given job.
                $RAHistoryNotesCount = $job_model->fetchRaHistory($_POST,$args[0], true);
                $this->smarty->assign("RAHistoryNotesCount", $RAHistoryNotesCount);
		
		$this->smarty->assign("NumRequestAuthorisation", $RAHistoryNotesCount);
		
                //Assign Permissions for page
                $this->smarty->assign('AP7023', Functions::hasPermission($this->user, 'AP7023')); // Display Appointments
                $this->smarty->assign('AP7024', Functions::hasPermission($this->user, 'AP7024')); // Display Invoice Costs
                $this->smarty->assign('AP7025', Functions::hasPermission($this->user, 'AP7025')); // Display Service Reports
                $this->smarty->assign('AP7026', Functions::hasPermission($this->user, 'AP7026')); // Display Parts Use
                $this->smarty->assign('AP7027', Functions::hasPermission($this->user, 'AP7027')); // Display Contact History
                $this->smarty->assign('AP7028', Functions::hasPermission($this->user, 'AP7028')); // Display Status Changes
                $this->smarty->assign('AP7035', Functions::hasPermission($this->user, 'AP7035')); // Display Job Update RA Drop Down
                $this->smarty->assign('AP11005', Functions::hasPermission($this->user, 'AP11005')); // Job update appointment CRUD buttons
                
                
		if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "") {
		    $urlArr = explode("?", $_SERVER['HTTP_REFERER']);
		    if(isset($urlArr[1])) {
			$this->smarty->assign("searchArgs", "?" . $urlArr[1]);
		    } else {
			$this->smarty->assign("searchArgs", "");
		    }
		} else {
		    $this->smarty->assign("searchArgs", "");
		}
		
		if(isset($_GET["ref"])) {
		    $this->smarty->assign("ref", $_GET["ref"]);
		} else {
		    if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "") {
			$urlArr = explode("/", $_SERVER['HTTP_REFERER']);
			end($urlArr);
			$key = key($urlArr);
			if(strstr($urlArr[$key], "?")) {
			    $tmpArr = explode("?", $urlArr[$key]);
			    $ref = $tmpArr[0];
			    $this->smarty->assign("ref", $ref);
			}
		    }
		}
		if(isset($_GET["params"])) {
		    $this->smarty->assign("params", $_GET["params"]);
		}
		
		$unitModel = $this->loadModel("UnitTypes");
		$imeiRequired = $unitModel->isImeiRequired($job["UnitTypeID"]);
		$this->smarty->assign("imeiRequired", $imeiRequired);
		
		$jobUpdateModel = $this->loadModel("JobUpdate");
		
		$conditionCodes = $jobUpdateModel->getConditionCodes($job);
		$this->smarty->assign("conditionCodes", $conditionCodes);
		
		$symptomCodes = $jobUpdateModel->getSymptomCodes($job);
		$this->smarty->assign("symptomCodes", $symptomCodes);
	
		$symptomTypes = $jobUpdateModel->getSymptomTypes($job);
		$this->smarty->assign("symptomTypes", $symptomTypes);
		
		$symptomConditions = $jobUpdateModel->getSymptomConditions($job);
		$this->smarty->assign("symptomConditions", $symptomConditions);
		
		$serviceCategory = $jobUpdateModel->getServiceCategory($job["JobID"]);
		$this->smarty->assign("serviceCategory", $serviceCategory);
		
		$qaQuestions = $jobUpdateModel->getQAQuestions();
		$this->smarty->assign("qaQuestions", $qaQuestions);
		
		$warrantyServiceTypes = $jobUpdateModel->getWarrantyServiceTypes($job);
		$this->smarty->assign("warrantyServiceTypes", $warrantyServiceTypes);
		
		$warrantyDefectTypes = $jobUpdateModel->getWarrantyDefectTypes($job);
		$this->smarty->assign("warrantyDefectTypes", $warrantyDefectTypes);
		
		$serviceAction = $jobUpdateModel->getServiceAction($j);
		$this->smarty->assign("serviceAction", $serviceAction);

		$qaQuestionData = $jobUpdateModel->getQAQuestionData($j["JobID"]);
		$this->smarty->assign("qaQuestionData", $qaQuestionData);

		$completionStatuses = $jobUpdateModel->getCompletionStatuses($j);
		$this->smarty->assign("completionStatuses", $completionStatuses);

		$explanation = $jobUpdateModel->getCompletionStatusExplanation($j);
		$this->smarty->assign("explanation", $explanation);
		
		$engineerHistoryCount = $jobUpdateModel->getRemoteEngineerHistory($j["JobID"]);
		$this->smarty->assign("engineerHistoryCount", count($engineerHistoryCount));
		
                $faultCodeCount = ($job["ConditionCodeID"] ? 1 : 0) + ($job["SymptomCodeID"] ? 1 : 0) + ($job["WarrantyServiceTypeID"] ? 1 : 0) + ($job["WarrantyDefectTypeID"] ? 1 : 0);
		$this->smarty->assign("faultCodeCount", $faultCodeCount);
		
		$inboundFaults = $jobUpdateModel->getInboundFaults($j);
		$this->smarty->assign("inboundFaults", $inboundFaults);
		
		$pendingRA = $jobUpdateModel->getPendingRA($j["JobID"]);
		$this->smarty->assign("pendingRA", $pendingRA);
		
                //checking if user/branch service Provider id is matching job service provider id
                if($this->user->ServiceProviderID!=""&&$job_model->current_record['ServiceProviderID']!=""){
                    if($this->user->ServiceProviderID==$job_model->current_record['ServiceProviderID']){
                        $allowJobPartEdit=true;
                    }
                }
                if(!isset($allowJobPartEdit)){$allowJobPartEdit=false;}
                
                $this->smarty->assign('allowJobPartEdit',$allowJobPartEdit);
                
                $this->smarty->assign('DPDTrackerURL', $this->config['DPD']['tracking']);
//                echo"<pre>";
//                print_r($this->user);
//                echo"</pre>";
                $jobChargeType=$skyline_model->getJobChargeType($JobID);
                        $this->smarty->assign('jobChargeType', $jobChargeType);
		//Display job update or service base template
		if(isset($_GET["appraisal"])) {
		    $this->smarty->display('serviceBase.tpl');
		} else {
		    $this->smarty->display('jobDetails.tpl');
		}
		
            } else {
		
                $this->smarty->display('jobError.tpl');
		
            }

        }
	
    }
    
    
    
    public function jobAdvancedSearchAction( $args ){
        
        
        
        if (isset($this->session->UserID)) {
            
            $page = $this->messages->getPage('jobAdvancedSearch');
            $this->smarty->assign('page', $page);
            $skyline_model = $this->loadModel( 'Skyline' );

            $BranchID = $this->user->BranchID;
           
            $NetworkID  = $this->user->NetworkID;
            $ClientID   = $this->user->ClientID;
                
            
            # when data is post back to function
            if ($_SERVER['REQUEST_METHOD'] == 'POST') 
            {
                if($_POST['NetworkID'])
                {
                    $NetworkID = $_POST['NetworkID'];
                }
                if($_POST['BranchID'])
                {
                    $BranchID = $_POST['BranchID'];
                }
                if($_POST['ClientID'])
                {
                    $ClientID = $_POST['ClientID'];
                }
                
                $searchStr = array(
                   
                    "ContactLastName"=>$_POST['ContactLastName'], 
                    'BuildingNameNumber'=>$_POST['BuildingNameNumber'], 
                    'Street'=>$_POST['Street'], 
                    'TownCity'=>$_POST['TownCity'], 
                    'PostalCode'=>$_POST['PostalCode'],
                    'ColAddPostcode'=>$_POST['ColAddPostcode'],
                    'OlderThan'=>$_POST['OlderThan'],
                    'NewerThan'=>$_POST['NewerThan'],
                    'SystemStatus'=>'',
                    'JobStatus'=>$_POST['JobStatus'],
                    'BrandStatus'=>'',
                    'NetworkID'=>$_POST['NetworkID'],
                    'ClientID'=>$_POST['ClientID'],
                    'BranchID'=>$_POST['BranchID'],
                    'BookingPerson'=>$_POST['BookingPerson'],
                    'ProductNo'=>$_POST['ProductNo'],
                    'ManufacturerID'=>$_POST['ManufacturerID'],
                    'UnitTypeID'=>$_POST['UnitTypeID'],
                    'ModelNumber'=>$_POST['ModelNumber'],
                    'SerialNo'=>$_POST['SerialNo'],
                    'ServiceProviderID'=>$_POST['ServiceProviderID'],
                    'ProductLocation'=>$_POST['ProductLocation']
                   
                    );
                
                $showResultsTables = 1; 
            }
            else
            {
               
            
                $showResultsTables = 0;
                $searchStr = array(

                    "ContactLastName"=>'', 
                    'BuildingNameNumber'=>'', 
                    'Street'=>'', 
                    'TownCity'=>'', 
                    'PostalCode'=>'',
                    'ColAddPostcode'=>'',
                    'OlderThan'=>'',
                    'NewerThan'=>'',
                    'SystemStatus'=>'',
                    'JobStatus'=>'',
                    'BrandStatus'=>'',
                    'NetworkID'=>$NetworkID,
                    'ClientID'=>$ClientID,
                    'BranchID'=>$BranchID,
                    'BookingPerson'=>'',
                    'ProductNo'=>'',
                    'ManufacturerID'=>'',
                    'UnitTypeID'=>'',
                    'ModelNumber'=>'',
                    'SerialNo'=>'',
                    'ServiceProviderID'=>'',
                    'ProductLocation'=>''

                    );

            }
            
            $networksList       = array();
            $clientsList        = array();
            $branchesList       = array();
            $bookingPersonsList = array();
            $serviceProvidersList = array();
            $manufacturerList     = array();
            $jobStatusesList      = array();
            
           
            
            if(!$NetworkID)
            {    
                $networksList     = $skyline_model->getNetworks();
            }
            if($NetworkID)
            {    
                $clientsList     = $skyline_model->getNetworkClients($NetworkID);
            }
            if($ClientID && $NetworkID)
            {
                $branchesList     = $skyline_model->getBranches(null, null, $ClientID, $NetworkID);
            }
            
            $bookingPersonsList   = $skyline_model->getUsers($NetworkID, $ClientID, $BranchID);
            
            
            
            $unitTypeList     = $skyline_model->getUnitTypes(null, $ClientID);
            
            if($NetworkID)
            {
                $manufacturerList = $skyline_model->getManufacturer(null, $NetworkID);
                $serviceProvidersList = $skyline_model->getNetworkServiceProviders($NetworkID);
            }
            
            $productLocations = $skyline_model->getProductLocations();
            
            $jobStatusesList  = $skyline_model->getJobStatus();
            
            
            
            $this->smarty->assign('networksList', $networksList);   
            $this->smarty->assign('clientsList', $clientsList);
            $this->smarty->assign('branchesList', $branchesList);
            $this->smarty->assign('bookingPersonsList', $bookingPersonsList);
            $this->smarty->assign('manufacturerList', $manufacturerList);        
            $this->smarty->assign('unitTypeList', $unitTypeList);        
            $this->smarty->assign('serviceProvidersList', $serviceProvidersList);
            $this->smarty->assign('productLocations', $productLocations);
            $this->smarty->assign('jobStatusesList', $jobStatusesList);
            

            $this->smarty->assign('showResultsTables', $showResultsTables);
            $this->smarty->assign('searchStr', $searchStr);
            $this->smarty->assign('ClientID', $ClientID);
            $this->smarty->assign('NetworkID', $NetworkID);
            $this->smarty->assign('BranchID', $BranchID);
            $this->smarty->assign('Title', $page['Text']['page_title']);
            $this->smarty->assign('user', $this->user);
            
            $this->smarty->display('jobAdvancedSearch.tpl');


            
        }
        else
        {
            $this->redirect('index'); 
        }
       
    }
    
    public function jobConfirmationAction( /* $args */ ) {
        $this->smarty->display('jobConfirmation.tpl');
    }
    
    /*
     * New function to handle all actions regarding product
     * - currently not sure if this is best way of processing actions
     */
    public function productAction( $args ){
       
          
        
        $product_model = $this->loadModel( 'Product' );
        
        $function = isset($args[0]) ? $args[0] : '';
        
        switch($function){
            case 'details':
                
		                    
		$JobTypesList       = array();
		if(isset($this->session->UserID))  
                {
                    $skyline_model = $this->loadModel('Skyline');
                    $JobTypesList = $skyline_model->getJobTypes();
                }
		$this->smarty->assign("JobTypesList",$JobTypesList);
		
		# 2nd args should be ProductID
                $data = $product_model->fetch( $args );
                if(!$data)
                {
                     $this->redirect('index/producterror/',null, null);
                }    
                $this->smarty->assign('Product', $data);

                $this->smarty->display('productDetails.tpl');
                break;
            case 'display_view':

                $product_model->fetch( $args );
                $this->smarty->assign('Product', $product_model);
                # $this->log('default -> product '.var_export($product_model->search_result));
                echo $this->smarty->fetch('app/ViewProduct.tpl');
                break;
            case 'search':
                $this->smarty->assign('productSearch', (isset($_GET['productSearch']) ? $_GET['productSearch'] : ''));
                $this->smarty->assign('productGroup', (isset($_GET['productGroup']) ? $_GET['productGroup'] : '' ));                        
                $this->smarty->display('productMatches.tpl');
                break;
            case 'advanced_search':
                break;
            default:
                # unrecognised action
                header('Location: /index/producterror/');
        }

    }
    
    public function productmatchesAction( /* $args */) {
        $this->smarty->display('ProductMatches.tpl');
    }
    
    public function productErrorAction( /* $args */ ) {
        $this->smarty->display('productError.tpl');
    }

    public function productSearchAction( /*$args*/ ) { 
       
        $productNoResultsFlag = false;
        $product_model = $this->loadModel( 'Product' );
        
        
        $this->smarty->assign('Product', $product_model);
        
        #$this->log('defeault->productSearchAction post var : ' . var_export($_POST, true));
        $this->smarty->assign('productSearch', (isset($_GET['productSearch']) ? $_GET['productSearch'] : ''));
        
        
        $product_search_results = $product_model->search($_GET);
        
        if(isset($product_search_results['iTotalRecords']) && $product_search_results['iTotalRecords']=='1' && isset($product_search_results['aaData'][0][0]))
        {
            $this->redirect('index/product/details/'.$product_search_results['aaData'][0][0]); 
        }
        else  if($product_search_results['iTotalRecords']=='0')
        {
            $productNoResultsFlag = true;
        } 
        
        if (isset($this->session->UserID) && $productNoResultsFlag)
        {
            $skyline_model = $this->loadModel( 'Skyline' );
            $JobTypesList = array();
            $JobTypesList = $skyline_model->getJobTypes();
            $this->smarty->assign('JobTypesList', $JobTypesList);
        }
        
        $this->smarty->assign('productNoResultsFlag', $productNoResultsFlag);
        $this->smarty->display('ProductMatches.tpl'); 
        
        
        /*************
        
        //$this->smarty->assign('name','Argos Northampton');
        //$this->smarty->assign('last_logged_in',date("jS F Y G:i",time()));    
        
        $product_model = $this->loadModel( 'Product' );

        #$this->log('defeault->productSearchAction post var : ' . var_export($_POST, true));
        $this->smarty->assign('productSearch', (isset($_POST['productSearch']) ? $_POST['productSearch'] : ''));
        
        //$this->smarty->assign('productGroup', (isset($_POST['productGroup']) ? $_POST['productGroup'] : 'ProductNo' ));             
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST') { 
            
            #updated fn
          //  $product_model->search( $_POST );
            
            
           // $this->smarty->assign('Product', $product_model);
            
            if(false && $product_model->count() == 0){
                # No product found
                header('Location: /index/productError');
            }else if($product_model->count() == 1){
                # Single product found
                # $this->smarty->display('productDetails.tpl');
                # $this->log('search result '. var_export($product_model->search_result, true) );
                $result = $product_model->search_result;
                $id = $result[0]['CatalogueItemID'];
                header("Location: /index/product/details/$id/");
            }else{
                # Multiple product found         
                $this->smarty->display('ProductMatches.tpl'); 
            } 
        }else{
            # empty search
            $this->smarty->assign('Product', $product_model);
            $this->smarty->display('ProductMatches.tpl'); 
        }        
     
        
        ***********************/
 
    }    
    
    public function productAdvancedSearchAction( /*$args*/ ) {
        
        $page = $this->messages->getPage('productAdvancedSearch');
        $this->smarty->assign('page', $page);
        $skyline_model = $this->loadModel( 'Skyline' );
        $productNoResultsFlag = false;
        
        if(isset($this->user->DefaultBrandID))
        {
            $BrandID = $this->user->DefaultBrandID;
            
        } 
        else 
        {
            
            $BrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
        }
        
        $NetworkID = 0;
        $ClientID  = 0;
        if($BrandID)
        {
           $BrandDetails  = $skyline_model->getBrand($BrandID);
           $NetworkID     = isset($BrandDetails[0]['NetworkID'])?$BrandDetails[0]['NetworkID']:0;
           $ClientID      = isset($BrandDetails[0]['ClientID'])?$BrandDetails[0]['ClientID']:0;
        }
        
       
        //If the user is logged in then we are getting manufacturers and unit types based on network and client.
        if($NetworkID)
        {    
            $manufacturerList = $skyline_model->getManufacturer($NetworkID);
        }
        else
        {
            $manufacturerList = $skyline_model->getManufacturer();
        }
        
        if($ClientID)
        {
            $unitTypeList  = $skyline_model->getUnitTypes(null, $ClientID);
        }
        else
        {
            $unitTypeList  = $skyline_model->getUnitTypes();
        }
        
        
      //  $manufacturerList = array_merge(array('' => 'Please select manufacturer'), $skyline_model->getManufacturer('alpha'));
      //  $productTypeList = array_merge(array('' => 'Please select product type'), $skyline_model->getUnitTypes('alpha'));
        
        
       // $this->log('manufact advance search using : ' . var_export($manufacturerList, true));
       // $this->log('product advance search using : ' . var_export($productTypeList, true));
        
        
        #$this->log('product advance search using : ' . var_export($productTypeList, true));

        $this->smarty->assign('manufacturerList', $manufacturerList);        

        $this->smarty->assign('unitTypeList', $unitTypeList);        
        
        # Define which value is selected
        //$this->smarty->assign('selectManufacturer', '');
        //$this->smarty->assign('selectProductType', '');
        
        # when data is post back to function
            if ($_SERVER['REQUEST_METHOD'] == 'POST') 
            {
            
                $searchStr = array("ProductNo"=>$_POST['ProductNo'], 'ModelDescription'=>$_POST['ModelDescription'], 'ManufacturerID'=>$_POST['ManufacturerID'], 'UnitTypeID'=>$_POST['UnitTypeID'], 'ModelNumber'=>$_POST['ModelNumber']);
                
           /*
                $product_model = $this->loadModel( 'Product' );

                $product_model->advancedSearch( $_POST );
                $this->smarty->assign('Product', $product_model);

                $this->smarty->assign('displayColumns', $product_model->display_columns);

                $this->smarty->assign('selectManufacturer', $_POST['ManufacturerName']);
                $this->smarty->assign('selectProductType', $_POST['UnitTypeName']);


                #obsolete count fn as 
                if($product_model->count() == 0){
                    # No product found
                    header('Location: /index/producterror');
                }else if($product_model->count() == 1){
                    # Signle product found
                    # $this->smarty->display('productDetails.tpl');

                    header('Location: /index/product/detail/' );
                }else{
                    # Multiple product found
                    $this->smarty->assign('Search', $_POST);
                    //$_POST['length'] = count($_POST);
                    $this->smarty->assign('SearchJSON', json_encode($_POST));
                    $this->smarty->display('productAdvancedSearch.tpl'); 
                }              */
                
                $showResultsTables = 1; 
                
                $product_model = $this->loadModel( 'Product' );
                $product_search_results = $product_model->advancedSearch($_POST);
        
                if(isset($product_search_results['iTotalRecords']) && $product_search_results['iTotalRecords']=='1' && isset($product_search_results['aaData'][0][0]))
                {
                    $this->redirect('index/product/details/'.$product_search_results['aaData'][0][0]); 
                }
                else  if($product_search_results['iTotalRecords']=='0')
                {
                    $productNoResultsFlag = true;
                    $showResultsTables = 0;
                } 
               
              
                
        }
        else
        {
            
            $showResultsTables = 0;
            $searchStr = array("ProductNo"=>'', 'ModelDescription'=>'', 'ManufacturerID'=>'', 'UnitTypeID'=>'', 'ModelNumber'=>'');
             
        }
        
        if (isset($this->session->UserID) && $productNoResultsFlag)
        {
            $JobTypesList = array();
            $JobTypesList = $skyline_model->getJobTypes();
            $this->smarty->assign('JobTypesList', $JobTypesList);
        }
        
        $this->smarty->assign('productNoResultsFlag', $productNoResultsFlag);
        $this->smarty->assign('showResultsTables', $showResultsTables);
        $this->smarty->assign('searchStr', $searchStr);
        $this->smarty->assign('ClientID', $ClientID);
        $this->smarty->assign('NetworkID', $NetworkID);
        $this->smarty->assign('Title', $page['Text']['page_title']);
        
        $this->smarty->display('productAdvancedSearch.tpl');
        
        
    }
    
    
    
    
    
    
    
    
    

    public function jobMatchesAction( /* $args */) {
        $this->smarty->display('JobMatches.tpl');
    }
    
    public function openJobMatchesAction( $args ) {
        #$this->log('DefaultController -> openJobMatchesAction | args :  ' . var_export($_POST, true) );
        
        $this->smarty->assign('jobGroup', ( isset($_POST['jobGroup']) ? $_POST['jobGroup'] : 'JobID'));
        
        $job_model = $this->loadModel('Job');
        $job_model->search_type = 'open';
        
        if(isset($_POST['jobGroup'])){
            $job_model->search( $_POST );
        }
        
        $this->smarty->assign('Job', $job_model);
        
        $this->smarty->display('OpenJobMatches.tpl');
    }  
    
    
    
    
    
    /**
     * printEmailAction
     *  
     * Deal withs printing e-mail for a specific job
     * 
     * 
     * @param array $args   Arguments passed in the URL
     *                      First argument should be JobID
     * @return  nothing
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     * 
     **************************************************************************/ 
    
    public function printEmailAction ($args) {
        
        $JobID = isset($args[0]) ? $args[0] : '';                               /* First argument should be JobID */
        $err = 0;

        $job_model = $this->loadModel('Job');
        $email_model = $this->loadModel('Email'); 
                        
        $job_model->fetch( $JobID );
        
         if(isset($_POST['SentEmailID']) && $_POST['SentEmailID'] && $_POST['PrintEmailFlag'])
         {    
	 
            $sentEmailDetails =  $email_model->fetchLastSentEmail($_POST['SentEmailID'], $JobID);

            if(count($sentEmailDetails))
            {    
                echo $sentEmailDetails['MailBody'];
                echo '<script type="text/javascript" >

                        

                            window.print();

                       

                    </script>';
            }    
         }
    }
    
    
    
    

    /**
     * sendemailAction
     *  
     * Deal withs ending e-mail for a specific job
     * 
     * 
     * @param array $args   Arguments passed in the URL
     *                      First argument should be JobID
     * @return  nothing
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     * 
     **************************************************************************/ 
    
    public function sendemailAction ($args) {
        $JobID   = isset($args[0]) ? $args[0] : '';                               /* First argument should be JobID */
        $errText = isset($args['errText']) ? $args['errText'] : false; 
        
        $err = 0;

        $job_model = $this->loadModel('Job');
        $email_model = $this->loadModel('Email'); 
                        
        $job_model->fetch( $JobID );
        
        if(isset($_POST['EmailID']) && $_POST['EmailID']) {
            
            $EmailID = $_POST['EmailID'];
            
            if (is_numeric($EmailID)) {
                
                $email_already_sent = false;
                $email_result = false;
                if ( ! $email_model->hasEmailBeenSent($JobID, $EmailID) ) 
                {     /* Check if e-mail has been previously sent */
                    
                    $email_record = $email_model->fetchRow(array('EmailID' => $EmailID)); /* Get the details of the selected e-mail */
                     
                    
                    if(isset($email_record['EmailCode']) && $email_record['EmailCode'])
                    {
                            $JobRecord   = $job_model->current_record;
                            
                            //$this->log($JobRecord);
                            
                            
                            $emails = []; 
                            if($email_record['EmailCode']=="job_booking" || $email_record['EmailCode']=="job_booking_crm" || $email_record['EmailCode']=="job_booking_smart_service_tracker")
                            {
                                
                            
                                    $MailSubject     =  $email_record['Title'];
                                    $MailBody        =  $email_record['Message'];
                                    $MailFromName    =  $this->config['SMTP']['FromName'];
                                    $MailFromEmail   =  $this->config['SMTP']['From'];
                                    $ToEmail         =  $job_model->current_record['ContactEmail'];
                                    $ReplyEmail      =  $this->config['SMTP']['Reply'];
                                    $ReplyName       =  $this->config['SMTP']['ReplyName'];
                                    $BCCFlag         =  false;

                                    $MailAccessCode  = md5($JobID.$EmailID);

                                    if($ToEmail)
                                    {  
                                       
                                        $JobRecord['MailAccessCode'] = $MailAccessCode;


                                            $MailSubject   = trim($job_model->current_record['Brand']." ".$email_record['Title']);


                                            $MailBody      = $email_model->prepareJobBookingEmailBody($email_record['Message'], $MailSubject, $JobRecord);

                                            if($job_model->current_record['ServiceCentreName'])
                                            {
                                                $MailFromName  = $job_model->current_record['ServiceCentreName']; 
                                                $ReplyName     = $job_model->current_record['ServiceCentreName']; 
                                            }

                                            if($job_model->current_record['ServiceCentreEmail'])
                                            {
                                                $MailFromEmail  = $job_model->current_record['ServiceCentreEmail']; 

                                            }

                                            if($job_model->current_record['ServiceReplyEmail'])
                                            {
                                                $ReplyEmail  = $job_model->current_record['ServiceReplyEmail']; 

                                            }

                                            $BCCFlag = true;



                                        $cc_email = false;

                                        $email_result = $email_model->Send(
                                                       $MailFromName,
                                                       $MailFromEmail,
                                                       $ToEmail,
                                                       $MailSubject,
                                                       $MailBody,
                                                       $cc_email,	    //cc email
                                                       $ReplyEmail,
                                                       $ReplyName,
                                                       $BCCFlag
                                                      );

                                         if ($email_result)
                                         {    
                                            $email_details = array('mailSubject'=>$MailSubject, 'replyEmail'=>$ReplyEmail, 'replyName'=>$ReplyName,  'EmailID'=>$EmailID, 'toEmail'=>$ToEmail, 'ccEmails'=>$cc_email, 'bccEmails'=>NULL);


                                            $email_model->markEmailSent($JobID, $email_details, $MailBody, $MailAccessCode);
                                            /* Mark the e-mail as sent */
                                            $err = -1;
                                         } 

                                    }
                             } 
                             else if($email_record['EmailCode']=="ra_required_client") 
                             {
                                   $raRequired = $job_model->checkRARequired( empty($JobRecord['ServiceTypeID']) ? null : $JobRecord['ServiceTypeID'], 
                                                      empty($JobRecord['ManufacturerID']) ? null : $JobRecord['ManufacturerID'], 
                                                      empty($JobRecord['AuthorisationNo']) ? null : $JobRecord['AuthorisationNo'] );     
            
                                    if($JobRecord['ContactEmail'] && $JobRecord['ServiceProviderID'] && $raRequired) 
                                    {
                                        $emails[] = $email_model->fetchRow(['EmailCode' => "ra_required_client"]);
                                        end($emails);
                                        $key = key($emails);
                                        $emails[$key]["toEmail"] = $JobRecord["ContactEmail"];
                                        $emails[$key]["fromName"] = "Skyline";
                                        $emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
                                        $emails[$key]["replyEmail"] = ($JobRecord['ServiceReplyEmail']) ? $JobRecord['ServiceReplyEmail'] : false;
                                        $emails[$key]["replyName"] = ($JobRecord['ServiceCentreName']) ? $JobRecord['ServiceCentreName'] : false;
                                        $emails[$key]["ccEmails"] = false;
                                        
                                        $err = -1;
                                    }
                             }
                             else if($email_record['EmailCode']=="unable_to_authorise_repair" || $email_record['EmailCode']=='query_raised_against_authorisation')
                             {
                                 
                                 $email_model->sendAuthorisationEmail($JobRecord['JobID'], $email_record['EmailCode'], false, false, false);
                                 
                                 $err = -1;
                                  
                             }    
                             else if($email_record['EmailCode']=="email_service_instruction" &&  $JobRecord['SPEmailServiceInstruction']=='Active' && ($JobRecord['SPAdminSupervisorEmail'] || $JobRecord['SPServiceManagerEmail']) && $JobRecord['ServiceProviderID']) 
                             {

                                $email_service_instruction  =  $email_model->fetchRow(['EmailCode' => "email_service_instruction"]);

                                //$this->log($email_service_instruction);

                                if(is_array($email_service_instruction) && count($email_service_instruction)>0)
                                {    
                                    $emails[] = $email_service_instruction;
                                    end($emails);
                                    $key = key($emails);


                                    $sp_send_email1 = ($JobRecord['SPAdminSupervisorEmail'])?$JobRecord['SPAdminSupervisorEmail']:false;
                                    $sp_send_email2 = false;
                                    if(!$sp_send_email1)
                                    {
                                      $sp_send_email1 =  $JobRecord['SPServiceManagerEmail']; 
                                    }
                                    else if($JobRecord['SPServiceManagerEmail'])
                                    {
                                        $sp_send_email2 = $JobRecord['SPServiceManagerEmail'];
                                    }    

                                    $emails[$key]["toEmail"] = $sp_send_email1;
                                    $emails[$key]["fromName"] = "Skyline";
                                    $emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
                                    $emails[$key]["replyEmail"] = false;
                                    $emails[$key]["replyName"] = false;
                                    $emails[$key]["ccEmails"] = $sp_send_email2;
                                    
                                    $err = -1;

                               }
                            }
                            else if($email_record['EmailCode']=="cancelled_job_notification" && ($JobRecord['StatusName']=='29 JOB CANCELLED' || $JobRecord['StatusName']=='29A JOB CANCELLED - NO CONTACT'))
                            {
                                
                                $sp_email1 = false;
                                $sp_email2 = false;

                                $sp_email1 = $JobRecord['SPAdminSupervisorEmail'];

                                if($sp_email1)
                                {
                                    $sp_email2 = $JobRecord['SPServiceManagerEmail'];
                                }
                                else
                                {
                                    $sp_email1 = $JobRecord['SPServiceManagerEmail'];
                                }

                                 if($sp_email1 && $JobRecord['ServiceProviderID']) {

                                    
                                    $cancelled_job_notification  = $email_model->fetchRow(['EmailCode' => "cancelled_job_notification"]);

                                    if(is_array($cancelled_job_notification) && count($cancelled_job_notification)>0)
                                    {    
                                        $emails[] = $cancelled_job_notification;
                                        end($emails);
                                        $key = key($emails);
                                    
                                        
                                        $emails[$key]["toEmail"] = $sp_email1;
                                        $emails[$key]["fromName"] = "Skyline";
                                        $emails[$key]["fromEmail"] = "no-reply@skylinecms.co.uk";
                                        $emails[$key]["replyEmail"] = false;
                                        $emails[$key]["replyName"] = false;
                                        $emails[$key]["ccEmails"] = ($sp_email2)?$sp_email2:false;
                                        
                                        $err = -1;
                                    }
                                }

                            }
                            else if( 
                                    (
                                        $email_record['EmailCode']=="appointment_booked" || 
                                        $email_record['EmailCode']=="appointment_cancelled" ||
                                    
                                        $email_record['EmailCode']=="appointment_reminder" ||
                                        $email_record['EmailCode']=="appointment_reminder_crm" ||
                                        $email_record['EmailCode']=="appointment_reminder_smart_service_tracker" ||
                                    
                                        $email_record['EmailCode']=="appointment_date_confirmed" ||
                                        $email_record['EmailCode']=="appointment_date_confirmed_crm" ||
                                        $email_record['EmailCode']=="appointment_date_confirmed_smart_service_tracker" 
                                    
                                    ) 
                                    && $JobRecord['ServiceProviderID'])
                            {
                                 $appointment_model = $this->loadModel('Appointment'); 
                                 
                                 $AppointmentInfo = $appointment_model->getNextAppointment($JobRecord['JobID']);
                                 
                                 if(count($AppointmentInfo))
                                 {   
                                     $serviceProviders_model = $this->loadModel('ServiceProviders'); 
                                     $spInfoResult =  $serviceProviders_model->fetchRow(array('ServiceProviderID'=>$AppointmentInfo['ServiceProviderID']));
                                     
                                     if(count($spInfoResult))
                                     {    
                                        if($email_record['EmailCode']=="appointment_booked" || $email_record['EmailCode']=="appointment_cancelled")
                                        {    
                                            $eCode = 'Booked'; 
                                            if($email_record['EmailCode']=="appointment_cancelled")
                                            {
                                                $eCode = 'Cancelled'; 
                                            }    
                                            $email_model->sendAppointmentEmail($AppointmentInfo['AppointmentID'], $eCode, $spInfoResult['ServiceManagerEmail'], $spInfoResult['AdminSupervisorEmail'], $spInfoResult['DiaryAdministratorEmail'], false);
                                        }
                                        else 
                                        {    
                                            $eCode = 'reminder'; 
                                            if( $email_record['EmailCode']=="appointment_date_confirmed" ||
                                                $email_record['EmailCode']=="appointment_date_confirmed_crm" ||
                                                $email_record['EmailCode']=="appointment_date_confirmed_smart_service_tracker" )
                                            {
                                                $eCode = 'date_confirmed'; 
                                            }    
                                            $email_model->sendAppointmentDetailsEmail($AppointmentInfo['AppointmentID'], $eCode, false, false, false);
                                        }
                                        
                                        $err = -1;
                                       
                                     }   
                                 }
                                 
                                  
                            }  
                            else if($email_record['EmailCode']=="one_touch_job_cancellation")
                            {


                            }
                            else if($email_record['EmailCode']=="job_complete" && $JobRecord['ClosedDate'] && $JobRecord['ClosedDate']!='0000-00-00')
                            {
                                $email_model->sendJobCompleteEmail($JobRecord['JobID'], false, false, false);
                                $err = -1;
                            }
                            else if($email_record['EmailCode']=="appraisal_request" || $email_record['EmailCode']=="appraisal_request_crm" || $email_record['EmailCode']=="appraisal_request_smart_service_tracker")
                            {
                                $email_model->sendAppraisalRequestEmail($JobRecord['JobID'], false, false, false);
                                $err = -1;
                            }
                            
                             
                             
                             
                             foreach($emails as $email) {

                                $email['notCheckBrandFlag'] = true;  
                                 
                                $email_model->processEmail($email, $JobRecord);
                                
                               // $this->log('ddddd3333333dd');
                                
                                //$email_model->markEmailSent($JobID, $email, $MailBody, $MailAccessCode);

                            }
                    }
                    
                }
                else {
                    $email_already_sent = true;
                }
                               
                
                
                if ($email_result || $err==-1) {
                    $err = -1;
                }
                else if ($email_already_sent)
                {
                    $err = 3;
                }    
                else {
                    $err = 2;
                }
            } else {
                $err = 1;                                                       /* No e-mail has been selected */   
            }
        }
        else if(isset($_POST['SentEmailID']) && $_POST['SentEmailID'])
        {
           $sentEmailDetails =  $email_model->fetchLastSentEmail($_POST['SentEmailID'], $JobID);
           
           $err = 2;
           
           if(count($sentEmailDetails))
           { 
               if( $sentEmailDetails["ToEmail"] && !$_POST['PrintEmailFlag'])
               {
                   $emailResult = $email_model->Send(
                     $sentEmailDetails["FromName"],	    //from name
                     $sentEmailDetails["FromEmail"],    //from email
                     $sentEmailDetails["ToEmail"],	    //to email
                     $sentEmailDetails['MailSubject'],	    //subject
                     $sentEmailDetails['MailBody'],		    //body
                     $sentEmailDetails["CCEmails"],	    //cc emails
                     $sentEmailDetails["ReplyEmail"],   //reply email
                     $sentEmailDetails["ReplyName"],    //reply name
                     true,		    //bcc flag,
                     $sentEmailDetails['BCCEmails']  //bcc emails    
                   );
                   $err = -1;
               }
                    
           }
        }
        if($err)
        {
            $this->redirect('index/sendemail/'.$JobID."/errText=".$err);
        }  

        $page = $this->messages->getPage('Job', $this->lang);
        $this->smarty->assign('page', $page);        

        $this->smarty->assign( $job_model->current_record );
        $this->smarty->assign('FullAddress', $job_model->getFullAddress($JobID));  
        $this->smarty->assign('errText', $errText);  
        
        $this->smarty->assign('PredefinedEmails',$email_model->getPredefinedEmail($JobID, $job_model->current_record['BrandID']));
        $this->smarty->assign('SentPredefinedEmails',$email_model->getSentPredefinedEmail($JobID, $job_model->current_record['BrandID']));

        $this->smarty->display('jobEmail.tpl');
        
        
        
        
        
//        switch ($err) {
//            case -1:                                                            /* Success in sending email */
//                echo '<script type="text/javascript">
//                    
//                        alert("Email has been sent");
//                    
//                    </script>';
//                break;
//            
//            /*case 0: - first display of page so no need to do anything */
//            
//            case 1:                                                             /* No email was selected */
//                echo '<script type="text/javascript">alert("Please select an email to send");</script>';
//                break;
//            
//            case 2:                                                            /* Fail in sending email */
//                echo '<script type="text/javascript">alert("Unable to send email");</script>';
//                break;
//            
//            case 3:                                                            /* Email has sent already. */
//                echo '<script type="text/javascript">alert("Email has been sent already.");</script>';
//                break;
//        }
    }
    
    
    
    
    
    
    /**
     * canceljobAction
     *  
     * Deals with cancelling a specific job
     * 
     * 
     * @param array $args   Arguments passed in the URL
     *                      First argument should be JobID
     * @return  nothing
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     * 
     **************************************************************************/ 
    
    public function canceljobAction ($args) {
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST') 
        {
            $job_model     = $this->loadModel('Job');
            $skyline_model = $this->loadModel('Skyline');
            $statusID      = $skyline_model->getStatusID('29 JOB CANCELLED');
            
            $result = $job_model->update([
			"CompletionStatus" => $_POST['CompletionStatus'], 
			"JobID" => $_POST['JobID'], 
			"CancelReason" => $_POST['CancelReason'],
                        'StatusID' => ($statusID == '') ? NULL : $statusID
		    ]);
            
            
            if($result['status']=='FAIL')
            {
                $this->log("Cancel Job Update Error:");
                $this->log($result);
            }
            else
            {
                
                $_POST['ContactHistoryActionID'] = 17;//CANCEL REQUEST action id.
                $model = $this->loadModel('ContactHistory');
                
                
                $NoteText = $_POST['CompletionStatus']."\r\n".'Details:'.$_POST['CancelReason'];
            
                $data = [   'JobID' => $_POST['JobID'],
                            'ContactHistoryActionID' => $_POST['ContactHistoryActionID'],
                            'BrandID' => $this->user->DefaultBrandID,
                            'ContactDate' => date('Y-m-d'), 
                            'ContactTime' => date('H:i:s'),
                            'UserCode' => $this->user->UserID,
                            'Note' => $NoteText, 
                            'Subject' => $_POST['CompletionStatus'],
                            'SystemAllocated'=>'yes'
                        ];

                $ch_result = $model->create($data);
                
                
                //Create Status history Record
                $status_history = $this->loadModel('StatusHistory');
                $status_history->create([   'JobID' => $_POST['JobID'],
                                            'StatusID' => $statusID,
                                            'UserID' => $this->user->UserID,
                                            'Date' => date('Y-m-d H:i:s') 
                                        ]);
                
               $audit_actionID = $skyline_model->getAuditTrailActionID(AuditTrailActions::JOB_CANCELLED);  
               
                //Create Audit Record
                $audit = $this->loadModel('Audit');
                $audit->create(['JobID' => $_POST['JobID'],
                                'AuditTrailActionID' => $audit_actionID,
                                'Description' => ''
                               ]);
                
                
                
                 $job_model->fetch($_POST['JobID']);

		 $cur = $job_model->current_record;
                 
                 if($cur['ServiceProviderID'] && $cur['ServiceProviderID']!='')
                 {    
                    $api = $this->loadModel('APIJobs');
                    $ServiceProvidersModel   = $this->loadModel('ServiceProviders');
                    $ServiceProvidersDetails = $ServiceProvidersModel->fetchRow(array("ServiceProviderID"=>$cur['ServiceProviderID']));
                    
                    
                    
                    $rmaResponse = $api->rmaPutNewJob($_POST['JobID']); 


                    if($rmaResponse && $rmaResponse=="SC0001")//If Rma traker call responds with success.
                    {    
                        if($ServiceProvidersDetails['Platform']=="ServiceBase")
                        {
                            $api->putContactHistory($ch_result['contactHistoryId']); 
                            $job_model->update(array("DownloadedToSC"=>1, "JobID"=>$_POST['JobID']));
                        }
                        else if($ServiceProvidersDetails['Platform']=="API") {

                            $job_model->update(array("DownloadedToSC"=>0, "JobID"=>$_POST['JobID']));

                        }
                        else if($ServiceProvidersDetails['Platform']=="Skyline" || $ServiceProvidersDetails['Platform']=="RMA Tracker") {

                            $job_model->update(array("DownloadedToSC"=>1, "JobID"=>$_POST['JobID']));
                        }
                    }
                   
                    
                    //Sending email to service provider - starts here..
                    $sp_email1 = false;
                    $sp_email2 = false;
                    
                    $sp_email1 = $cur['SPAdminSupervisorEmail'];
                    
                    if($sp_email1)
                    {
                        $sp_email2 = $cur['SPServiceManagerEmail'];
                    }
                    else
                    {
                        $sp_email1 = $cur['SPServiceManagerEmail'];
                    }
                    
                     if($sp_email1 && $cur['ServiceProviderID']) {
                         
                        $emailModel = $this->loadModel('Email'); 
				
                        $cancelled_job_notification  = $emailModel->fetchRow(['EmailCode' => "cancelled_job_notification"]);

                        if(is_array($cancelled_job_notification) && count($cancelled_job_notification)>0)
                        {    
                            $cancelled_job_notification["toEmail"] = $sp_email1;
                            $cancelled_job_notification["fromName"] = "Skyline";
                            $cancelled_job_notification["fromEmail"] = "no-reply@skylinecms.co.uk";
                            $cancelled_job_notification["replyEmail"] = false;
                            $cancelled_job_notification["replyName"] = false;
                            $cancelled_job_notification["ccEmails"] = ($sp_email2)?$sp_email2:false;
                            
                            
                            
                            $emailModel->processEmail($cancelled_job_notification, $cur);

                        }
                    }
                    //Sending email to service provider - ends here..
                 }   
                
            }
            
            echo json_encode($result);
        }
        else
        {  
            $JobID = isset($args[0]) ? $args[0] : false;                               /* First argument should be JobID */

            if($JobID)
            {    
                $page = $this->messages->getPage('cancelJob', $this->lang);
                $this->smarty->assign('page', $page);  

                $job_model   = $this->loadModel('Job');

                $job_model->fetch( $JobID );

                $this->smarty->assign( $job_model->current_record );


                $reasonsArray =  array(

                    "DUPLICATE JOB",
                    "EXCHANGED",
                    "PARTS ISSUE",
                    "REQUEST BY CUSTOMER",
                    "RESCHEDULE",
                    "TIME DELAY",
                    "COST ISSUE",
                    "WRONG ASC",
                    "WRONG MANUFACTURER"


                );

                $this->smarty->assign('reasonsArray', $reasonsArray);

                $this->smarty->display('cancelJob.tpl');
            }
        }
    }
    
    
    
    
    
     /**
     * view mail 
     *  
     * Displays the email body.
     * 
     * 
     * @param array $args   Arguments passed in the URL
     *                      First argument should be MailAccessCode
     *              
     * 
     * @return  nothing but prints the email body if it finds.
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     * 
     **************************************************************************/ 
    
    public function viewEmailAction ($args) {
        
        
        
        $MailAccessCode = isset($args[0]) ? $args[0] : false;
        
        if($MailAccessCode)
        {
            $email_model = $this->loadModel('Email'); 

            $email_record = $email_model->fetchUserEmail($MailAccessCode); /* Get email body */
            
            if($email_record['MailBody'])
            {
                print $email_record['MailBody'];
            } 
            else
            {
                print "No mail found.";
            }
        }
        else
        {
            print "Mail access code is missing.";
        }
    }
    
    
    
    
     /**
     * Time out  
     *  
     * Displays time out error page.
     * 
     *              
     * 
     * @return  nothing but prints html form.
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     * 
     **************************************************************************/ 
    
    public function timeOutAction ( /*$args*/ ) {
        
         echo $this->smarty->fetch('timeOut.tpl');
        
    }

    
    
    // CSV job upload function
    // Used by AJAX only
    // 2012-10-04 © Vic v.rutkunas@pccsuk.com
    
    public function csvUploadAction() {
	
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	//Settings
	//$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
	$targetDir = APPLICATION_PATH . "/uploads/csv";

	$cleanupTargetDir = true; // Remove old files
	$maxFileAge = 3600 * 24 * 31; // Temp file age in seconds (1 month)

	//10 minutes execution time
	@set_time_limit(10 * 60);

	//Get parameters
	$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
	$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
	$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

	//Clean the fileName for security reasons
	$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

	//Make sure the fileName is unique but only if chunking is disabled
	if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
	    $ext = strrpos($fileName, '.');
	    $fileName_a = substr($fileName, 0, $ext);
	    $fileName_b = substr($fileName, $ext);

	    $count = 1;
	    while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b)) {
		$count++;
	    }

	    $fileName = $fileName_a . '_' . $count . $fileName_b;
	}

	$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

	//Create target dir
	if (!file_exists($targetDir))
	    @mkdir($targetDir);

	//Remove old temp files	
	if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
	    while (($file = readdir($dir)) !== false) {
		$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

		//Remove temp file if it is older than the max age and is not the current file
		if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
			@unlink($tmpfilePath);
		}
	    }

	    closedir($dir);
	    
	} else {
	    
	    die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	    
	}


	//Look for the content type header
	if (isset($_SERVER["HTTP_CONTENT_TYPE"])) {
	    $contentType = $_SERVER["HTTP_CONTENT_TYPE"];
	}

	if (isset($_SERVER["CONTENT_TYPE"])) {
	    $contentType = $_SERVER["CONTENT_TYPE"];
	}

	//Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
	if (strpos($contentType, "multipart") !== false) {
	    if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
		//Open temp file
		$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
		if ($out) {
		    //Read binary input stream and append it to temp file
		    $in = fopen($_FILES['file']['tmp_name'], "rb");
		    if ($in) {
			while ($buff = fread($in, 4096)) {
			    fwrite($out, $buff);
			}
		    } else {
			die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		    }
		    fclose($in);
		    fclose($out);
		    @unlink($_FILES['file']['tmp_name']);
		} else {
		    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
	    } else {
		die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
	    }
	} else {
	    //Open temp file
	    $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
	    if ($out) {
		//Read binary input stream and append it to temp file
		$in = fopen("php://input", "rb");
		if ($in) {
		    while ($buff = fread($in, 4096)) {
			fwrite($out, $buff);
		    }
		} else {
		    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		}
		fclose($in);
		fclose($out);
	    } else {
		die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
	    }
	}

	//Check if file has been uploaded
	if (!$chunks || $chunk == $chunks - 1) {
	    //Strip the temp .part suffix off 
	    rename("{$filePath}.part", $filePath);
	}

	
	//CSV content processing starts here **********************************************************************************

	//VALIDATION
	
	//status 0 = general error.
	//status 1 = process completed.
	//status 2 = csv file headers doesn't match requirements.
	//status 3 = csv contents doesn't match requirements.
	
	$skyline=$this->loadModel('Skyline');
	
	$f = fopen($filePath,"r");	
	
	
	//Checking header format
	
	$raw = array();
	while($row = fgetcsv($f)) {
	    $raw[] = $row;
	}
	
	if(count($raw) < 2) {
	    $response = '{  "status" : 0, 
			    "error" : "Validation failed: CSV file must have a header row and at least 1 job. Current number of rows: ' . count($raw) .
			'"}';
	    $this->log("ERROR: " . $response);
	    die($response);
	}
		
	$headerRaw = $raw[0];
	$header = [];
	
	for($i = 0; $i < count($headerRaw); $i++) {
	    $header[$i][0] = $skyline->getNameFromNumber($i);
	    $header[$i][2] = $headerRaw[$i];
	} 
	
	$header[0][1] = "Under 28 Days";
	$header[1][1] = "SLP #";
	$header[2][1] = "Account #";
	$header[3][1] = "Cat No";
	$header[4][1] = "Model";
	$header[5][1] = "Date of Order";
	$header[6][1] = "Brand";
	$header[7][1] = "Full Name";
	$header[8][1] = "Address1";
	$header[9][1] = "Address2";
	$header[10][1] = "City";
	$header[11][1] = "Postcode";
	$header[12][1] = "Telephone";
	$header[13][1] = "Mobile";
	$header[14][1] = "Email";
	$header[15][1] = "Notes";
	
	$headerValid = true;
	
	foreach($header as $key => $value) {
	    if(@$value[1] == $value[2]) {
		$header[$key][2] = "OK";
	    } else {
		$headerValid = false;
	    }
	}
	    
	if(!$headerValid) {
	    $error = '{	
			"status" : 2,
			"header" : ' . json_encode($header) . '
		      }';
	    $this->log("ERROR: " . $error);
	    die($error);
	}
	
	//Checking content format

	$header[0][3]  = "Must be YES or NO";
	$header[1][3]  = "Must be numeric";
	$header[2][3]  = "Must be numeric";
	$header[3][3]  = "Must be 5 characters long";
	$header[4][3]  = "Not checked";
	$header[5][3]  = "Not checked";
	$header[6][3]  = "Must be valid brand";
	$header[7][3]  = "Not checked";
	$header[8][3]  = "Not checked";
	$header[9][3]  = "Not checked";
	$header[10][3] = "Not checked";
	$header[11][3] = "Must be valid postcode";
	$header[12][3] = "Must be numeric";
	$header[13][3] = "Must be numeric";
	$header[14][3] = "Must be valid email";
	$header[15][3] = "Not checked";
	
	$errorRows = [];
	
	foreach($raw as $key => $row) {
	    
	    if($key == 0) {
		continue;
	    }
	    
	    if(!isset($row[0]) && !isset($row[1]) && !isset($row[2])) {
		continue;
	    }
	    
	    //Under 28 days
	    if($row[0] != "Yes" && $row[0] != "No" && $row[0] != "") {
		//$errorRows[] = array($key, $header[0][1], $row[0], $header[0][3]);
	    }
	    
	    //SLP
	    if(intval(trim($row[1]))==0) {
		//$errorRows[] = array($key, $header[1][1], $row[1], $header[1][3]);
	    }
	    
	    //postcode
	    if(strlen(trim($row[11]))>8 || strlen(trim($row[11]))<6) {
		$errorRows[] = array($key, $header[11][1], $row[11], $header[11][3]);
	    }
	    
	    //brand
	    if(strtolower($row[6])!="k&co" && strtolower($row[6])!="very" && strtolower($row[6])!="very.com" && strtolower($row[6])!="littlewoods" && strtolower($row[6])!="isme") {
		$errorRows[] = array($key, $header[6][1], $row[6], $header[6][3]);
	    }
	    
	    //catalogue number
	    if(strlen(trim($row[3]))<5) {
		$errorRows[] = array($key, $header[3][1], $row[3], $header[3][3]);
	    }
	    
	    //email
	    $regex = "/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/";
	    if(!preg_match($regex,$row[14])) {
		//$errorRows[] = array($key, $header[14][1], $row[14], $header[14][3]);
	    }

	    //telephone number
	    $telArr = str_split(trim($row[12]));
	    $telCount = 0;
	    foreach($telArr as $el) {
		if($el=="0" || ($el!="0" && intval($el)!=0)) {
		    $telCount++;
		}
	    }
	    if($telCount<10) {
		//$errorRows[] = array($key, $header[12][1], $row[12], $header[12][3]);
	    }

	    //cellphone number
	    $cellArr = str_split(trim($row[13]));
	    $cellCount = 0;
	    foreach($cellArr as $el) {
		if($el=="0" || $el==0 || intval($el)!=0) {
		    $cellCount++;
		}
	    }
	    if($cellCount<10) {
		//$errorRows[] = array($key, $header[13][1], $row[13], $header[13][3]);
	    }
	    
	}
	
	//$this->log("ERROR_ROWS: " . var_export($errorRows,true));
	
	//if validation errors found, display them
	if(count($errorRows) > 0) {
	    $error = '{	
			"status" : 3,
			"header" : ' . json_encode($header) . ',
			"errorRows" : ' . json_encode($errorRows) . ' 
		      }';
	    $this->log("CSV validation error: " . $error);
	    die($error);
	}
	
		
	//JOB IMPORT
	
	require_once("JobController.class.php");
	$job = $this->loadModel('Job'); 
	$client_mod = $this->loadModel('Clients');
	
	rewind($f);
	
	//count how many lines are there in the file
	$batchRowCount = 0;
	while(!feof($f)) {
	    $line = fgets($f);
	    if($line != "" && $line != "\r\n" && $line != "\n") {	    
		$batchRowCount++;
	    }
	}

		
	//get client ID
	$clientID = null;
	//This caused a problem, because of some reason on live database there's both "Shop Direct" and "Shop Direct Ltd"
	//$clientID = ($clientID) ? $clientID : $job->getClientID("Shop Direct");
	$clientID = ($clientID) ? $clientID : $job->getClientID("Shop Direct Ltd");
	
	if(!$clientID) {
	    echo('{ "status" : 0, "error" : "Import failed. Please contact system admin for support, error code: 01" }');
	    throw new Exception("ClientID not found - can't proceed with job import.");
	}
	
	//get client data
	$client=$client_mod->fetchRow(array("ClientID" => $clientID));
	//get the last client batch record
	$latestBatchRecord=$job->getLatestBatchRecord($clientID);
	//create a new batch number (last number + 1 or if first time, just 1)
	$batchNumber=($latestBatchRecord) ? ($latestBatchRecord["batchNumber"] + 1) : 1;
	//set batch hash
	$batchHash=$_GET["batchHash"];
	//set batch description
	$description=$_GET["description"];
	//create a new batch record
	$createBatchRecord=$job->createBatchRecord($batchNumber,$clientID,$batchRowCount,$description,$batchHash);
	//retrieve latest batch record
	$batchRecord=$job->getLatestBatchRecord($clientID);
	$batchID=$batchRecord["batchID"];	
	
	
	$job->changeBatchState($batchID, "importing");

	//bring file pointer to the beginning of the file
	rewind($f);
	
	//read the header row to skip it
	$row = fgetcsv($f);
	
	//create a job from every row
	while($row = fgetcsv($f)) {

	    if(!isset($row[0]) && !isset($row[1]) && !isset($row[2])) {
		continue;
	    }
	    
	    //process customer name
	    $titleID = null;
	    $firstName = "";
	    $lastName = "";
	    $nameSplit = explode(" ", $row[7]);
	    
	    //array_walk - closure - removes dots and empty spaces from array elements
	    array_walk($nameSplit, function(&$value, &$key) {
		$value = str_replace(".", "", trim($value));
	    });
	    
	    if(preg_grep("/mr/i",$nameSplit) || preg_grep("/mrs/i",$nameSplit) || preg_grep("/miss/i",$nameSplit) || preg_grep("/ms/i",$nameSplit)) {
		
		switch (strtolower($nameSplit[0])) {
		    case "mr"   : $titleID = 1; break;
		    case "mrs"  : $titleID = 2; break;
		    case "miss" : $titleID = 3; break;
		    case "ms"   : $titleID = 4; break;
		    default	: $titleID = null; break;
		}
		
		if(count($nameSplit) == 2) {
		    $firstName = "";
		    $lastName = $nameSplit[1];
		}
		if(count($nameSplit) == 3) {
		    $firstName = $nameSplit[1];
		    $lastName = $nameSplit[2];
		}
		if(count($nameSplit) == 4) {
		    $firstName = $nameSplit[1];
		    $lastName = $nameSplit[2] . " " . $nameSplit[3];
		}
		
	    } else {
		
		if(count($nameSplit) == 1) {
		    $firstName = "";
		    $lastName = $nameSplit[0];
		}
		if(count($nameSplit) == 2) {
		    $firstName = $nameSplit[0];
		    $lastName = $nameSplit[1];
		}
		if(count($nameSplit) == 3) {
		    $firstName = $nameSplit[0];
		    $lastName = $nameSplit[1] . " " . $nameSplit[2];
		}
		if(count($nameSplit) == 4) {
		    $firstName = $nameSplit[0];
		    $lastName = $nameSplit[1] . " " . $nameSplit[2] . " " . $nameSplit[3];
		}
		
	    }

	    //process brand/branch
	    //IMPORTANT: this asumes that 1 brand has 1 branch with exactly the same name
	    $branch=strtolower(substr($row[6],0,4));
	    $branchID=$job->getBranch($branch,$clientID);
	    
	    //set a flag which indicates to JobController::processDataAction() that this is AJAX CSV job import call
	    $_POST["csvAjax"]=1;
	    	
	    $modelID=$job->getModelID($row[4]);
	    $_POST["ModelID"] = ($modelID) ? $modelID : null;
	    
	    if($modelID) {
		$manufacturerID = $job->getManufacturerID($modelID);
	    } else {
		$manufacturerID = 1;
	    }
	    
	    $productID = $job->getProductID($row[3]);
	    $_POST["ProductID"] = $productID;
	    
	    if(!$productID) {
		$_POST['Notes']="PRODUCT: " . $row[6] . " CATALOGUE NUMBER " . $row[3] . ".   Under 28 days = " . 
		    (($row[0]!="" && $row[0]!="No") ? "Yes" : "No");
	    } else {
		$_POST['Notes']="PRODUCT: " . $row[6] . ". Under 28 days = " . (($row[0]!="" && $row[0]!="No") ? "Yes" : "No");
	    }
	    
	    //clean up dashes from phone number and add a zero in front if it's not there
	    $phoneTemp=str_replace("-", "", $row[12]);
	    $phoneFirst=substr($phoneTemp,0,1);
	    if($phoneFirst!="0" && $phoneFirst!=0) {
		$phone="0" . $phoneTemp;
	    } else {
		$phone=$phoneTemp;
	    }
	    
	    //set customer data
	    $_POST['JobSourceID'] = 5;
	    $_POST["CustomerTitleID"] = $titleID;
	    $_POST["CompanyName"] = "";
	    $_POST["ContactFirstName"] = $firstName;
	    $_POST["ContactLastName"] = $lastName;
	    $_POST['PostalCode'] = strtoupper($row[11]);
	    $_POST['BuildingNameNumber'] = "";
	    $_POST['Street'] = $row[8];
	    $_POST['LocalArea'] = $row[9];
	    $_POST['City'] = $row[10];
	    $_POST['CountyID'] = null;
	    $_POST['CountryID'] = 1;
	    $_POST['ContactEmail'] = $row[14];
	    $_POST['ContactHomePhone'] = $phone;
	    $_POST['ContactHomePhoneExt'] = "";
	    $_POST['ContactMobile'] = $row[13];
	    $_POST['data_protection_act'] = 1;
	    $_POST['contact_method_mail'] = 1;
	    $_POST['contact_method_letter'] = 1;
	    $_POST['contact_method_sms'] = 1;
	    
	    //set job data
	    $_POST['ProductDescription'] = false;
	    $_POST['ModelName'] = $row[4];
	    $_POST['NetworkID'] = 4;
	    $_POST['ClientID'] = $clientID;
	    $_POST['BranchID'] = $branchID;
	    $_POST['JobTypeID'] = 1;
	    $_POST['ServiceTypeID'] = 2;
	    $_POST['ServiceBaseUnitType'] = "DIGITAL CAMERA";
	    $_POST['ServiceBaseModel'] = "";
	    $_POST['ManufacturerID'] = $manufacturerID;
	    $_POST['SerialNo'] = "";
	    $_POST['ReportedFault'] = "No fault given";
	    $_POST['ProductLocation'] = "Branch";
	    $_POST['OriginalRetailerPart1'] = "";
	    $_POST['OriginalRetailerPart2'] = "";
	    $_POST['OriginalRetailerPart3'] = "";
	    $_POST['Insurer'] = "";
	    $_POST['PolicyNo'] = $row[2];
	    $_POST['AuthorisationNo'] = "";
	    $_POST['ReferralNo'] = $row[1];
	    $_POST['StockCode'] = "";
	    $_POST['ColAddCompanyName'] = "";
	    $_POST['ColAddBuildingNameNumber'] = "";
	    $_POST['ColAddStreet'] = "";
	    $_POST['ColAddLocalArea'] = "";
	    $_POST['ColAddCity'] = "";
	    $_POST['ColAddCountyID'] = "";
	    $_POST['ColAddCountryID'] = "";
	    $_POST['ColAddPostcode'] = "";
	    $_POST['DateOfPurchase'] = $row[5];
	    $_POST['DefaultBranchID'] = false;
	    $_POST['ReceiptNo'] = "";
	    $_POST['batchID'] = (isset($batchID)) ? $batchID : null;
	    
	    $_POST['UnitTypeID'] = 10;
	    //$_POST['RepairSkillID']=2;
	    
	    //$this->log("POST DUMP: " . var_export($_POST,true));
	    
	    //$_POST['ServiceProviderID']=($skyline->findServiceCentre($_POST)) ? $skyline->findServiceCentre($_POST) : 0;
	    $_POST['ServiceProviderID'] = 10;
	    
	    $jobController = new JobController();
	    $jobController->processDataAction();
	    
	    //release session file lock and sleep for a microsecond so that update progress ajax call can kick in to get response
	    $id = session_id();
	    session_write_close();
	    usleep(1);
	    session_start($id);
	 
	    $this->log($_POST);
	    
	}
	
	
	//change batch state to "finished"
	$job->changeBatchState($batchID,"finished");
	
	
	//send email summary
	
	$email=$this->loadModel('Email');
	$sp=$this->loadModel('ServiceProviders');	
	$serviceProvider=$sp->getServiceProvider($_POST['ServiceProviderID']);
	
	$fromName="Admin";
	$from="Admin@skylinecms.co.uk";
	
	//----enable this when live----
	//$to=$client["ContactEmail"];		    //client email
	//$cc=$serviceProvider["ContactEmail"];	    //service provider email
	
	//----disable this when live----
	//prevent sending summary emails to Joe while testing localy
	if(APPLICATION_PATH == "C:\xampp\htdocs\Skyline\public\application") {
	    $to = "v.rutkunas@pccsuk.com";
	    $cc = "v.rutkunas@pccsuk.com";
	} else {
	    $to = "j.berry@pccsuk.com";
	    $cc = "j.berry@pccsuk.com";
	}
	
	//$cc=false;
	
	$subject="Summary Analysis: Skyline CSV Import Procedure";
	
	$body='
		<style>
		    td {
			border:1px solid black;
			border-collapse:collapse;
			border-spacing:0;
			margin:0;
		    }
		</style>
		<p>IMPORTANT SKYLINE INFORMATION</p>
		<table>
		    <tr>
			<td colspan="2"><strong>Summary Analysis: Skyline CSV Import Procedure</strong></td>
		    </tr>
		    <tr>
			<td>CSV Import Date & Time:</td>
			<td><strong>' . date("d F Y H:i") . '</strong></td>
		    </tr>
		    <tr>
			<td>Skyline Client:</td>
			<td><strong>Shop Direct Ltd</strong></td>
		    </tr>
		    <tr>
			<td>Service Provider:</td>
			<td><strong>Camtronics</strong></td>
		    </tr>
		    <tr>
			<td>Jobs Created:</td>
			<td><strong>' . ($batchRowCount - 1) . '</strong></td>
		    </tr>
		</table>
		<p>System URL: ' . $_SERVER["SERVER_NAME"] . $_SERVER['PHP_SELF'] . '</p>
	';
	
	$replyto=$from;
	$replyName=$fromName;
	
	$sent=$email->Send(
			    $fromName,
			    $from,
			    $to,
			    $subject,
			    $body,
			    $cc,	//cc email
			    $replyto,
			    $replyName,
			    true
			  );
	
	
	//output result in JSON format
	$output='{"status" : 1}';
	die($output);
	
	
	// Return JSON-RPC response
	//die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');

    }

    
    
    //CSV import progress checking
    //2012-10-08 © Vic v.rutkunas@pccsuk.com
    
    public function progressResponseAction() {
	
	$job = $this->loadModel('Job'); 

	$clientID = $job->getClientID("Shop Direct Ltd");
	if(!$clientID) {
	    throw new Exception("ClientID not found - can't proceed with job import.");
	}
	
	//gets latest batch record also comparing hash
	$batchRecord = $job->getLatestBatchRecord_Hash($clientID, $_POST["batchHash"]);
	
	if(!$batchRecord) {
	    $result = '{"batchState" : "initiating", "totalJobs" : 0, "importedJobs" : 0}';
	    die($result);
	}
	
	$totalJobs = $batchRecord["batchRowCount"] - 1;
	
	$batchID = $batchRecord["batchID"];	
	
	$importedJobs = $job->getImportedJobs($batchID);
	
	$result = '{"batchState" : "' . $batchRecord["batchState"] . '", "totalJobs" : ' . $totalJobs . ', "importedJobs" : ' . $importedJobs . '}';
	die($result);
	
    }

    
    //Standalone function for pushing a single job to service center
    //2012-10-08 © Vic v.rutkunas@pccsuk.com
   
    public function pushJobToServiceCenterAction() {

	$jobID = $_GET["jobID"];
	
	$jobModel = $this->loadModel("Job");
	
	$result = $jobModel->pushJobToSC($jobID);
	
	if($result) {
	    echo("success");
	} else {
	    echo("failure");
	}
	
    }
	
	
    // Intended for use by cron - finds all the jobs that were not pushed to service center and pushes them.
    // 2012-10-08 © Vic v.rutkunas@pccsuk.com
   
    public function pushJobToSCCronAction() {
	
	$jobModel = $this->loadModel("Job");
	
	$jobs = $jobModel->getUnsentJobIDs();
	
	$jobCount=0;
	
	foreach($jobs as $job) {
	    
	    $jobModel->pushJobToSC($job["JobID"]);
	    $jobCount++;
	    
	}
	
	echo("Summary: " . $jobCount . " pushed in total.");
	
    }
    
    
    
    /*************************************************************************
     * 
     * Private methods
     * 
     *************************************************************************/
    
    private function getStatusDate ($status_name_array, $status_history) {
        
        /* Used in setting up TimeLine
         * 
         * Find the most recent occurence of any status in status_name_array 
         * 
         * Note: $status_history is the associative array returned by
         *       StatusHistory model getJobStatusHistory
         * 
         */
        foreach($status_history as $history) {
            foreach($status_name_array as $name) {
                if ($history['StatusName'] == $name)
                    return $history['StatusDate'];
            }
            
        }
        
        // not found so return null...
        return null;
    }
    
    
    
    public function saveTableStateAction() {
	
	//$this->log("POST: " . var_export($_POST,true));
	//$this->log("GET: " . var_export($_GET,true));
	if(isset($_POST["data"])){
	$data = $_POST["data"];
	$tableID = $_GET["tableID"];
	
	$job_model = $this->loadModel("Job");
	
	$job_model->saveTableState($tableID, $data, $this->user);
	
	echo 1;
        }else{
        echo 0;
        }
	
    }
    
    
    
    public function loadTableStateAction() {
       
        $tableID = $_GET["tableID"];
	$job_model = $this->loadModel("Job");
 	$result = $job_model->loadTableState($tableID, $this->user);
        echo($result);
        
    }
    
    
    
    /*
     * http://stackoverflow.com/questions/96759/how-do-i-sort-a-multidimensional-array-in-php
     **************************************************************************/
    private function sort_multi_array ($array, $key)
    {
        $keys = array();
        for ($i=1;$i<func_num_args();$i++) {
            $keys[$i-1] = func_get_arg($i);
        }

        // create a custom search function to pass to usort
        $func = function ($a, $b) use ($keys) {
            for ($i=0;$i<count($keys);$i++) {
                if ($a[$keys[$i]] != $b[$keys[$i]]) {
                    return ($a[$keys[$i]] < $b[$keys[$i]]) ? -1 : 1;
                }
            }
            return 0;
        };

        usort($array, $func);

        return $array;
    }
    
    
    
    /**
    @action get Samsung Transaction Report modal window template (SiteMap page)
    @input  void
    @output html template
    @return void
    @date   2013-02-28 © Vic <v.rutkunas@pccsuk.com>
    */

    public function getSamsungTransactionReportModalAction() {
	
	$skyline = $this->loadModel("Skyline");
	
	switch($this->user->UserType) {
	    case "Admin": {
		$this->smarty->assign("userLevel", "Admin");
		$this->smarty->assign("networks", $skyline->getNetworks());
		break;
	    }
	    case "Network": {
		$this->smarty->assign("userLevel", "Network");
		$this->smarty->assign("clients", $skyline->getNetworkClients($this->user->NetworkID));
		break;
	    }
	    case "Client": {
		$this->smarty->assign("userLevel", "Client");
		$this->smarty->assign("branches", $skyline->getClientBranches($this->user->ClientID));
		break;
	    }
	    case "Branch": {
		$this->smarty->assign("userLevel", "Branch");
		break;
	    }
	}	
	
	$html = $this->smarty->fetch('samsungTransactionReport.tpl');
	echo($html);
	
    }
   
    
    
    public function getServiceActionTrackingReportModalAction() {
	
	$skyline = $this->loadModel("Skyline");
	
	switch($this->user->UserType) {
	    case "Admin": {
		$this->smarty->assign("userLevel", "Admin");
		$this->smarty->assign("networks", $skyline->getNetworks());
		break;
	    }
	    case "Network": {
		$this->smarty->assign("userLevel", "Network");
		$this->smarty->assign("clients", $skyline->getNetworkClients($this->user->NetworkID));
		break;
	    }
	    case "Client": {
		$this->smarty->assign("userLevel", "Client");
		$this->smarty->assign("branches", $skyline->getClientBranches($this->user->ClientID));
		break;
	    }
	    case "Branch": {
		$this->smarty->assign("userLevel", "Branch");
		break;
	    }
	}	
	
	$html = $this->smarty->fetch("serviceActionTrackingReportModal.tpl");
	echo($html);
	
    }
   
   
    
    /**
    @action get rapid process modal
    @input  void
    @output html template
    @return void
    @date   2013-03-19 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function rapidProcessModalAction() {
	
	$html = $this->smarty->fetch("rapidProcessModal.tpl");
	echo($html);
	
    }
    
     /**
    @action get miliseconds from now till session expires
    @input  void
    @output integer
    @return void
    @date   2013-05-24  Andris <a.polnikovs@pccsuk.com>
    */
    public function getExpirityMilisecsFromNowAction(){
      echo$_SESSION['expire']-date("U");
        
    }
    
    
}

?>
