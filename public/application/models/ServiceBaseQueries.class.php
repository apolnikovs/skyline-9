<?php

require_once('CustomModel.class.php');
require_once('Constants.class.php');
require_once('Functions.class.php');

/**
 * This model is for ServiceBase API Database Queries only.
 * 
 * There should be NO database updates in this model (those should be 
 * marshalled by a separate Business model and rely upon individual table models
 * for create/update/delete methods for specific tables).  Neither should there 
 * be any data manipulation - that should be performed in the higher level
 * Business model. Just plain vanilla database queries here please.
 * 
 * In this way, these same database queries can be used in different
 * Business models with different business logic without duplication of 
 * query code.
 * 
 * 
 * Methods in this model should be of the form:
 * 
 *      public function getXXXXXX( $args,...... ) {
 *          $sql = "select .......";
 *          $params = array( 'name' => value );
 *          return $this->Query($this->conn, $sql, $params);
 *      }
 * 
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.1
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 10/07/2013  1.0     Brian Etherington     Initial Version
 ******************************************************************************/
class ServiceBaseQueries extends CustomModel {

    public function __construct($controller) {  
        
        parent::__construct($controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        
        $this->debug = false;

    }
    
    public function fetchJob($job_id) {
        $sql = "select IFNULL(client.AccountNumber, nsp.AccountNo) as ClientAccountNo,
                       job.JobID as SLNumber,
                       job.RMANumber as RMANumber,
                       job.ServiceCentreJobNo as SCJobNo,
                   #    job.AgentRefNo as AgentRefNo,
                       job.NetworkRefNo as NetworkRefNo,
                       customer_title.Title as CustomerTitle,
                       customer.ContactFirstName as CustomerForename,
                       customer.ContactLastName as CustomerSurname,
                       customer.BuildingNameNumber as CustomerBuildingName,
                       customer.Street as CustomerStreet,
                       customer.LocalArea as CustomerAddressArea,
                       customer.TownCity as CustomerAddressTown,
                       county.name as CustomerCounty,
                       customer.PostalCode as CustomerPostcode,
                       customer.ContactHomePhone as CustomerHomeTelNo,
                       customer.ContactWorkPhone as CustomerWorkTelNo,
                       customer.ContactMobile as CustomerMobileNo,
                       customer.ContactEmail as CustomerEmailAddress,
                   #    job.OriginalRetailer as OriginalRetailer,
                   #    job.Retailerlocation as Retailerlocation,
                   #    service_type.Code as ServiceType,
                       IFNULL(model.ModelNumber, job.ServiceBaseModel) as ModelNo,
                       job.SerialNo as SerialNo,
                       IFNULL(manufacturer.ManufacturerName, job.ServiceBaseManufacturer) as Manufacturer,
                       IFNULL(product_unit_type.UnitTypeName, IFNULL(model_unit_type.UnitTypeName, job.ServiceBaseUnitType)) as UnitType,
                   #    DATE_FORMAT(job.DateOfPurchase,'%Y-%m-%d') as DOP,
                       job.PolicyNo as PolicyNo,
                       job.Insurer as Insurer,
                       job.ReportedFault as ReportedFault,
                   #    job.ConditionCode as ConditionCode,  
                   #    job.SymptomCode as SymptomCode,
                   #    job.GuaranteeCode as GuaranteeCode,
                   #    job.DefectType as DefectType,
                   #    DATE_FORMAT(job.DateBooked,'%d/%m/%Y') as DateBooked,
                   #    job.TimeBooked as TimeBooked,
                   #    DATE_FORMAT(job.DateUnitReceived,'%d/%m/%Y') as DateUnitReceived,
                   #    DATE_FORMAT(job.RepairCompleteDate,'%d/%m/%Y') as DateRepairComplete,
                   #    DATE_FORMAT(job.ClosedDate,'%d/%m/%Y') as ClosedDate,
                   #    DATE_FORMAT(job.ETDDate,'%d/%m/%Y') as ETDDate,
                   #    job.RepairDescription as RepairDescription,  
                   #    status.StatusName as Status,
                   #    job.AgentStatus as AgentStatus,
                   #    job.CompletionStatus as CompletionStatus,
                   #    job.Accessories as Accessories,
                   #    job.UnitCondition as UnitCondition,
                   #    job.JobSite as JobSite,
                   #    job.EngineerCode as EngineerCode,
                   #    job.EngineerName as EngineerName,
                   #    job.ClaimNumber as ClaimNumber,
                       job.AuthorisationNo as AuthorisationNumber,  
                       IF (customer.DataProtection='YES', 'Y','N') as DataConsent,
                   #    job.ClaimTransmitStatus as ClaimTransmitStatus,
                       job.eInvoiceStatus as eInvoiceStatus,
                       job.ChargeableLabourCost as ChargeableLabourCost,
                       job.ChargeableLabourVATCost as ChargeableLabourVATCost,
                       job.ChargeablePartsCost as ChargeablePartsCost,
                       job.ChargeableDeliveryCost as ChargeableDeliveryCost,
                       job.ChargeableDeliveryVATCost as ChargeableDeliveryVATCost,
                       job.ChargeableVATCost as ChargeableVATCost,
                       job.ChargeableSubTotal as ChargeableSubTotal,   
                   #    job.AgentChargeableInvoiceNo as AgentChargeableInvoiceNo,
                   #    DATE_FORMAT(job.AgentChargeableInvoiceDate,'%d/%m/%Y') as AgentChargeableInvoiceDate,
                       job.WarrantyLabourCost as WarrantyLabourCost,
                       job.WarrantyDeliveryCost as WarrantyDeliveryCost,
                       job.ClaimBillNumber as ClaimBillNumber,
                   #    job.EstimateStatus as EstimateStatus,
                   #    job.ProductLocation as ProductLocation,
                   #    job.ReceiptNo as ReceiptNo,
                       job.ColAddCompanyName as ColAddCompanyName,
                       job.ColAddBuildingNameNumber as ColAddBuildingNameNumber,   
                       job.ColAddStreet as ColAddStreet,
                       job.ColAddLocalArea as ColAddLocalArea,
                       job.ColAddTownCity as ColAddTownCity,
                       county_coll.Name as ColAddCounty,
                       country_coll.Name as ColAddCountry,
                       job.ColAddPostcode as ColAddPostcode,
                       job.ColAddEmail as ColAddEmail,
                       job.ColAddPhone as ColAddPhone,
                       job.ColAddPhoneExt as ColAddPhoneExt,   
                       job.ExtendedWarrantyNo as ExtendedWarrantyNo
                   #    job.SamsungSubServicetype as SamsungSubServicetype,
                   #    job.SamsungRefNo as SamsungRefNo,
                   #    job.ConsumerType as ConsumerType,
                   #    job.ImeiNo as ImeiNo,
                   #    mpn.MobilePhoneNetworkName as MobilePhoneNetwork
                from job 
                left join customer on job.CustomerID=customer.CustomerID
                left join customer_title on customer.CustomerTitleID=customer_title.CustomerTitleID
                left join county on customer.CountyID=county.CountyID 
                left join county county_coll on job.ColAddCountyID=county_coll.CountyId
                left join country country_coll on job.ColAddCountryID=country_coll.CountryId
           #     left join service_type on job.ServiceTypeID=service_type.ServiceTypeID
           #     left join status on job.StatusID=status.StatusID
                left join client on job.ClientID=client.ClientID
                left join network_service_provider nsp on job.NetworkID=nsp.NetworkID and job.ServiceProviderID=nsp.ServiceProviderID
                left join model on job.ModelID=model.ModelID
                left join manufacturer on job.ManufacturerID=manufacturer.ManufacturerID
                left join unit_type model_unit_type on model.UnitTypeID=model_unit_type.UnitTypeID
                left join product on job.ProductID=product.ProductID
                left join unit_type product_unit_type on model.UnitTypeID=product_unit_type.UnitTypeID
           #     left join mobile_phone_network mpn on job.MobilePhoneNetworkID=mpn.MobilePhoneNetworkID
                where job.JobID=:JobID";
        $params = array('JobID' => $job_id);
        $job = $this->Query($this->conn, $sql, $params);
        if (count($job) == 0) {
            return null;
        }
        $job = $job[0];
        
        $job['Parts'] = $this->fetchParts($job_id);
        // 11/07/2013 appointments array has been removed from the api specification
        //            because these records will be handled by the existing diary interface (?)
        //$job['Appointments'] = $this->fetchAppointments($job_id);
        //$job['StatusUpdates'] = $this->fetchStatusHistory($job_id);
        $job['ContactHistory'] = $this->fetchContactHistory($job_id);
        $job['ClaimResponseErrors'] = $this->fetchClaimResponse($job_id);
        
        return $job;

    }
    
    public function fetchServiceProviderConnectionDetails($job_id) {
        $sql = "select job.ServiceProviderID,
                       sp.CompanyName as ServiceCentre,
                       sp.Platform as Platform,
                       IF(ISNULL(sp.Port),sp.IPAddress,CONCAT(sp.IPAddress, ':', sp.Port)) AS URL,
                       user.Username as Username,
		       user.Password as Password
                from job
                left join service_provider sp on job.ServiceProviderID=sp.ServiceProviderID
                left join user on job.ServiceProviderID=user.ServiceProviderID
                where job.JobID=:JobID";
        $params = array('JobID' => $job_id);
        $details = $this->Query($this->conn, $sql, $params);
        if (count($details) == 0) {
            return null;
        }
        return $details[0];
    }
    
    private function fetchParts($job_id) {
        $sql = "select part.PartID as SkylinePartID,
                       part.SBPartID as SBPartID,
                       part.PartNo as PartNo,
                   #    part.PartDescription as Description,
                   #    part.Quantity as Quantity,
                   #    part.OrderStatus as PartStatus,
                   #    part.SectionCode as SectionCode,
                   #    part.DefectCode as DefectCode,
                   #    part.RepairCode as RepairCode,
                   #    part.CircuitReference as CircuitRef,
                   #    part.PartSerialNo as PartSerialNo,
                   #    part.OldPartSerialNo as OldPartSerialNo,
                   #    part.OrderDate as OrderDate,
                   #    part.ReceivedDate as OrderReceivedDate,
                   #    part.SupplierOrderNo as OrderNo,
                   #    part.InvoiceNo as OrderInvoiceNo,
                       part.UnitCost as UnitCost,
                   #    part.SaleCost as SaleCost,
                   #    part.VATRate as VATRate,
                   #    part.IsOtherCost as IsOtherCost,
                   #    part.IsAdjustment as IsAdjustment,
                   #    part.IsMajorFault as IsMajorFault,
                   #    DATE_FORMAT(part.DueDate,'%d/%m/%Y') as DueDate,
                       part.SupplierOrderNo as SupplierOrderNo                                              
                from part 
                where part.JobID=:JobID
                order by part.PartID";
        $params = array('JobID' => $job_id);
        return $this->Query($this->conn, $sql, $params);
    }  
    
    // 11/07/2013 appointments array has been removed from the api specification
    //            because these records will be handled by the existing diary interface (?)
    /*private function fetchAppointments($job_id) {
        $sql = "select appointment.SBAppointID as SBAppointID,
                       DATE_FORMAT(appointment.AppointmentDate,'%Y-%m-%d') as AppointmentDate,
                       appointment.AppointmentTime as AppointmentTime,
                       appointment.AppointmentType as AppointmentType,
                       IF(appointment.OutCardLeft=1,'Y','N') as OutCardLeft,
                       appointment.Engineercode as Engineercode                        
                from appointment 
                where appointment.JobID=:JobID
                order by appointment.AppointmentID";
        $params = array('JobID' => $job_id);
        return $this->Query($this->conn, $sql, $params);
    }*/
    
    /*private function fetchStatusHistory($job_id) {
        $sql = "select status_history.StatusHistoryID as SkylineStatusHistoryID,
                       DATE_FORMAT(status_history.Date,'%Y-%m-%d') as DateChanged,
                       TIME(status_history.Date) as TimeChanged,
                       '' as OldStatus,
                       status.StatusName as NewStatus,
                       status_history.UserCode as UserCode
                from status_history 
                left join status on status_history.StatusID=Status.StatusID
                where status_history.JobID=:JobID
                order by status_history.Date";
        $params = array('JobID' => $job_id);
        $status_history = $this->Query($this->conn, $sql, $params);
        
        // calculate old_status from previous row...
        $old_status = '';
        foreach($status_history as &$row) {
            $row['OldStatus'] = $old_status;
            $old_status = $row['NewStatus'];
        }
        
        return $status_history;
    }*/
    
    private function fetchContactHistory($job_id) {
        $sql = "select contact_history.ContactHistoryID as SkylineContactHistoryID,
                       DATE_FORMAT(contact_history.ContactDate,'%d/%m/%Y') as EntryDate,
                       contact_history.ContactTime as EntryTime,
                       contact_history_action.Action as Action,
                       contact_history.Note as Notes,
                       contact_history.UserCode as UserCode,
                       contact_history.Private as Private                       
                from contact_history 
                left join contact_history_action on contact_history.ContactHistoryActionID=contact_history_action.ContactHistoryActionID
                where contact_history.JobID=:JobID
                order by ContactHistoryID";
        $params = array('JobID' => $job_id);
        return $this->Query($this->conn, $sql, $params);
    }
    
    private function fetchClaimResponse($job_id) {
        $sql = "select claim_response.ClaimResponseID as SkylineClaimResponseID,
                       claim_response.ErrorCode as ErrorCode,
                       claim_response.ErrorDescription as ErrorDescription
                from claim_response 
                where claim_response.JobID=:JobID
                order by ClaimResponseID";
        $params = array('JobID' => $job_id);
        return $this->Query($this->conn, $sql, $params);
    }
}
