-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28-0ubuntu0.12.04.2 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-01-22 11:43:04
-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS=0;

INSERT IGNORE INTO `appointment_type` (`AppointmentTypeID`, `Type`, `Status`, `CreatedDate`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 'Delivery', 'Active', '2012-10-08 14:40:24', NULL, '0000-00-00 00:00:00'),
	(2, 'Installation', 'Active', '2012-10-30 13:28:27', NULL, '0000-00-00 00:00:00'),
	(3, 'Inspection', 'Active', '2012-10-30 13:28:31', NULL, '0000-00-00 00:00:00'),
	(4, 'Collection', 'Active', '2012-10-29 14:01:59', NULL, '0000-00-00 00:00:00'),
	(5, 'Repair', 'Active', '2012-10-08 14:41:48', NULL, '0000-00-00 00:00:00'),
	(6, 'Send-in', 'Active', '2012-10-08 14:42:08', NULL, '0000-00-00 00:00:00'),
	(7, 'Wallmount', 'Active', '2012-10-08 14:53:39', 1, '0000-00-00 00:00:00'),
	(8, 'Delivery (2 Person)', 'Active', '2012-10-29 13:59:54', NULL, '0000-00-00 00:00:00'),
	(9, 'Collection (2 Person)', 'Active', '2012-10-29 14:00:28', NULL, '0000-00-00 00:00:00'),
	(10, 'Inspection (2 Person)', 'Active', '2012-10-30 13:44:10', NULL, '0000-00-00 00:00:00'),
	(11, 'Installation (2 Person)', 'Active', '2012-10-30 13:44:14', NULL, '0000-00-00 00:00:00'),
	(12, 'Wall Mount (2 Person)', 'Active', '2012-10-29 14:01:23', NULL, '0000-00-00 00:00:00'),
	(13, 'Repair (2 Person)', 'Active', '2012-10-29 14:01:38', NULL, '0000-00-00 00:00:00'),
	(14, 'Return', 'Active', '2012-11-22 12:09:41', NULL, '0000-00-00 00:00:00'),
	(15, 'Fit Parts', 'Active', '2012-11-26 14:51:37', NULL, '0000-00-00 00:00:00'),
	(16, 'Fit Parts (2 Person)', 'Active', '2012-11-26 14:51:50', NULL, '0000-00-00 00:00:00');

INSERT IGNORE INTO `contact_history_action` (`ContactHistoryActionID`, `BrandID`, `Action`, `ActionCode`, `Type`, `Status`) VALUES
	(1, 1000, 'PHONE IN', 1, 'System Defined', 'Active'),
	(2, 1000, 'PHONE OUT', 2, 'System Defined', 'Active'),
	(3, 1000, 'FAXED IN', 3, 'System Defined', 'Active'),
	(4, 1000, 'FAXED OUT', 4, 'System Defined', 'Active'),
	(5, 1000, 'E-MAIL IN', 5, 'System Defined', 'Active'),
	(6, 1000, 'E-MAIL OUT', 6, 'System Defined', 'Active'),
	(7, 1000, 'VISIT IN', 7, 'System Defined', 'Active'),
	(8, 1000, 'VISIT OUT', 8, 'System Defined', 'Active'),
	(9, 1000, 'LETTER IN', 9, 'System Defined', 'Active'),
	(10, 1000, 'LETTER OUT', 10, 'System Defined', 'Active'),
	(11, 1000, 'A GENERAL NOTE', 11, 'System Defined', 'Active'),
	(12, 1000, 'WAITING FOR TECHNICAL', 12, 'System Defined', 'Active'),
	(13, 1000, 'WAITING FOR CUSTOMER', 13, 'System Defined', 'Active'),
	(14, 1000, 'INFO RECEIVED', 14, 'System Defined', 'Active'),
	(15, 1000, 'RMA Tracker', 15, 'System Defined', 'Active'),
	(17, 1000, 'CANCEL REQUEST', 17, 'System Defined', 'Active');

INSERT IGNORE INTO `customer_title` (`CustomerTitleID`, `BrandID`, `Title`, `TitleCode`, `Type`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 1000, 'Mr', 1, 'Male', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(2, 1000, 'Mrs', 2, 'Female', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(3, 1000, 'Miss', 3, 'Female', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(4, 1000, 'Ms', 4, 'Female', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(5, 1000, 'Dr', 5, 'Unknown', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2012-09-11 19:45:58');

REPLACE INTO `email` (`EmailID`, `EmailCode`, `Title`, `Message`, `ModifiedDate`, `ModifiedUserID`) VALUES

INSERT IGNORE INTO `general_default` (`GeneralDefaultID`, `BrandID`, `IDNo`, `Category`, `DefaultName`, `Default`, `Description`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 1000, 1, 'Job', 'Job Turnaround Time', '14', 'Job Turnaround Time', '2012-09-03 16:19:35', '0000-00-00 00:00:00', 'Active', 1, '2012-11-21 18:40:08');

INSERT IGNORE INTO `job_source` (`JobSourceID`, `Description`) VALUES
	(1, 'Skyline'),
	(2, 'Micro Sites'),
	(3, 'API'),
	(4, 'Unknown'),
	(5, 'Import'),
	(6, 'ServiceBase'),
	(7, 'RMA Tracker'),
	(8, 'Fixzone'),
	(9, 'Samsung'),
	(10, 'Panasonic');

INSERT IGNORE INTO `job_type` (`JobTypeID`, `BrandID`, `Type`, `TypeCode`, `Priority`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 1000, 'Consumer Repair', 1, 1, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-11-19 11:02:44'),
	(2, 1000, 'Store Stock Repair', 2, 2, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-11-19 11:02:50'),
	(3, 1000, 'Installation', 3, 3, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(4, 1000, 'Other Service', 4, 4, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-04 16:22:15');


INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7001, 'Network Level Access Permison', 'Allow access to the network features', '2012-09-14 13:48:41', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7002, 'Client Level Access Permission.', 'Allow the user to access client features', '2012-09-14 13:48:41', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7003, 'Display Job Search Box', 'Show the job search box on home page.', '2012-09-14 13:48:41', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7004, 'Display Open and Overdue Jobs on Home Pa', 'Show the job search box on home page.', '2012-09-14 13:48:41', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7005, 'Job Allocation Admin Section', 'Job Allocation Admin Section', '2012-09-14 13:48:41', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7006, 'Service Provider Change Link', 'Service Provider Change/Default Link on Job Booking process.', '2012-09-14 13:48:41', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7007, 'Insert Branch when booking job', 'Insert Branch when booking job', '2012-09-14 13:48:41', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7008, 'Display Menu Item - Open Jobs', 'Display Menu Item - Open Jobs', '2012-09-18 10:04:01', 'openJobs');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7009, 'Display Menu Item - Overdue Jobs', 'Display Menu Item - Overdue Jobs', '2012-09-18 10:04:01', 'overdueJobs');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7010, 'Display Menu Item - Performance', 'Display Menu Item - Performance', '2012-09-18 10:04:01', 'performance');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7011, 'Display Menu Item - Site Map', 'Display Menu Item - Site Map', '2012-09-18 10:04:02', 'siteMap');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7012, 'Display Menu Item - Add Contact Note', 'Display Menu Item - Add Contact Note', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7013, 'Display Menu Item - Send Email', 'Display Menu Item - Send Email', '2012-09-18 10:04:02', 'sendemail');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7014, 'Display Menu Item - Send SMS', 'Display Menu Item - Send SMS', '2012-09-18 10:04:02', 'sendsms');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7015, 'Display Menu Item - Cancel Jobs', 'Display Menu Item - Cancel Jobs', '2012-09-18 10:04:02', 'canceljob');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7016, 'Display Menu Item - Other Actions', 'Display Menu Item - Other Actions', '2012-09-18 10:04:02', 'otheractions');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7017, 'Display Control Item - Wild Card Search', 'Display Control Item - Wild Card Search', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7018, 'Display Control Item - Advanced Job Sear', 'Display Control Item - Advanced Job Search', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7019, 'Display Control Item - Advanced Catalogu', 'Display Control Item - Advanced Catalogue', '2012-09-18 10:04:02', 'productAdvancedSearch');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7020, 'Display Language Control - English', 'Display Language Control - English', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7021, 'Display Language Control - German', 'Display Language Control - German', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7022, 'Display CSV Import button', 'Display CSV Import button', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7023, 'Display Appointments', 'Display Appointments', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7024, 'Display Invoice Costs', 'Display Invoice Costs', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7025, 'Display Service Report', 'Display Service Report', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7026, 'Display Parts Use', 'Display Parts Use', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7027, 'Display Contact History', 'Display Contact History', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7028, 'Display Status Changes', 'Display Status Changes', '2012-09-18 10:04:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7029, 'RA Jobs Display Return', 'RA Jobs Display Return', '2012-10-29 14:25:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7030, 'RA Jobs Display Repair', 'RA Jobs Display Repair', '2012-10-29 14:25:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7031, 'RA Jobs Display On Hold', 'RA Jobs Display On Hold', '2012-10-29 14:25:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7032, 'RA Jobs Display Estimate', 'RA Jobs Display Estimate', '2012-10-29 14:25:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7033, 'RA Jobs Display Invoice', 'RA Jobs Display Invoice', '2012-10-29 14:25:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7034, 'Display RA Jobs Menu Item', 'Display RA Jobs Menu Item', '2012-10-29 14:25:02', 'raJobs');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7035, 'Display Job Update RA Drop Down', 'Display Job Update RA Drop Down', '2012-10-29 14:25:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7036, 'Insert Repair Authorisation Number', 'Insert Repair Authorisation Number', '2012-10-29 14:25:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7037, 'Display Policy Search Panel', 'Show the Policy Search panel on the Home Page', '2012-10-29 14:25:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7038, 'Free Text Job Booking', 'Show Free Text Job Booking Button', '2013-01-04 14:25:02', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7039, 'Display Menu Item - Help & Legal', 'Display Menu Item - Help & Legal', '2012-09-18 10:04:01', 'help');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7040, 'Allow model update on booking', 'Allow model update on booking', '2013-01-10 12:55:01', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7041, 'Display Booking Panel on Home Page', 'Display Booking Panel on Home Page', '2013-02-05 05:18:01', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7042, 'Display Menu Item - Graphical Analysis', 'Display Menu Item - Graphical Analysis', '2013-02-05 05:18:01', 'graphicalAnalysis');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(7046, 'Insert Part - Job Update Page', 'Insert Part - Job Update Page', '2013-02-05 05:18:01', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(10000, 'Arrange Collection at Booking', 'Arrange Collection at Booking', '2013-02-05 05:18:01', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(11000, 'Display Booking Panel on Home Page', 'Display Booking Panel on Home Page', '2013-02-05 05:18:01', ''); 
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(11001, 'Display Menu Item - Graphical Analysis', 'Display Menu Item - Graphical Analysis', '2013-02-05 05:18:01', 'graphicalAnalysis'); 
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(11002, 'Site Map - Reports', 'Site Map - Reports', '2013-02-05 05:18:01', ''); 
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(11003, 'Site Map - Reports - Samsung', 'Site Map - Reports - Samsung', '2013-02-05 05:18:01', 'astrReport'); 
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, URLSegment) VALUES(11004, 'Site Map - Request Authorisation', 'Site Map - Request Authorisation', '2013-02-05 05:18:01', 'raJobs');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, EndDate, Status, ModifiedUserID, ModifiedDate, URLSegment) VALUES (12002, 'Branch Service Appraisal', 'Branch Service Appraisal', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', 1, '2013-02-12 09:14:58', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, EndDate, Status, ModifiedUserID, ModifiedDate, URLSegment) VALUES (11005, 'Job update appointment CRUD buttons', 'Job update appointment CRUD buttons', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', 1, '2013-02-12 12:12:58', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, EndDate, Status, ModifiedUserID, ModifiedDate, URLSegment) VALUES (11006, 'Site Map - Appointment Diary', 'Site Map - Appointment Diary', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', 1, '2013-03-11 02:47:58', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, EndDate, Status, ModifiedUserID, ModifiedDate, URLSegment) VALUES (11007, 'Site Map - Booking Performance Wallboard', 'Site Map - Booking Performance Wallboard', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', 1, '2013-03-11 02:47:58', '');
INSERT IGNORE INTO permission (PermissionID, Name, Description, CreatedDate, EndDate, Status, ModifiedUserID, ModifiedDate, URLSegment) VALUES (11008, 'Site Map - Diary Wall Board', 'Site Map - Diary Wall Board', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', 1, '2013-03-11 02:47:58', '');

INSERT IGNORE INTO `primary_fields` (`primaryFieldID`, `primaryFieldName`) VALUES
	(1, 'Policy No.'),
	(2, 'Authorisation No.'),
	(3, 'Referral Number'),
	(4, 'Catalogue No'),
	(5, 'Return'),
	(6, 'Repair'),
	(7, 'On Hold'),
	(8, 'Estimate'),
	(9, 'Invoice'),
	(10, 'Courier'),
	(11, 'Consignment No'),
	(12, 'Consignment Date'),
	(13,'Model No');
INSERT IGNORE INTO primary_fields (primaryFieldID, primaryFieldName) VALUES (10, 'Courier'); 
INSERT IGNORE INTO primary_fields (primaryFieldID, primaryFieldName) VALUES (11, 'Consignment No'); 
INSERT IGNORE INTO primary_fields (primaryFieldID, primaryFieldName) VALUES (12, 'Consignment Date');

INSERT IGNORE INTO `ra_status` (`RAStatusID`, `ListOrder`, `BrandID`, `Status`, `CreatedDateTime`, `ModifiedUserID`, `ModifiedDate`, `RAStatusName`, `RAStatusCode`, `Colour`) VALUES
	(1, 1, 1000, 'Active', '2012-11-01 17:19:11', 1, '2012-12-03 21:07:53', 'New Request', 1, 'aaffff'),
	(2, 2, 1000, 'Active', '2012-11-30 16:22:26', 1, '2012-11-30 16:22:26', 'Authorisation Required', 2, 'f4f409'),
	(3, 3, 1000, 'Active', '2012-12-03 21:09:14', 1, '2012-12-03 21:09:14', 'Authorisation Accepted', 3, '7fff00'),
	(4, 4, 1000, 'Active', '2012-12-03 21:10:14', 1, '2012-12-03 21:10:14', 'Authorisation Declined', 4, 'ff0000'),
	(5, 5, 1000, 'Active', '2012-12-03 21:13:00', 1, '2012-12-03 21:13:00', 'General Query', 5, '0000ff'),
	(6, 6, 1000, 'Active', '2012-12-03 21:13:45', 1, '2012-12-03 21:13:45', 'Query Answered', 6, '999999');

INSERT IGNORE INTO `repair_skill` (`RepairSkillID`, `RepairSkillName`, `RepairSkillCode`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 'AERIAL', 'ConsumerElectronics', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-09 11:15:12'),
	(2, 'BROWN GOODS', 'ConsumerElectronics', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-09 11:15:14'),
	(3, 'COMPUTER', 'ConsumerElectronics', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-09 11:15:16'),
	(4, 'WHITE GOODS', 'DomesticAppliance', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-09 11:15:18'),
	(5, 'CAMERA', 'ConsumerElectronics', '2012-10-25 13:30:19', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:20'),
	(6, 'GAS', 'DomesticAppliance', '2012-10-25 13:30:19', '0000-00-00 00:00:00', 'Active', 1, '2013-01-09 11:15:21'),
	(7, 'REFRIDGERATION', 'DomesticAppliance', '2012-10-25 13:30:19', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:26'),
	(8, 'BANG & OLUFSEN', 'ConsumerElectronics', '2012-11-01 14:16:59', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:28'),
	(9, 'OTHER', 'ConsumerElectronics', '2012-11-26 14:51:55', '0000-00-00 00:00:00', 'Active', NULL, '2013-01-09 11:15:30');


INSERT IGNORE INTO `service_provider` (`ServiceProviderID`, `CompanyName`, `BuildingNameNumber`, `PostalCode`, `Street`, `LocalArea`, `TownCity`, `CountyID`, `CountryID`, `WeekdaysOpenTime`, `WeekdaysCloseTime`, `SaturdayOpenTime`, `SaturdayCloseTime`, `ServiceProvided`, `SynopsisOfBusiness`, `ContactPhoneExt`, `ContactPhone`, `ContactEmail`, `AdminSupervisor`, `ServiceManager`, `Accounts`, `IPAddress`, `Port`, `InvoiceToCompanyName`, `UseAddressProgram`, `ServiceBaseVersion`, `ServiceBaseLicence`, `VATRateID`, `VATNumber`, `Platform`, `Skin`, `ReplyEmail`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`, `ViamenteKey`, `DiaryStatus`, `OnlineDiary`, `DefaultTravelTime`, `DefaultTravelSpeed`, `AutoChangeTimeSlotLabel`, `CourierStatus`, `RunViamenteToday`, `DiaryAdministratorEmail`, `SetupUniqueTimeSlotPostcodes`, `UnlockingPassword`, `PasswordProtectNextDayBookings`, `PasswordProtectNextDayBookingsTime`, `AutoSelectDay`, `AutoDisplayTable`, `AutoSpecifyEngineerByPostcode`, `ServiceManagerForename`, `ServiceManagerSurname`, `ServiceManagerEmail`, `AdminSupervisorForename`, `AdminSupervisorSurname`, `AdminSupervisorEmail`, `SendASCReport`, `EngineerDefaultStartTime`, `EngineerDefaultEndTime`, `NumberOfVehicles`, `NumberOfWaypoints`, `DiaryAllocationType`, `SendRouteMapsEmail`, `Multimaps`, `EmailServiceInstruction`) VALUES
	(0, 'GENERIC SERVICE CENTRE', ' ', 'nn1 5ex', ' 66 Palmerston Road', '', 'Northampton', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '.', 'generic.service.centre@skylinecms.co.uk', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, '', 'Skyline', NULL, '', '2013-02-21 12:24:05', '0000-00-00 00:00:00', 'Active', 1, '2013-02-21 12:24:42', NULL, 'In-Active', 'In-Active', 120, 120, 'No', 'In-active', 'No', NULL, 'No', NULL, 'No', NULL, '', '', 'No', '', '', '', '', '', '', 'No', NULL, NULL, NULL, NULL, 'Postcode', NULL, 6, 'In-Active');
	
INSERT IGNORE INTO `service_type` (`ServiceTypeID`, `BrandID`, `JobTypeID`, `ServiceTypeName`, `ServiceTypeCode`, `Code`, `Priority`, `Chargeable`, `Warranty`, `DateOfPurchaseForced`, `OverrideTradeSettingsPolicy`, `ForcePolicyNoAtBooking`, `OverrideTradeSettingsModel`, `ForceModelNumber`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 1000, 1, 'Chargeable', 1, 'C', NULL, 'Y', 'N', 'N', 'N', 'N', 'Y', 'Y', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:29:50'),
	(2, 1000, 1, 'Warranty', 2, 'W', NULL, 'N', 'Y', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:26:59'),
	(3, 1000, 1, 'Trade', 3, 'T', NULL, 'Y', 'N', 'B', 'N', 'N', 'Y', 'Y', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:29:56'),
	(4, 1000, 1, 'Extended Warranty (Client)', 4, 'Y', NULL, 'Y', 'N', 'B', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:25:59'),
	(5, 1000, 1, 'Extended Warranty (D&G)', 5, 'E', NULL, 'N', 'Y', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:27:14'),
	(6, 1000, 1, 'Extended Warranty (Sony)', 6, 'X', NULL, 'N', 'Y', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:27:16'),
	(7, 1000, 1, 'Insurance', 7, 'N', NULL, 'Y', 'N', 'B', 'Y', 'Y', 'Y', 'Y', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:30:03'),
	(8, 1000, 1, 'Special Claim', 8, 'S', NULL, 'N', 'Y', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:27:18'),
	(9, 1000, 2, 'Chargeable', 9, 'C', NULL, 'Y', 'N', 'B', 'N', 'N', 'Y', 'Y', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:30:09'),
	(10, 1000, 2, 'Warranty', 10, 'W', NULL, 'N', 'Y', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:27:20'),
	(11, 1000, 3, 'Basic Install', 11, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(12, 1000, 3, 'Home Cinema Install', 12, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(13, 1000, 3, 'Installation (Client)', 13, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(14, 1000, 3, 'Installation (Sony)', 14, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(15, 1000, 3, 'PC Setup', 15, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(16, 1000, 3, 'Projector Install', 16, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(17, 1000, 3, 'Satellite System Install', 17, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(18, 1000, 3, 'TV Aerial Install', 18, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(19, 1000, 3, 'Wallmount <32Inch', 19, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(20, 1000, 3, 'Wallmount 32Inch-55inch', 20, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(21, 1000, 3, 'Wallmount 60Inch+', 21, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(22, 1000, 3, 'White Goods Install', 22, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(23, 1000, 4, 'In-home Support', 23, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(24, 1000, 4, 'Tutorial', 24, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(25, 1000, 4, 'Inspection', 25, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-12-18 12:22:09'),
	(26, 1000, 3, 'Advanced AV', 26, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2012-12-18 12:22:09'),
	(27, 1000, 3, 'Advanced TV', 27, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2012-12-18 12:22:09'),
	(28, 1000, 3, 'Complete Wallmount', 28, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2012-12-18 12:22:09'),
	(29, 1000, 3, 'Complete N/W', 29, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2012-12-18 12:22:09'),
	(30, 1000, 3, 'Ultimate', 30, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2012-12-18 12:22:09'),
	(31, 1000, 3, 'CCTV', 31, '', NULL, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2012-12-18 12:22:09'),
	(32, 1000, 4, 'Old Installation', 32, 'I', 99, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'In-active', NULL, '2012-12-18 12:26:30'),
	(33, 1000, 4, 'Old In Home', 33, 'H', 99, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'In-active', NULL, '2012-12-18 12:20:26'),
	(34, 1000, 4, 'Digital Health Check', 34, 'D', 99, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'In-active', NULL, '2012-12-18 12:20:28'),
	(35, 1000, 4, 'Old Installation Sony', 35, 'L', 99, 'N', 'Y', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'In-active', NULL, '2012-12-18 12:27:23'),
	(36, 1000, 4, 'Old Credit', 36, 'R', 99, 'Y', 'N', 'N', 'N', 'N', 'N', 'N', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'In-active', NULL, '2012-12-18 12:20:35');

INSERT IGNORE INTO `skillset` (`SkillsetID`, `SkillsetName`, `ServiceDuration`, `EngineersRequired`, `RepairSkillID`, `ServiceTypeID`, `Status`, `AppointmentTypeID`) VALUES
	(1, 'Aerial Installation', 60, 1, 1, NULL, 'Active', 2),
	(2, 'Aerial Inspection', 120, 1, 1, NULL, 'Active', 3),
	(3, 'Aerial Repair', 60, 1, 1, NULL, 'Active', 5),
	(4, 'Brown Goods Delivery', 20, 1, 2, NULL, 'Active', 1),
	(5, 'Brown Goods Installation', 30, 1, 2, NULL, 'Active', 2),
	(6, 'Brown Goods Inspection', 60, 1, 2, NULL, 'Active', 3),
	(7, 'Brown Goods Install Wallmount', 60, 1, 2, NULL, 'Active', 7),
	(8, 'Brown Goods Collection', 30, 1, 2, NULL, 'Active', 4),
	(9, 'Brown Goods Repair', 40, 1, 2, NULL, 'Active', 5),
	(11, 'Camera Collection', 10, 1, 5, NULL, 'Active', 4),
	(12, 'Camera Send-in', 1, 1, 5, NULL, 'Active', 6),
	(13, 'Camera Repair', 60, 1, 5, NULL, 'Active', 5),
	(14, 'Computer Delivery', 15, 1, 3, NULL, 'Active', 1),
	(16, 'Computer Collection', 10, 1, 3, NULL, 'Active', 4),
	(17, 'Computer Send-in', 5, 1, 3, NULL, 'Active', 6),
	(18, 'Computer Repair', 60, 1, 3, NULL, 'Active', 5),
	(20, 'Camera Delivery', 15, 1, 5, NULL, 'Active', 1),
	(25, 'Computer Installation', 30, 1, 3, NULL, 'Active', 2),
	(29, 'Gas Delivery', 45, 1, 6, NULL, 'Active', 1),
	(30, 'Gas Installation', 120, 1, 6, NULL, 'Active', 2),
	(31, 'Gas Inspection', 240, 1, 6, NULL, 'Active', 3),
	(32, 'Gas Collection', 45, 1, 6, NULL, 'Active', 4),
	(33, 'Gas Repair', 180, 1, 6, NULL, 'Active', 5),
	(34, 'Refridgeration Delivery', 30, 1, 7, NULL, 'Active', 1),
	(35, 'Refrigeration Installation', 45, 1, 7, NULL, 'Active', 2),
	(36, 'Refrigeration Inspection', 90, 1, 7, NULL, 'Active', 3),
	(37, 'Refrigeration Collection', 15, 1, 7, NULL, 'Active', 4),
	(38, 'Refrigeration Repair', 120, 1, 7, NULL, 'Active', 5),
	(39, 'White Goods Delivery', 10, 1, 4, NULL, 'Active', 1),
	(40, 'White Goods Installation', 25, 1, 4, NULL, 'Active', 2),
	(42, 'White Goods Collection', 50, 1, 4, NULL, 'Active', 4),
	(43, 'White Goods Repair', 50, 1, 4, NULL, 'Active', 5),
	(44, 'Brown Goods Collection (2 Person)', 30, 2, 2, NULL, 'Active', 9),
	(45, 'Brown Goods Delivery (2 Person)', 20, 2, 2, NULL, 'Active', 8),
	(46, 'Brown Goods Wall Mount (2 Person)', 60, 2, 2, NULL, 'Active', 12),
	(47, 'Brown Goods Repair (2 Person)', 40, 2, 2, NULL, 'Active', 13),
	(48, 'White Goods Collection (2 Person)', 50, 2, 4, NULL, 'Active', 9),
	(49, 'White Goods Delivery (2 Person)', 10, 2, 4, NULL, 'Active', 8),
	(50, 'White Goods Installation (2 Person)', 25, 2, 4, NULL, 'Active', 11),
	(51, 'White Goods Repair (2 Person)', 50, 2, 4, NULL, 'Active', 13),
	(52, 'Aerial Inspection (2 Person)', 120, 2, 1, NULL, 'Active', 10),
	(53, 'Brown Goods Inspection (2 Person)', 60, 2, 2, NULL, 'Active', 10),
	(54, 'BANG & OLUFSEN Delivery', 15, 1, 8, NULL, 'Active', 1),
	(55, 'BANG & OLUFSEN Delivery (2 Person)', 15, 2, 8, NULL, 'Active', 8),
	(56, 'BANG & OLUFSEN Collection', 15, 1, 8, NULL, 'Active', 4),
	(57, 'BANG & OLUFSEN Collection (2 Person)', 30, 2, 8, NULL, 'Active', 9),
	(58, 'BANG & OLUFSEN Install Wallmount (2 Pers', 60, 2, 8, NULL, 'Active', 12),
	(59, 'BANG & OLUFSEN Repair', 60, 1, 8, NULL, 'Active', 5),
	(60, 'BANG & OLUFSEN Repair (2 Person)', 60, 2, 8, NULL, 'Active', 13),
	(61, 'White Goods Inspection', 30, 1, 4, NULL, 'Active', 3),
	(62, 'Brown Goods Return', 30, 1, 2, NULL, 'Active', 14),
	(63, 'Brown Goods Fit Parts', 30, 1, 2, NULL, 'Active', 15),
	(64, 'Brown Goods Fit Parts (2 Person)', 30, 2, 2, NULL, 'Active', 16);

INSERT IGNORE INTO `status` (`StatusID`, `StatusName`, `TimelineStatus`, `Colour`, `Category`, `Branch`, `ServiceProvider`, `FieldCall`, `Support`, `PartsOrders`, `RelatedTable`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, '01 UNALLOCATED JOB', 'booked', 'ef1a1a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:55'),
	(2, '02 ALLOCATED TO AGENT', 'booked', 'ffff56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:55'),
	(3, '03 TRANSMITTED TO AGENT', 'booked', '7fff00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:55'),
	(4, '04 JOB VIEWED BY AGENT', 'viewed', '007f7f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:55'),
	(5, '34 PACKAGING SENT', 'viewed', '9bdb5c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:55'),
	(6, '30 OUT CARD LEFT', 'site_visit', 'ff00ff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:55'),
	(7, '39 CX NOT AVAIL - MESSAGE LEFT', 'viewed', 'aaffff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:55'),
	(8, '40 CX NOT AVAIL - NO RESPONSE', 'viewed', 'ff7f00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:55'),
	(9, '41 CX NOT AVAIL - WRONG NUM', 'viewed', 'ff00ff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(10, '05 FIELD CALL REQUIRED', 'viewed', 'ff5656', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(11, '06 FIELD CALL ARRANGED', 'site_visit', 'ffff56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(12, '07 UNIT IN REPAIR', 'in_progress', '03bc03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(13, '31 WAITING FOR INFO', 'in_progress', 'f27f13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(14, '32 INFO RECEIVED', 'in_progress', '00bf00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(15, '08 ESTIMATE SENT', 'in_progress', '00fc0c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(16, '09 ESTIMATE ACCEPTED', 'in_progress', '56ff9a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(17, '10 ESTIMATE REFUSED', 'in_progress', '4c4c4c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(18, '11 PARTS REQUIRED', 'in_progress', 'bf5f00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(19, '12 PARTS ORDERED', 'parts_ordered', '00007f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(20, '13 PARTS RECEIVED', 'parts_received', '10eaea', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(21, '14 2ND FIELD CALL REQ.', 'parts_received', 'ff007f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(22, '15 2ND FIELD CALL BKD.', 'revisit', 'ff0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:56'),
	(23, '16 AUTHORITY REQUIRED', 'viewed', '7f0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(24, '17 AUTHORITY ISSUED', 'viewed', '02ef02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(25, '18 REPAIR COMPLETED', 'closed', '00007f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(26, '18 FIELD CALL COMPLETE', 'in_progress', '56ffff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(27, '19 DELIVER BACK REQ.', 'parts_received', 'ff7f00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(28, '20 DELIVER BACK ARR.', 'revisit', '74c607', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(29, '21C INVOICED', 'closed', 'bfbf00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(30, '21W CLAIMED', 'closed', 'ffff00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(31, '25 INVOICE PAID', 'closed', 'ffaad4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(32, '26 INVOICE RECONCILED', 'closed', '7f7f7f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(33, '29 JOB CANCELLED', 'closed', '000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(34, '33 PART NO LONGER AVAILABLE', 'parts_ordered', 'cccccc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(35, '35 EINVOICE PENDING', 'closed', '56ffff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:57'),
	(36, '36 EINVOICE REJECTED', 'closed', 'ff0004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:58'),
	(37, '37 EINVOICE RESUBMITTED', 'closed', 'ffaaaa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:58'),
	(38, '38 EINVOICE APPROVED', 'closed', '56ff56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:58'),
	(39, '00 REPAIR AUTHORISATION', 'booked', 'aabbcc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:18:58'),
	(40, '18A ALTERNATIVE RESOLUTION', 'closed', 'ffff56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:22:34'),
	(41, '29A JOB CANCELLED - NO CONTACT', 'closed', '000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:22:13'),
	(42, '07 AWAITING REPAIR', 'in_progress', '03bc03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-24 13:34:48', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:21:58'),
	(43, '07 REPAIR INVESTIGATION', 'in_progress', '03bc03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-24 13:49:21', '0000-00-00 00:00:00', 'Active', 1, '2013-01-30 15:22:02'),
	(44, '00 APPRAISAL REQUIRED', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-11 11:44:28'),
	(45, '00 APPRAISAL COMPLETE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-11 11:44:48'),
	(46, '28 RESUBMIT CLAIM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-26 12:12:12'),
	(47, '99 Service Order Closed', 'closed', '03bc03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-26 12:12:12');

INSERT IGNORE INTO `unit_type` (`UnitTypeID`, `RepairSkillID`, `UnitTypeName`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, NULL, 'UNKNOWN', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(2, 1, 'TV AERIAL', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(3, 2, 'AMPLIFIER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(4, 2, 'AUDIO', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(5, 2, 'AUDIO / DOCKING STATION', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(6, 2, 'COFFEE MACHINE', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(7, 2, 'DIGIBOX', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(8, 2, 'DIGITAL CAMCORDER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(9, 2, 'DIGITAL PHOTO FRAME', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(10, 2, 'DIGITAL SLR CAMERA', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(11, 2, 'DIGITAL STILLS CAMERA', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(12, 2, 'FREEVIEW BOX', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-11-13 15:38:58'),
	(13, 2, 'DVD/BLU RAY', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(14, 2, 'DVD/HDD', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(15, 2, 'DVD/VCR', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(16, 2, 'GAMES CONSOLE', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(17, 2, 'HOME CINEMA', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(18, 2, 'LCD PROJECTOR', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(19, 2, 'LCD TV <32inch', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(20, 2, 'LCD TV 32INCH', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(21, 2, 'LCD TV/DVD COMBI <32INCH', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(22, 2, 'LCD TV/DVD COMBI 32INCH+', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(23, 2, 'LCD/PLASMA TV 33-46INCH', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(24, 2, 'LCD/PLASMA TV 47INCH+', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(25, 2, 'LCD/PLASMA TV 33-41INCH', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(26, 2, 'LCD/PLASMA TV 42INCH+', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(27, 2, 'MICROWAVE', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(28, 2, 'MP3 PLAYER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-10-10 19:21:15'),
	(29, 2, 'PROJECTION TV', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(30, 2, 'PVR', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(31, 2, 'SATELLITE NAVIGATION SYSTEM', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-10-10 19:21:00'),
	(32, 3, 'TABLET', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-07 13:33:54'),
	(33, 2, 'WIRELESS DONGLE', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(34, 3, 'LAPTOP', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(35, 3, 'PC', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(36, 4, 'ELECTRIC COOKER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 15:02:20'),
	(37, 4, 'FRIDGE', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(38, 4, 'FRIDGE/FREEZER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(39, 4, 'WHITE GOODS', '2012-09-03 16:19:26', '2012-11-26 14:58:45', 'In-active', 1, '2012-11-26 14:58:45'),
	(42, 2, 'PROJECTOR', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(46, 4, 'DISHWASHER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(47, 4, 'FREEZER', '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(66, 2, 'DIGITAL CAMERA', '2012-10-04 11:55:05', '0000-00-00 00:00:00', 'Active', 1, '2012-10-04 11:55:05'),
	(67, 2, 'BRIDGE CAMERA', '2012-10-10 19:19:59', '0000-00-00 00:00:00', 'Active', 1, '2012-10-10 19:19:59'),
	(68, 2, 'CAMCORDER', '2012-10-10 19:20:13', '0000-00-00 00:00:00', 'Active', 1, '2012-10-10 19:20:13'),
	(69, 2, 'COMPACT CAMERA', '2012-10-10 19:20:28', '0000-00-00 00:00:00', 'Active', 1, '2012-10-10 19:20:28'),
	(70, 2, 'SKY BOX', '2012-10-10 19:20:45', '0000-00-00 00:00:00', 'Active', 1, '2012-10-10 19:20:45'),
	(71, 2, 'iPOD', '2012-10-14 14:26:32', '0000-00-00 00:00:00', 'Active', 1, '2012-10-14 14:27:01'),
	(72, 4, 'BAIN MARIE', '2012-11-26 14:31:20', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:59:40'),
	(73, 4, 'CHILLER', '2012-11-26 14:31:32', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:59:55'),
	(74, 4, 'CONTACT GRILL', '2012-11-26 14:31:50', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 15:00:05'),
	(75, 4, 'DEEP FRYER', '2012-11-26 14:32:09', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 15:00:15'),
	(76, 4, 'DISHWASHER (BUILT IN)', '2012-11-26 14:37:00', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:55:18'),
	(77, 9, 'CCTV', '2012-11-26 14:55:31', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:55:31'),
	(78, 4, 'ELECTRIC GRIDDLE', '2012-11-26 14:56:11', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:56:11'),
	(79, 4, 'ELECTRIC OVEN', '2012-11-26 14:56:25', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:56:25'),
	(80, 4, 'FREEZER (BUILT IN)', '2012-11-26 14:56:50', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:56:50'),
	(81, 4, 'FRIDGE (BUILT IN)', '2012-11-26 14:57:07', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:57:07'),
	(82, 4, 'GAS COOKER', '2012-11-26 14:57:24', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:57:24'),
	(83, 4, 'GAS HOB', '2012-11-26 14:57:39', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:57:39'),
	(84, 4, 'INDUCTION COOKER', '2012-11-26 14:57:54', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:57:54'),
	(85, 4, 'INDUCTION HOB', '2012-11-26 14:58:07', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:58:07'),
	(86, 4, 'JUICER', '2012-11-26 14:58:19', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:58:19'),
	(87, 4, 'GAS OVEN', '2012-11-26 14:58:33', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:58:33'),
	(88, 2, 'LCD/PLASMA TV 70INCH+', '2012-11-26 14:59:19', '0000-00-00 00:00:00', 'Active', 1, '2012-11-26 14:59:19'),
	(89, 2, 'DVD/BLURAY', '2013-01-11 10:26:31', '0000-00-00 00:00:00', 'Active', 1, '2013-01-11 10:26:31'),
	(90, 4, 'WASHING MACHINE', '2013-01-11 13:53:23', '0000-00-00 00:00:00', 'Active', 1, '2013-01-11 13:54:46'),
	(91, 4, 'TUMBLE DRYER', '2013-01-11 14:20:05', '0000-00-00 00:00:00', 'Active', 1, '2013-01-11 14:20:05'),
	(92, 4, 'WASHER/DRYER', '2013-01-14 16:12:39', '0000-00-00 00:00:00', 'Active', 1, '2013-01-14 16:12:58');

INSERT IGNORE INTO `vat_rate` (`VatRateID`, `Code`, `VatCode`, `VatRate`, `Status`) VALUES
	(1, 'T0', 1, 0.00, 'Active'),
	(2, 'T1', 2, 20.00, 'Active'),
	(3, 'T4', 3, 0.00, 'Active'),
	(4, 'T9', 4, 0.00, 'Active');

INSERT IGNORE INTO national_bank_holiday (NationalBankHolidayID, HolidayDate, HolidayName, CountryID) 
		VALUES (1, '2013-01-01', 'New Year\'s Day', 1), 
		(2, '2013-03-29', 'Good Friday', 1), 
		(3, '2013-04-01', 'Easter Monday', 1), 
		(4, '2013-05-06', 'Early May bank holiday', 1), 
		(5, '2013-05-27', 'Spring bank holiday', 1), 
		(6, '2013-08-26', 'Summer Bank Holliday', 1), 
		(7, '2013-12-25', 'Christmas Day', 1), 
		(8, '2013-12-26', 'Boxing Day', 1); 

REPLACE INTO `authorisation_types` (`AuthorisationTypeID`, `AuthorisationTypeName`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 'Return', NULL, '2013-06-05 15:52:22'),
	(2, 'Repair', NULL, '2013-06-05 15:52:29'),
	(3, 'On Hold', NULL, '2013-06-05 15:52:43'),
	(4, 'Estimate', NULL, '2013-06-05 15:52:52'),
	(5, 'Invoice', NULL, '2013-06-05 15:53:00');
		
REPLACE INTO `ra_status` (`RAStatusID`, `AuthorisationTypeID`, `ListOrder`, `BrandID`, `Status`, `CreatedDateTime`, `ModifiedUserID`, `ModifiedDate`, `RAStatusName`, `RAStatusCode`, `Colour`) VALUES
	(1, 2, 1, 1000, 'In-active', '2012-11-01 17:19:11', 1, '2013-01-26 18:16:54', 'New Request', 1, 'aaffff'),
	(2, 2, 2, 1000, 'Active', '2012-11-30 16:22:26', 1, '2013-01-26 18:32:03', 'Authorisation Required', 2, 'f4f409'),
	(3, 2, 3, 1000, 'Active', '2012-12-03 21:09:14', 1, '2013-01-26 18:32:33', 'Authorisation Accepted', 3, '7fff00'),
	(4, 2, 4, 1000, 'Active', '2012-12-03 21:10:14', 1, '2013-01-26 18:32:59', 'Authorisation Declined', 4, 'ff0000'),
	(5, 2, 5, 1000, 'Active', '2012-12-03 21:13:00', 1, '2013-01-26 18:33:16', 'General Query', 5, '0000ff'),
	(6, 2, 6, 1000, 'Active', '2012-12-03 21:13:45', 1, '2013-01-26 18:33:32', 'Query Answered', 6, '999999'),
	(7, 2, 1, 1000, 'In-active', '2013-05-09 14:22:20', 1, '2013-05-09 14:27:31', 'On Hold - Query', 7, '191919'),
	(8, 2, 1, 2004, 'Active', '2013-05-09 14:22:53', 1, '2013-05-10 13:53:16', 'Parts No Longer Available', 7, '191919'),
	(9, 2, 1, 1000, 'Active', '2013-05-09 17:15:45', 1, '2013-05-09 17:16:37', 'Write Off Authorised', 8, '5656ff'),
	(10, 2, 1, 1000, 'Active', '2013-05-10 13:40:16', 1, '2013-05-10 13:40:16', 'Customer Requests Write Off', 9, '333333'),
	(11, 2, 1, 1000, 'Active', '2013-05-10 13:41:08', 1, '2013-05-10 13:41:08', 'Beyond Economic Repair', 10, '666666'),
	(12, 2, 1, 1000, 'Active', '2013-05-10 13:41:44', 1, '2013-05-10 13:41:44', 'Claims Investigation', 11, '666666'),
	(13, 2, 1, 1000, 'Active', '2013-05-10 13:42:09', 1, '2013-05-10 13:42:09', 'Unit Inspected Fault Confirmed', 12, '666666'),
	(14, 2, 1, 2004, 'Active', '2013-05-10 13:42:52', 1, '2013-05-10 13:54:21', 'Customer Requests Write Off', 9, '333333'),
	(15, 2, 1, 2004, 'Active', '2013-05-10 13:43:04', 1, '2013-05-10 13:55:30', 'Beyond Economic Repair', 10, '666666'),
	(16, 2, 1, 2004, 'Active', '2013-05-10 13:43:18', 1, '2013-05-10 13:55:58', 'Claims Investigation', 11, '666666'),
	(17, 2, 1, 2004, 'Active', '2013-05-10 13:43:35', 1, '2013-05-10 13:56:22', 'Unit Inspected Fault Confirmed', 12, '666666'),
	(18, 2, 1, 1000, 'Active', '2013-05-10 13:44:37', 1, '2013-05-10 13:44:37', 'Write Off Authorised', 13, '4c4c4c'),
	(19, 2, 1, 1000, 'Active', '2013-05-10 13:45:02', 1, '2013-05-10 13:45:02', 'Rejected Please Repair', 14, '4c4c4c'),
	(20, 2, 1, 1000, 'Active', '2013-05-10 13:48:36', 1, '2013-05-10 13:48:36', 'More Info Required', 15, '4c4c4c'),
	(21, 2, 1, 1000, 'Active', '2013-05-10 13:49:08', 1, '2013-05-10 13:49:08', 'Information Update', 16, '4c4c4c'),
	(22, 2, 1, 1000, 'Active', '2013-05-10 13:49:33', 1, '2013-05-10 13:49:33', 'Return As Received', 17, '4c4c4c'),
	(23, 2, 1, 2004, 'Active', '2013-05-10 13:50:01', 1, '2013-05-12 18:45:09', 'Write Off Authorised', 13, '4c4c4c'),
	(24, 2, 1, 2004, 'Active', '2013-05-10 13:50:20', 1, '2013-05-10 13:59:54', 'Rejected Please Repair', 14, '4c4c4c'),
	(25, 2, 1, 2004, 'Active', '2013-05-10 13:50:38', 1, '2013-05-10 14:00:17', 'More Info Required', 15, '4c4c4c'),
	(26, 2, 1, 2004, 'Active', '2013-05-10 13:51:06', 1, '2013-05-10 14:00:38', 'Information Update', 16, '4c4c4c'),
	(27, 2, 1, 2004, 'Active', '2013-05-10 13:51:32', 1, '2013-05-10 14:00:53', 'Return As Received', 17, '4c4c4c'),
	(28, 2, 1, 1000, 'Active', '2013-05-10 13:57:13', 1, '2013-05-12 18:44:40', 'Incident Closed', 18, '333333'),
	(29, 2, 1, 2004, 'Active', '2013-05-10 13:58:55', 1, '2013-05-10 13:58:55', 'Claim Closed', 18, '333333');

REPLACE INTO `sb_authorisation_statuses` (`SBAuthorisationStatusID`, `SBAuthorisationStatus`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, '16 Authority Required', NULL, '2013-06-05 16:34:45'),
	(2, '16 Authority Query', NULL, '2013-06-05 16:35:29'),
	(3, '17 Authority Issued', NULL, '2013-06-05 16:35:36'),
	(4, '17 Authority Rejected', NULL, '2013-06-05 16:35:42');
	
SET FOREIGN_KEY_CHECKS=1;

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-06-18 15:39:34
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table skyline2.part_status
CREATE TABLE IF NOT EXISTS `part_status` (
  `PartStatusID` int(10) NOT NULL AUTO_INCREMENT,
  `PartStatusName` varchar(50) DEFAULT NULL,
  `PartStatusDescription` varchar(250) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `PartStatusDisplayOrder` int(4) DEFAULT NULL,
  `DisplayOrderSection` enum('Yes','No') NOT NULL DEFAULT 'No',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `Available` enum('Y','N') DEFAULT NULL,
  `InStock` enum('Y','N') DEFAULT NULL,
  PRIMARY KEY (`PartStatusID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table skyline2.part_status: ~18 rows (approximately)
/*!40000 ALTER TABLE `part_status` DISABLE KEYS */;
REPLACE INTO `part_status` (`PartStatusID`, `PartStatusName`, `PartStatusDescription`, `ServiceProviderID`, `PartStatusDisplayOrder`, `DisplayOrderSection`, `Status`, `Available`, `InStock`) VALUES
	(1, '01 IN STOCK', 'Part is from stock no order required', NULL, 1, 'No', 'Active', 'Y', 'Y'),
	(2, '02 ORDER REQUIRED', 'Part needs ordering but the order hasn\'t been processed through the system yet', NULL, 2, 'No', 'Active', 'N', 'N'),
	(3, '03 ORDERED', 'Order has been placed', NULL, 3, 'No', 'Active', 'N', 'N'),
	(4, '04 DELIVERED', 'Part has been delivered', NULL, 4, 'No', 'Active', 'N', 'N'),
	(5, '05 EXCLUDE FROM ORDER', 'Part does not require ordering but is not from stock', NULL, 5, 'No', 'Active', 'N', 'N'),
	(6, '06 PART NO LONGER AVALIABLE', 'Part is not available from supplier', NULL, 6, 'No', 'Active', 'N', 'N'),
	(7, '07 PART OUT OF STOCK', 'Part is out of stock from supplier', NULL, 7, 'No', 'Active', 'N', 'N'),
	(8, '08 FOC', 'Free of charge part, not to be charged to customer/client', NULL, 8, 'No', 'Active', 'N', 'N'),
	(9, '09 SCRAP', 'Part from scrap unit', NULL, 9, 'No', 'Active', 'Y', 'Y'),
	(10, '10 ENGINEER ORDER REQUEST', 'Part has been requested from remote engineer', NULL, 0, 'Yes', 'Active', 'N', 'N'),
	(11, '11 PARTS FITTED', 'Part has been fitted to unit', NULL, 11, 'No', 'Active', 'N', 'N'),
	(12, '12 PARTS BEING SENT', 'Parts have been sent to remote engineer', NULL, 12, 'No', 'Active', 'N', 'N'),
	(13, '13 RETURN TO SUPPLIER', 'Part requires sending back to the supplier', NULL, 13, 'No', 'Active', 'N', 'N'),
	(14, '14 RETURED', 'Part has been returned to supplier', NULL, 14, 'No', 'Active', 'N', 'N'),
	(15, '15 PART REQUEST', '?', NULL, 15, 'No', 'Active', 'N', 'N'),
	(16, '16 SUPPLIER NOTIFIED', 'Supplier notified of issue with part', NULL, 16, 'No', 'Active', 'N', 'N'),
	(17, '30 ADJUSTMENT ONLY', 'Part is an adjustment only', NULL, 17, 'No', 'Active', 'N', 'N'),
	(18, '18 REQUSITIONED', '?', NULL, 18, 'No', 'Active', 'N', 'N'),
	(19, '19 REQUISITION APPROVED', 'REQUISITION APPROVED BY MANAGER', NULL, 19, 'No', 'Active', 'N', 'N');
/*!40000 ALTER TABLE `part_status` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
