<style>
.cardLabel{
    padding-top:0px;
    }
    sup{
        color:red;
        }
</style>
<script>
function calculateCost(f){
    var exRate={$currencyRate};
      spC=$('#PurchasePricePerItem').val();
      supC=$('#PurchasePricePerItemC').val();
 
      
        if(f==1){
           $('#PurchasePricePerItemC').val((spC * exRate).toFixed(2))
        }
        if(f==2){
            $('#PurchasePricePerItem').val((supC / exRate).toFixed(2))
        }
       
      }

function submitForm(){

}

function validateQTY(){
$('#qtyError').hide();
if({$data.QTYOrdered|default:'0'}<$('#QTYReceived').val()*1){
$('#QTYReceived').focus().select();
$('#QTYReceived').val('');
$('#qtyError').show();
}
}

$(document).ready(function(){
    /////////validation
    
    $("#receiveForm").validate = null;
	
	$("#receiveForm").validate({
	    rules: {
                       
                        QTYReceived:{
                             required: function (element) {
                                    }    
                                  },
                        PurchasePricePerItem:{
                             required: function (element) {
                                   
                                   }
                                   },
                        SupplierInvoiceNo:{
                             required: function (element) {
                                   
                                   }
                                   }
                              
                        

                    },
	   
 
	    errorPlacement: function(error, element) {
		error.appendTo(element.parent("p"));
                
                $.colorbox.resize();
                
	    },
	    ignore:	    "",
	    errorClass:	    "fieldError2",
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   "label",
             submitHandler: function() {
             $('#butholder').hide();
             $.post("{$_subdomain}/StockControl/ReceiveParts/", $('#receiveForm').serialize(),
function(data) {
$.colorbox.close();
oTable = $('#StockResults').dataTable();
oTable.fnReloadAjax();
             });
             }
	   
	});
});
</script>
<div class="SystemAdminFormPanel" style="width:700px;">
    <fieldset>
        <form id="receiveForm" action="#">
            <input type="hidden" name="currencyRate" value="{$currencyRate}">
            <input type="hidden" name="items" value="{$data.items|default:'error no items'}">
            <input type="hidden" name="SPPartOrderID" value="{$data.OrderNo|default:'&nbsp;'}">
            <input type="hidden" name="spCurrency" value="{$data.spCurrency}">
            <input type="hidden" name="supCurrency" value="{$data.supCurrency}">
            <input type="hidden" name="ServiceProviderSupplierID" value="{$data.DefaultServiceProviderSupplierID|default:'&nbsp;'}">
            <input type="hidden" name="QtyOrdered" value="{$data.QTYOrdered|default:'0'}">
        <legend>{$page['Text']['ReceivePart']}</legend>
           <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

          </p>
          <p>
                           <label  class="cardLabel" for="OrderNo" >{$page['Labels']['OrderNo']|escape:'html'}:</label>
                           
                           <span  class="cardSpan" >{$data.OrderNo|default:'&nbsp;'}</span>
       </p>
     <p>
                           <label  class="cardLabel" for="PartNumber" >{$page['Labels']['PartNumber']|escape:'html'}:</label>
                           
                           <span  class="cardSpan" >{$data.PartNumber|default:'&nbsp;'}</span>
     </p>
     <p>
                           <label  class="cardLabel" for="PartDescription" >{$page['Labels']['PartDescription']|escape:'html'}:</label>
                           
                           <span  class="cardSpan" >{$data.PartDescription|default:'&nbsp;'}</span>
     </p>
     <p>
                           <label  class="cardLabel" for="qty" >{$page['Labels']['Supplier']|escape:'html'}:</label>
                           
                           <span  class="cardSpan" >{$data.Supplier|default:'&nbsp;'}</span>
     </p>
     <p>
                           <label  class="cardLabel" for="qty" >{$page['Labels']['QTYOrdered']|escape:'html'}:</label>
                           
                           <span  class="cardSpan" >{$data.QTYOrdered|default:'&nbsp;'}</span>
     </p>
     <p  id="qtyError" style="color:red;display:none">
     <span >You cannot receive more items then you ordered</span>
     </p>
     <p>
                           <label required=required class="cardLabel" for="qty" >{$page['Labels']['QTYReceived']|escape:'html'}<sup>*</sup>:</label>
                           
                           <input id="QTYReceived" onchange="validateQTY()"  style="width:50px;" name="QTYReceived" value="{$data.QTYReceived|default:''}">
                           
     </p>
     <p>
         <label  class="cardLabel" for="PurchasePricePerItem" >{$page['Labels']['PurchasePricePerItem']|escape:'html'}<sup>*</sup>:</label>
                           
        <input id="PurchasePricePerItem" required="required"  onchange="calculateCost(1)" style="width:50px;" name="PurchasePricePerItem" value="{$data.PurchasePricePerItem|default:''}">
                        
                         <span>{$data.spCurrency}</span>
     <span {if $data.spCurrency==$data.supCurrency}style="display:none"{/if}>
                        
       
         
         <input id="PurchasePricePerItemC"  onchange="calculateCost(2)" style="width:50px;" name="PurchasePricePerItemC" value="{($data.PurchasePricePerItem*$currencyRate)|string_format:"%.2f"}">
                        
         
         
         <span>{$data.supCurrency}</span>
                         </span>
     </p>
     <p>
                           <label  class="cardLabel" for="SupplierInvoiceNo" >{$page['Labels']['SupplierInvoiceNo']|escape:'html'}<sup>*</sup>:</label>
                           
                         <input required="required" id="SupplierInvoiceNo"  onchange="" style="width:150px;" name="SupplierInvoiceNo" value="{$data.SupplierInvoiceNo|default:''}">
     </p>
     <p>
                           <label  class="cardLabel" for="PartLocation" >{$page['Labels']['PartLocation']|escape:'html'}:</label>
                           
                         <span  class="cardSpan" >{$data.PartLocation|default:'&nbsp;'}</span>
     </p>
     <p>
                           <label  class="cardLabel" for="ShelfLocation" >{$page['Labels']['ShelfLocation']|escape:'html'}:</label>
                           
                            <select   name="ShelfLocation" id="ShelfLocation"  >
                               
                                   
                                {foreach $ShelfLocation|default:"" as $s}

                                    <option value="{$s.ShelfLocation|default:""}" >{$s.ShelfLocation|default:""}</option>
                                {/foreach}
                                
                            </select>
                        
     </p>
     <p>
                           <label  class="cardLabel" for="BinLocation" >{$page['Labels']['BinLocation']|escape:'html'}:</label>
                           
                        <select   name="BinLocation" id="BinLocation"  >
                               
                                   
                                {foreach $BinLocation|default:"" as $s}

                                    <option value="{$s.BinLocation|default:""}" >{$s.BinLocation|default:""}</option>
                                {/foreach}
                                
                            </select>
     </p>
     <div id="butholder" style="height:20px;margin-bottom: 10px;text-align: center;">
                                <button type="submit" style="margin-left:38px" class="gplus-blue centerBtn">Save</button>
                                <button type="button" onclick="$.colorbox.close();"  class="gplus-blue" style="float:right">Cancel</button>
                                </div>
     </form>
    </fieldset>

</div>