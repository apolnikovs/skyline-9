{if $CancelPage}
    
    <div id="AppointmentResultsPanel" class="SystemAdminFormPanel" >
    
                <form id="AppointmentForm" name="AppointmentForm" method="post"  action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                      <p>
                        <label id="suggestText" ></label></p>
                      <p>
                         {$page['Text']['confirm_cancel_text']|escape:'html'} 
                      </p>
                      
                       <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="APT_AppointmentID"  value="{$AppointmentID|escape:'html'}" >
                                    <input type="hidden" name="APT_Cancelled"  value="Yes" >
                                    
                                        
                                        <input type="submit" name="delete_save_btn_apt" class="textSubmitButton" id="delete_save_btn_apt"  value="{$page['Buttons']['yes_text']|escape:'html'}" >
                                    
                                   
                                   
                                        &nbsp;&nbsp;
                                        
                                        <input type="submit" name="cancel_btn_apt" class="textSubmitButton" id="cancel_btn_apt" onclick="return false;"  value="{$page['Buttons']['no_text']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
    
{else}
    
<script type="text/javascript">   
     
 $(document).ready(function() {
 
            $(document).on('change', "#APT_ServiceProviderID", function() {
            
                $("#APT_ServiceProviderEngineerID").html('');
                $engineerDropDownList = '<option value="" selected="selected">Select from drop down</option>';
            
            
                 $.post("{$_subdomain}/Data/getEngineers/"+$("#APT_ServiceProviderID").val(),        

                                '',      
                                function(data){
                                        var $spEngineers = eval("(" + data + ")");

                                        if($spEngineers)
                                        {

                                            for(var $i=0;$i<$spEngineers.length;$i++)
                                            {

                                                $engineerDropDownList += '<option value="'+$spEngineers[$i]['ServiceProviderEngineerID']+'" >'+$spEngineers[$i]['EngineerName']+'</option>';
                                            }
                                        }

                                         $("#APT_ServiceProviderEngineerID").html('');
                                         $("#APT_ServiceProviderEngineerID").html($engineerDropDownList);
                                         
                                       
                                });
   
                        return false;        
            });
            
            
                 
          $( "#APT_AppointmentDate" ).datepicker({
			dateFormat: "dd/mm/yy",
                        showOn: "button",
			buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/calendar.png",
			buttonImageOnly: true,
                        onClose: function(dateText, inst) { 
                        
                        
                        },
                        onSelect: function(dateText, inst) { 
                        
                       
                             
                        }
                    });  
                               
   
   
   
   }); 
   
</script>  
    
    <div id="AppointmentResultsPanel" class="SystemAdminFormPanel" >
    
                <form id="AppointmentForm" name="AppointmentForm" method="post"  action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                      <p>
                        <label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
                         
                            <p>
                               <label  for="APT_AppointmentDate" >{$page['Labels']['appointment_date']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp; <input  type="text" class="text"  name="APT_AppointmentDate" value="{$datarow.AppointmentDate|escape:'html'}" id="APT_AppointmentDate" >

                            </p>
                         
                            
                            <p>
                               <label  for="APT_AppointmentTime" >{$page['Labels']['timeslot']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp; <input  type="radio" name="APT_AppointmentTime" value="AM" {if $datarow.AppointmentTime eq 'AM'} checked="checked" {/if} > AM
                                <input  type="radio" name="APT_AppointmentTime" value="PM" {if $datarow.AppointmentTime eq 'PM'} checked="checked" {/if} > PM
                                <input  type="radio" name="APT_AppointmentTime" value="ANY" {if $datarow.AppointmentTime eq 'ANY'} checked="checked" {/if} > AT

                            </p>
                         
                            
                            <p>
                               <label  for="APT_AppointmentType" >{$page['Labels']['appointment_type']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp;
                                
                               
                                <select name="APT_AppointmentType" id="APT_AppointmentType" class="text" >
                                    <option value="">{$page['Text']['select_default_option']|escape:'html'}</option>
                                    {foreach $AppointmentTypes as $AppointmentType}
                                    
                                    <option value="{$AppointmentType.Type|upper|escape:'html'}" {if $datarow.AppointmentType eq $AppointmentType.Type|upper|escape:'html'} selected="selected" {/if}  >{$AppointmentType.Type|upper|escape:'html'}</option>
                                    
                                    {/foreach}
                                </select> 
                               
                            </p>
                         
                            
                            <p>
                               <label  for="APT_Notes" >{$page['Labels']['appointment_notes']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp; <textarea  style="width:310px;height:150px;" class="text" name="APT_Notes" id="APT_Notes" >{$datarow.Notes|escape:'html'}</textarea>

                            </p>
                            
                            
                            {if $SuperAdmin}
                            
                                 <p>
                                    <label  for="APT_ServiceProviderID" >{$page['Labels']['service_provider']|escape:'html'}:<sup>*</sup></label>

                                     &nbsp;&nbsp;


                                     <select name="APT_ServiceProviderID" id="APT_ServiceProviderID" class="text" >
                                         <option value="">{$page['Text']['select_default_option']|escape:'html'}</option>
                                         {foreach $ServiceProviders as $ServiceProvider}

                                         <option value="{$ServiceProvider.ServiceProviderID}" {if $datarow.ServiceProviderID eq $ServiceProvider.ServiceProviderID} selected="selected" {/if}  >{$ServiceProvider.CompanyName|upper|escape:'html'}</option>

                                         {/foreach}
                                     </select> 

                                 </p>
                                 
                                 
                                 
                                
                            {else}
                                <input name="APT_ServiceProviderID" id="APT_ServiceProviderID" type="hidden" value="{$ServiceProviderID|escape:'html'}" >
                                
                            {/if}  
                            
                            <p>
                                    <label  for="APT_ServiceProviderEngineerID" >{$page['Labels']['engineer']|escape:'html'}:<sup>*</sup></label>

                                     &nbsp;&nbsp;


                                <select name="APT_ServiceProviderEngineerID" id="APT_ServiceProviderEngineerID" class="text" >

                                    <option value="">{$page['Text']['select_default_option']|escape:'html'}</option>
                                    {foreach $EngineersList as $Engineer}

                                    <option value="{$Engineer.ServiceProviderEngineerID}" {if $datarow.ServiceProviderEngineerID eq $Engineer.ServiceProviderEngineerID} selected="selected" {/if}  >{$Engineer.EngineerName|upper|escape:'html'}</option>

                                    {/foreach}
                                </select> 

                            </p>
                            
                            

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="APT_AppointmentID"  value="{$datarow.AppointmentID|escape:'html'}" >
                                    <input type="hidden" name="APT_JobID"  value="{$datarow.JobID|escape:'html'}" >
                                   
                                    {if $datarow.AppointmentID neq '' && $datarow.AppointmentID neq '0'}
                                        
                                        <input type="submit" name="update_save_btn_apt" class="textSubmitButton" id="update_save_btn_apt"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn_apt" class="textSubmitButton" id="insert_save_btn_apt"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        &nbsp;&nbsp;
                                        
                                        <input type="submit" name="cancel_btn_apt" class="textSubmitButton" id="cancel_btn_apt" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                                        
{/if}
                 

                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>
                        
                        
                        
                                                          
{* This block of code is for to display message after data deletion *} 

                                    
<div id="dataDeletedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataDeletedMsgForm" name="dataDeletedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_deleted_msg1']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>
                        
