<script type="text/javascript">
    $(document).ready(function() {
       $("#ServiceTypeMask").combobox(); 
    });
</script>
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <div id="serviceTypesFormPanel" class="SystemAdminFormPanel" >
    
                <form id="STForm" name="STForm" method="post"  action="#" class="inline">
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                           
                            </p>
                       
        
                         
                            <p>
                               <label style='width:130px' for="ServiceTypeMask" >{$page['Labels']['service_type_mask']|escape:'html'}:</label>

                                &nbsp;&nbsp;
                                
                                
                                <select name="ServiceTypeMask" id="ServiceTypeMask" >
                               
                                    <option value="" {if $datarow.ServiceTypeMask eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $serviceTypes as $serviceType}
                                        
                                        {if $datarow.ServiceTypeID neq $serviceType.ServiceTypeID}
                                            
                                            <option value="{$serviceType.ServiceTypeID|escape:'html'}" {if $datarow.ServiceTypeMask eq $serviceType.ServiceTypeID} selected="selected" {/if}>{$serviceType.Name|escape:'html'}</option>
                                        {/if}    
                                            
                                    {/foreach}

                                </select>
                                
                            </p>
                             <p>
                              
                    <label style='width:130px;margin-right:18px;' for="ServiceTypeMask" > {$page['Labels']['charge_type']|escape:'html'}: </label>
                     <input {if $datarow.Warranty|default:'N'=="Y"}checked=checked{/if} type="checkbox" value="Y" name="Warranty"><span>{$page['Labels']['warranty']|escape:'html'}</span>
                     <input {if $datarow.Chargeable|default:'N'=="Y"}checked=checked{/if} type="checkbox" value="Y" name="Chargeable"><span>{$page['Labels']['chargeable']|escape:'html'}</span>
                    </p>
                            
                            <p>
                               <label style='width:130px'  for="Status" >{$page['Text']['assigned']|escape:'html'}</label>
                               &nbsp;&nbsp;
                               <input  type="radio"   name="Status" value="Active" {if $datarow.Status eq 'Active'} checked='checked' {/if}  > {$page['Text']['yes_text']|escape:'html'}
                               <input  type="radio"   name="Status" value="In-active" {if $datarow.Status neq 'Active'} checked='checked' {/if}  > {$page['Text']['no_text']|escape:'html'}

                            </p>
                         
                         
                       
                         
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                         <input type="hidden" name="ServiceTypeID"  value="{$datarow.ServiceTypeID|escape:'html'}" >
                                         <input type="hidden" name="ServiceTypeAliasID"  value="{$datarow.ServiceTypeAliasID|escape:'html'}" >
                                         <input type="hidden" name="NetworkID"  value="{$datarow.NetworkID|escape:'html'}" >
                                         <input type="hidden" name="ClientID"  value="{$datarow.ClientID|escape:'html'}" >
                                    

                                      
                                         <input type="submit" name="update_save_btn" class="textSubmitButton" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    &nbsp;&nbsp;
                                   
                                        
                                        <input type="submit" name="cancel_btn" class="textSubmitButton" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    