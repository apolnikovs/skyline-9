# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.145');


# ---------------------------------------------------------------------- #
# Modify table "non_skyline_job"                                         #
# ---------------------------------------------------------------------- #

ALTER TABLE `non_skyline_job` ADD COLUMN `DeliveryAddress1` VARCHAR(50) NULL AFTER `ReportedFault`;
ALTER TABLE `non_skyline_job` ADD COLUMN `DeliveryAddress2` VARCHAR(50) NULL AFTER `DeliveryAddress1`;
ALTER TABLE `non_skyline_job` ADD COLUMN `DeliveryAddress3` VARCHAR(50) NULL AFTER `DeliveryAddress2`;
ALTER TABLE `non_skyline_job` ADD COLUMN `DeliveryAddress4` VARCHAR(50) NULL AFTER `DeliveryAddress3`;
ALTER TABLE `non_skyline_job` ADD COLUMN `DeliveryPostcode` VARCHAR(8) NULL AFTER `DeliveryAddress4`;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.146');