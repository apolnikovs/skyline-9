# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-08-30 09:44                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.76');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `network` DROP FOREIGN KEY `country_TO_network`;

ALTER TABLE `network` DROP FOREIGN KEY `county_TO_network`;

ALTER TABLE `network` DROP FOREIGN KEY `user_TO_network`;

ALTER TABLE `network` DROP FOREIGN KEY `client_TO_network`;

ALTER TABLE `job` DROP FOREIGN KEY `network_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `branch_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `client_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `customer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `product_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `manufacturer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `job_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `status_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `model_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job_ModifiedUser`;

ALTER TABLE `network_client` DROP FOREIGN KEY `network_TO_network_client`;

ALTER TABLE `network_manufacturer` DROP FOREIGN KEY `network_TO_network_manufacturer`;

ALTER TABLE `network_service_provider` DROP FOREIGN KEY `network_TO_network_service_provider`;

ALTER TABLE `user` DROP FOREIGN KEY `network_TO_user`;

ALTER TABLE `audit` DROP FOREIGN KEY `job_TO_audit`;

ALTER TABLE `part` DROP FOREIGN KEY `job_TO_part`;

ALTER TABLE `appointment` DROP FOREIGN KEY `job_TO_appointment`;

ALTER TABLE `contact_history` DROP FOREIGN KEY `job_TO_contact_history`;

ALTER TABLE `brand` DROP FOREIGN KEY `network_TO_brand`;

ALTER TABLE `client_branch` DROP FOREIGN KEY `network_TO_client_branch`;

ALTER TABLE `unit_pricing_structure` DROP FOREIGN KEY `network_TO_unit_pricing_structure`;

ALTER TABLE `product` DROP FOREIGN KEY `network_TO_product`;

ALTER TABLE `service_type_alias` DROP FOREIGN KEY `network_TO_service_type_alias`;

ALTER TABLE `status_history` DROP FOREIGN KEY `job_TO_status_history`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `network_TO_town_allocation`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `network_TO_central_service_allocation`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `network_TO_postcode_allocation`;

ALTER TABLE `bought_out_guarantee` DROP FOREIGN KEY `network_TO_bought_out_guarantee`;

ALTER TABLE `claim_response` DROP FOREIGN KEY `job_TO_claim_response`;

# ---------------------------------------------------------------------- #
# Modify table "network"                                                 #
# ---------------------------------------------------------------------- #

ALTER TABLE `network` ADD COLUMN `Terms` TEXT;

ALTER TABLE `network` ADD COLUMN `CustomerAssurancePolicy` TEXT;

ALTER TABLE `network` ADD COLUMN `TermsConditions` TEXT;

ALTER TABLE `network` MODIFY `Terms` TEXT AFTER `ContactEmail`;

ALTER TABLE `network` MODIFY `CustomerAssurancePolicy` TEXT AFTER `Terms`;

ALTER TABLE `network` MODIFY `TermsConditions` TEXT AFTER `CustomerAssurancePolicy`;

# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` ADD COLUMN `EstimateStatus` ENUM('Awaiting Confiirmation','Accepted','Rejected');

# ---------------------------------------------------------------------- #
# Add table "Service_Provider_NPS"                                       #
# ---------------------------------------------------------------------- #

CREATE TABLE `Service_Provider_NPS` (
    `JobNoID` INTEGER,
    `Rating` INTEGER,
    `Testimonial` TEXT,
    `Date` DATE,
    `Time` TIME
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "Contact_Us_Subject"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `Contact_Us_Subject` (
    `ContactUsSubjectID` INTEGER NOT NULL,
    `Subject` TEXT,
    `PriorityOrder` INTEGER,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ContactUsSubjectID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "Contact_Us_Messages"                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `Contact_Us_Messages` (
    `ContactUsMessageID` INTEGER NOT NULL,
    `CustomerID` INTEGER,
    `ContactUsSubjectID` INTEGER,
    `Message` TEXT,
    `CreatedDate` DATE,
    `EndDate` DATE,
    `Status` ENUM('Active','In-active'),
    PRIMARY KEY (`ContactUsMessageID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "Welcome_Messages"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `Welcome_Messages` (
    `WelcomeMessageID` INTEGER NOT NULL,
    `CustomerID` INTEGER,
    `WelcomeMessageDefaultsID` INTEGER,
    `JobID` INTEGER,
    `CreatedDateTime` DATETIME,
    `EndDate` DATE,
    `Status` ENUM('Active','In-active'),
    PRIMARY KEY (`WelcomeMessageID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "Welcome_Message_Defaults"                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `Welcome_Message_Defaults` (
    `WelcomeMessageDefaultsID` INTEGER NOT NULL,
    `Title` TEXT,
    `HyperlinkText` TEXT,
    `Message` TEXT,
    `Type` ENUM(),
    `CreatedDateTime` DATETIME,
    `ModifiedUserID` INTEGER,
    `ModifiedDate` DATE,
    PRIMARY KEY (`WelcomeMessageDefaultsID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `network` ADD CONSTRAINT `country_TO_network` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `network` ADD CONSTRAINT `county_TO_network` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `network` ADD CONSTRAINT `user_TO_network` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network` ADD CONSTRAINT `client_TO_network` 
    FOREIGN KEY (`DefaultClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `network_TO_job` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `job` ADD CONSTRAINT `branch_TO_job` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `job` ADD CONSTRAINT `product_TO_job` 
    FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`);

ALTER TABLE `job` ADD CONSTRAINT `service_type_TO_job` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `manufacturer_TO_job` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `job` ADD CONSTRAINT `job_type_TO_job` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `status_TO_job` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `job` ADD CONSTRAINT `model_TO_job` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job` 
    FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job_ModifiedUser` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_client` ADD CONSTRAINT `network_TO_network_client` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `network_manufacturer` ADD CONSTRAINT `network_TO_network_manufacturer` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `network_TO_network_service_provider` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `user` ADD CONSTRAINT `network_TO_user` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `audit` ADD CONSTRAINT `job_TO_audit` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `part` ADD CONSTRAINT `job_TO_part` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `job_TO_appointment` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `contact_history` ADD CONSTRAINT `job_TO_contact_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `brand` ADD CONSTRAINT `network_TO_brand` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `network_TO_client_branch` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `network_TO_unit_pricing_structure` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `product` ADD CONSTRAINT `network_TO_product` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `network_TO_service_type_alias` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `status_history` ADD CONSTRAINT `job_TO_status_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `network_TO_town_allocation` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `network_TO_central_service_allocation` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `network_TO_postcode_allocation` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `network_TO_bought_out_guarantee` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `claim_response` ADD CONSTRAINT `job_TO_claim_response` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.77');
