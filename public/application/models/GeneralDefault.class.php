<?php
require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * GeneralDefault.class.php
 * 
 * This model handles interaction with the general_default table.
 * 
 * @author      Andrew J. Williams <a.williams@pccsuk.com>
 * @version     1.0
 * @copyright   2012 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author               Reason
 * 26/06/2012  1.00    Andrew J. Williams   Initial Version
 ******************************************************************************/

class GeneralDefault extends CustomModel { 
    
    private $conn;                                                              /* Database connection */
    private $tbl;                                                               /* For Table Factory Class */
    private $dbColumns = array(
                               'gd.`GeneralDefaultID`',
                               'gd.`IDNo`',
                               'gd.`Category`',
                               'gd.`DefaultName`',
                               'gd.`Default`',
                               'gd.`Description`',
                               'b.`BrandName`',
                               'gd.`CreatedDate`',
                               'gd.`EndDate`',
                               'gd.`Status`',
                               'gd.`ModifiedUserID`',
                               'gd.`ModifiedDate`'
                              );
    private $tables = "general_default gd LEFT JOIN brand b ON gd.BrandID = b.BrandID";
    private $table = "general_default";
    
    public function __construct($Controller) {
                  
        parent::__construct($Controller); 
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        $this->tbl = TableFactory::GeneralDefault();
    }
       
    /**
     * fetch
     *  
     * Get all the items from the table
     * 
     * @param array $args   Associative array
     * 
     * @return array  records in database
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function fetch($args) {
        
        if($this->controller->user->SuperAdmin)
        {
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        }
        else if(is_array($this->controller->user->Brands))
        {    
           $brandsList  = implode(",", array_keys($this->controller->user->Brands));
           
           
           
           if($brandsList)
           {
                $brandsList .= ",".$this->controller->SkylineBrandID;
           }    
          
          
            $args['where'] = "gd.BrandID IN (".$brandsList.")";

            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
       
        }
       
        return ($output);
    }
    
    /**
     * processData
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args   Associative array contains all elements of submitted form.
     *
     * @return array        Status and message.
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function processData($args) { 
        
        if(!isset($args['GeneralDefaultID']) || $args['GeneralDefaultID'] == 0)
        {
            return $this->create($args);
        }
        else
        {
            return $this->update($args);
        }
     }
    
     /**
      * fetchRow
      * 
      * This method is used for to fetch a row from database.
      *
      * @param  array $args
      * 
      * @return array It contains row of the given primary key.
      * 
      * @author Andrew Williams <a.williams@pccsuk.com> 
      *************************************************************************/ 
     public function fetchRow($args) {
        
         $sql = "
                 SELECT
			gd.`GeneralDefaultID`,
			gd.`IDNo`,
			gd.`Category`,
			gd.`DefaultName`,
			gd.`Default`,
			gd.`Description`,
			gd.`BrandID`,
                        gd.`CreatedDate`,
			gd.`Status`
		 FROM
			`general_default` gd
		 WHERE
			gd.`GeneralDefaultID` = {$args['GeneralDefaultID']}
                ";
        
        $result = $this->Query($this->conn, $sql);

        return($result[0]);
     }
     
     
     
      /**
     * Description
     * 
     * This method finds general default value for given BrandID and IDNo.
     * 
     * @param integer $BrandID This is primary key of brand table.
     * @param integer $IDNo
     * @global $this->table
     * 
     * @return string It returns value.
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function getValue($BrandID, $IDNo) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = "SELECT `Default` FROM ".$this->table." WHERE (BrandID=:BrandID OR BrandID='1000') AND IDNo=:IDNo ORDER BY BrandID DESC LIMIT 0,1";
        
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        
        
        $fetchQuery->execute(array(':BrandID' => $BrandID, ':IDNo' => $IDNo));
        $result = $fetchQuery->fetch();
        
        //$this->controller->log(var_export($result, true));
        
        if(isset($result[0]))
        {
           return $result[0];
        }
        else
        {
             return NULL;
        }
       
    }
     
     
     
     
      /**
     * Description
     * 
     * This method finds the maximum code for given brand in database table.
     * 
     * @param interger $BrandID This is primary key of brand table.
     * @global $this->table
     * @return integer It returns maximum code if it finds in the database table otherwise it returns 0.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function getCode($BrandID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT IDNo FROM '.$this->table.' WHERE BrandID=:BrandID ORDER BY IDNo DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        //$this->controller->log(var_export($BrandID, true));
        
        $fetchQuery->execute(array(':BrandID' => $BrandID));
        $result = $fetchQuery->fetch();
        if(isset($result[0]))
        {
           return $result[0];
        }
        else
        {
             return 0;
        }
       
    }
    
    
    /**
     * Description
     * 
     * This method is used for to validate action code for given brand.
     *
     * @param interger $IDNo  
     * @param interger $BrandID.
     * @param interger $GeneralDefaultID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($IDNo, $BrandID, $GeneralDefaultID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT IDNo FROM '.$this->table.' WHERE IDNo=:IDNo AND BrandID=:BrandID AND GeneralDefaultID!=:GeneralDefaultID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':IDNo' => $IDNo, ':BrandID' => $BrandID, ':GeneralDefaultID' => $GeneralDefaultID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['IDNo'])
        {
                return false;
        }
        
        return true;
    
    }
     
     
     
    /**
     * create
     * 
     * This method is used for to update an item in the database.
     *
     * @param array $args  The fields to be updated. It must invlude the primary
     *                     key GeneralDefaultID
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function create($args) {
        
        if ( isset($args['OldStatus']) ) {                                      /* If  OldStatus is set */
                $oldStatus = $args['OldStatus'];                                /* Take copy of value */
                unset($args['OldStatus']);                                      /* And delete from arguments as it is not a field to update! */
        }
        unset($args['GeneralDefaultID']);
        
        if(!isset($args['IDNo']) || !$args['IDNo'])
        {
            $args['IDNo'] = $this->getCode($args['BrandID'])+1;//Preparing next type code.
        }
        
        if($this->isValidAction($args['IDNo'], $args['BrandID'], 0))
        {
        
            $cmd = $this->tbl->insertCommand($args);
            $result = $this->Execute($this->conn, $cmd, $args);
            
             if($result) {
                return (array(
                            'status' => 'OK',
                            'message' => $this->controller->page['Text']['data_inserted_msg']
                            )
                    );
            } else {
                $this->controller->log($this->conn->errorCode().": ".$this->conn->errorInfo());
                return (array(
                            'status' => 'ERROR',
                            'message' => $this->controller->page['Errors']['data_not_processed'],
                            'code' => $this->conn->errorCode(),
                            )
                    );
            }
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
       

    }
     
    /**
     * update
     * 
     * This method is used for to update an item in the database.
     *
     * @param array $args  The fields to be updated. It must invlude the primary
     *                     key GeneralDefaultID
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function update($args) {
        if ( isset($args['OldStatus']) ) {                                      /* If  OldStatus is set */
                $old_status = $args['OldStatus'];                                /* Take copy of value */
                unset($args['OldStatus']);                                      /* And delete from arguments as it is not a field to update! */
        }
        
        if ( ($old_status == "Active") AND ($args['Status'] == "In-active") ) {
            $args['EndDate'] = date("Y-m-d H:i:s");                                   /* Switch from active to active, set end date to now */
        } elseif ( ($old_status == "In-active") AND ($args['Status'] == "Active") ) {
            $args['EndDate'] = null;                                            /* Switch from inactive to active, clear end date */
        }
        
        if($this->isValidAction($args['IDNo'], $args['BrandID'], $args['GeneralDefaultID']))
        { 
        
            $cmd = $this->tbl->updateCommand($args);

            $result = $this->Execute($this->conn, $cmd, $args);

            if($result) {
                return (array(
                            'status' => 'OK',
                            'message' => $this->controller->page['Text']['data_updated_msg']
                            )
                    );
            } else {
                return (array(
                            'status' => 'ERROR',
                            'message' => $this->controller->page['Errors']['data_not_processed']
                            )
                    );
            }
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
}
?>
