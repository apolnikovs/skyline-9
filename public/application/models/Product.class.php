<?php
require_once('CustomModel.class.php');

/**
 * Description of Product
 *
 * @author s.tsang@pccsuk.com
 * 
 * productSearch:   [search term]
 * productGroup:    $search_fields
 * 
 * $permissions     RWD
 * $display_buttons Add, Edit, Delete, View, Pick
 * 
 */
class Product extends CustomModel {
    private $conn;
    private $dbColumns = array('ProductID', 'ProductNo', 'ModelDescription',  'ManufacturerName', 'ModelName', 'UnitTypeName');
    private $matches = null;    
    private $permissions = 'R';
    
    private $tables = "product AS t1 LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID
                        LEFT JOIN manufacturer AS t4 ON t3.ManufacturerID = t4.ManufacturerID
                        LEFT JOIN unit_type AS t5 ON t3.UnitTypeID = t5.UnitTypeID";

    # db name and display names
    public $display_columns = array(
        'ProductID' => 'Catalogue Item ID',        
        'ProductNo' => 'Product Code', 
        'ModelDescription' => 'Product Description',  
        'ManufacturerName' => 'Manufacturer', 
        'ModelName' => 'Model No.', 
        'UnitTypeName' => 'Product Type'
    );

    # standard search signle field search
    public $search_fields = array(
        'ProductNo' => 'Product Code',
        'ModelName' => 'Model No.',
        'ModelDescription' => 'Description of Product'
    );
    
    public $default_search_field = 'ProductNo';
  
    # advanced search combination fields
    public $advanced_search_fields = array(
        'ProductNo' => 'Product Code',
        'ModelDescription' => 'Product Description',
        'ManufacturerName' => 'Manufacturer',
        'UnitTypeName' => 'Product Type',
        'ModelName' => 'Model No.'
    );
    
    public $search_term = '';
    public $search_type = 'ProductID'; 
    public $search_result = '';  
    public $super_filter = '""';       
    
    
    public function  __construct($Controller) {
                  
        parent::__construct($Controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        
        $this->UpdateAlternativeFieldsNames();
        
    }
    
    
    
    public function select($productNo=null, $clientID=null) {
        
        if ($clientID == null) {
            $clientID = $this->controller->user->ClientID;
        }
        
        $sql = "SELECT	    p.ProductID, 
			    p.ClientID, 
			    p.NetworkId, 
			    p.ProductNo, 
			    p.ActualSellingPrice, 
			    p.AuthorityLimit, 
			    p.PaymentRoute, 
			    m.ModelID, 
			    m.ModelNumber, 
			    m.ModelName, 
			    m.ModelDescription, 
			    ut.UnitTypeID, 
			    ut.UnitTypeName, 
			    rs.RepairSkillID, 
			    rs.RepairSkillName, 
			    mfg.ManufacturerID, 
			    mfg.ManufacturerName
                FROM	    product p
                LEFT JOIN   model m ON (m.ModelID=p.ModelID AND m.Status='Active') 
                LEFT JOIN   unit_type ut ON (ut.UnitTypeID=p.UnitTypeID and ut.Status='Active')
                LEFT JOIN   manufacturer mfg ON (mfg.ManufacturerID=m.ManufacturerID and mfg.Status='Active')
                LEFT JOIN   repair_skill rs ON (rs.RepairSkillID=ut.RepairSkillID and rs.Status='Active')
                WHERE	    p.ClientID=:ClientID AND p.Status='Active'";
        
        $params = array ( 'ClientID' => $clientID );
        
        if ($productNo != null) {
            $sql .= ' AND ProductNo=:ProductNo';
            $params['ProductNo'] = $productNo;
        }
        
        $result = $this->Query( $this->conn, $sql, $params );
        
        if (count($result) == 0)
            return false;
        
        if (count($result) == 1)
            return $result[0];
                
        return $result;
    }
    
    
    
    /*
     * Getting product details. in the future this would require jobs details too
     */
    public function detail( $args ){
        if(isset($args[1]) && isset($this->testData[$args[1]])){
            return $this->testData[$args[1]];
        }else{
            return false;
        }
    }

    public function fetch( $args ) {
       
        $ProductID = isset($args[1])?$args[1]:0;
        
        /*$this->controller->log('product -> fetch : ' . var_export($args, true));
        
        $table = 'catalogue, product';
        $columns = $this->dbColumns;
                
        $args['where'] = (isset($args['?pk']) ? 'ProductID = ' . $args['?pk'] : '');
        $args['dbColumns'] = $this->dbColumns;
        
        
        $data = $this->ServeDataTables($this->conn, $table, $columns, $args);
        
        $this->search_result = $data['aaData'][0];
        
        $this->matches = count( $data['aaData'] );

        return  $data;*/
        
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT T1.ProductNo, T2.ModelDescription, T3.UnitTypeName, T4.ManufacturerName, T2.ModelNumber FROM product AS T1 LEFT JOIN model AS T2 ON T1.ModelID=T2.ModelID LEFT JOIN unit_type AS T3 ON T1.UnitTypeID=T3.UnitTypeID LEFT JOIN manufacturer AS T4 ON T2.ManufacturerID=T4.ManufacturerID WHERE T1.ProductID=:ProductID AND T1.Status=:Status AND T2.Status=:Status AND T3.Status=:Status AND T4.Status=:Status';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ProductID' => $ProductID, ':Status' => 'Active'));
        $result = $fetchQuery->fetch();
        
        return $result;
        
        
        
    }
    
    
    public function fetchAll( $args ) {
        # $this->controller->log('fields : ' . var_export($args, true));

        $columns = $this->dbColumns;
        $args['dbColumns'] = $this->dbColumns;
        
        $data = $this->ServeDataTables($this->conn, $this->tables, $columns, $args);
        
        $this->matches = count( $data['aaData'] );

        return  $data;
        
    }    
    
    public function Permissions(){ return $this->permissions; }
    
    /*
     * Currently only returns found count either standard search or advanced search
     */
    public function search( $args ){
        
        
        
         # $this->controller->log('fields : ' . var_export($args, true));

        $columns = $this->dbColumns;
        $args['dbColumns'] = $this->dbColumns;
        
        
        
        
        $args['productSearch'] = (isset($args['productSearch']))?$args['productSearch']:'';
        
        //$args['where'] = "ProductNo LIKE ".$this->conn->quote( '%'.$args['productSearch'].'%' )." OR ModelName LIKE ".$this->conn->quote( '%'.$args['productSearch'].'%' )." OR ModelDescription LIKE ".$this->conn->quote( '%'.$args['productSearch'].'%' );
      
        
        
         if($args['productSearch'])
         {
            $searchStrLength = strlen($args['productSearch']);
            $actualSearchString = false;
            
            
            if($searchStrLength >2 && $args['productSearch'][0]=='*' && $args['productSearch'][$searchStrLength-1]=='*')
            {
                $actualSearchString = '%'.substr($args['productSearch'], 1, $searchStrLength-2).'%';
            }
            else if($searchStrLength ==2 && $args['productSearch']=='**')
            {
                 $actualSearchString = '%%';
            }
            else if($searchStrLength >1 && $args['productSearch'][0]=='*')
            {
                $actualSearchString = '%'.substr($args['productSearch'], 1);
            }
            else if($searchStrLength >1 && $args['productSearch'][$searchStrLength-1]=='*')
            {
                $actualSearchString = substr($args['productSearch'], 0, $searchStrLength-1).'%';
            }
            else if($searchStrLength ==1 && $args['productSearch']=='*')
            {
                $actualSearchString = '%%';
            }
            
            
            if($actualSearchString)
            {
                if($actualSearchString=='%%')
                {
                    
                }   
                else
                {    
                    
                    $args['where'] = "ProductNo LIKE ".$this->conn->quote( $actualSearchString )." OR ModelName LIKE ".$this->conn->quote( $actualSearchString )." OR ModelDescription LIKE ".$this->conn->quote( $actualSearchString );
         
                }    
            }    
            else
            {
               $actualSearchString = $args['productSearch'];
               $args['where'] = "ProductNo=".$this->conn->quote( $actualSearchString )." OR ModelName=".$this->conn->quote( $actualSearchString )." OR ModelDescription=".$this->conn->quote( $actualSearchString );
            }
            
            
            //$args['where'] = "t1.JobID LIKE ".$this->conn->quote( '%'.$args['jobSearch'].'%' )." OR t1.ServiceCentreJobNo LIKE ".$this->conn->quote( '%'.$args['jobSearch'].'%' )." OR t2.PostalCode LIKE ".$this->conn->quote( '%'.$args['jobSearch'].'%' )." OR t2.ContactLastName LIKE ".$this->conn->quote( '%'.$args['jobSearch'].'%' );
                
        }
        
        
        
        $NetworkID = (isset($this->user->NetworkID))?$this->user->NetworkID:0;
        $ClientID  = (isset($this->user->ClientID))?$this->user->ClientID:0;
        if($NetworkID==0 && $ClientID==0)
        {
               
            if(isset($this->user->DefaultBrandID))
            {
                $BrandID = $this->user->DefaultBrandID;

            } 
            else 
            {

                $BrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            }
            
            if($BrandID)
            { 
                $skyline_model = $this->controller->loadModel( 'Skyline' ); 
                $BrandDetails  = $skyline_model->getBrand($BrandID);
                $NetworkID     = isset($BrandDetails[0]['NetworkID'])?$BrandDetails[0]['NetworkID']:0;
                $ClientID      = isset($BrandDetails[0]['ClientID'])?$BrandDetails[0]['ClientID']:0;
            }
        }
        
        
        
        
        $extraWhereClause = '';
        $sep = '';
        if($NetworkID)
        {
            $extraWhereClause .= $sep."t1.NetworkID=".$this->conn->quote($NetworkID);
            $sep = " AND ";
        }
        if($ClientID)
        {
            $extraWhereClause .= $sep."t1.ClientID=".$this->conn->quote($ClientID);
            $sep = " AND ";
        }
        if($extraWhereClause!='')
        {
            if(isset($args['where']))
            {
                $args['where'] = $extraWhereClause." AND (".$args['where'].")";
            } 
            else
            {
                $args['where'] = $extraWhereClause;
            }
        }
        
        
        
        $data = $this->ServeDataTables($this->conn, $this->tables, $columns, $args);
        
  
        return  $data;
        
        
        /***************
        
        
        #$this->controller->log('Product model -> search :' . var_export($args, true ));
        
        //$this->super_filter = '{"'.$args['productGroup'].'" : "'.$args['productSearch'].'"}';
        
        // $this->search_type = $args['productGroup'];
        
        //$where = "WHERE ".$args['productGroup']." LIKE '%".$args['productSearch']."%'";
        //$query = "SELECT * FROM " . $this->tables . " " . $where;
        
        
       // $where = "WHERE ".$args['productGroup']." LIKE '%".$args['productSearch']."%'";
       // $query = "SELECT * FROM " . $this->tables . " " . $where;
        
        
        $sql = "SELECT * FROM  " . $this->tables . " WHERE ProductNo=:searchTerm OR ModelName=:searchTerm OR ModelDescription=:searchTerm" ;
              
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':searchTerm' => '%'.$args['productSearch'].'%'));
        
        
        //'ProductNo' => 'Product Code',
        //'ModelName' => 'Model No.',
        //'ModelDescription' => 'Description of Product'
        
      // '%".$args['productSearch']."%'
       
          
        //$query = $this->conn->query($query);
        
        
        $fetchQuery->setFetchMode(PDO::FETCH_ASSOC);     
        $this->search_result = $fetchQuery->fetchAll();
        
        $this->matches = count( $this->search_result );

        #$this->controller->log('Product model -> search :' . var_export($this->search_result, true ));
        
         * 
         * 
         */
    }
    
    public function advancedSearch( $args ){
        
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST') 
        {
        
            $tables = "product AS t1 LEFT JOIN model AS t2 ON t1.ModelID = t2.ModelID
                            LEFT JOIN manufacturer AS t3 ON t2.ManufacturerID = t3.ManufacturerID
                            LEFT JOIN unit_type AS t4 ON t1.UnitTypeID = t4.UnitTypeID";
            $args['where'] = '';
            $sep = '';
            
          
            if(isset($_POST['ProductNo']) && $_POST['ProductNo'])
            {
                $args['where'] .= "t1.ProductNo LIKE ".$this->conn->quote( '%'.$_POST['ProductNo'].'%' );
                $sep = " AND ";
            }
            if(isset($_POST['ModelDescription']) && $_POST['ModelDescription'])
            {
                $args['where'] .= $sep."t2.ModelDescription LIKE ".$this->conn->quote( '%'.$_POST['ModelDescription'].'%' );
                $sep = " AND ";
            }
            if(isset($_POST['ManufacturerID']) && $_POST['ManufacturerID'])
            {
                $args['where'] .= $sep."t3.ManufacturerID LIKE ".$this->conn->quote( '%'.$_POST['ManufacturerID'].'%' );
                $sep = " AND ";
            }
            if(isset($_POST['UnitTypeID']) && $_POST['UnitTypeID'])
            {
                $args['where'] .= $sep."t4.UnitTypeID LIKE ".$this->conn->quote( '%'.$_POST['UnitTypeID'].'%' );
                $sep = " AND ";
            }
            if(isset($_POST['ModelNumber']) && $_POST['ModelNumber'])
            {
                $args['where'] .= $sep."t2.ModelNumber LIKE ".$this->conn->quote( '%'.$_POST['ModelNumber'].'%' );
                $sep = " AND ";
            }
            if(isset($_POST['NetworkID']) && $_POST['NetworkID'])
            {
                $args['where'] .= $sep."t1.NetworkID=".$this->conn->quote($_POST['NetworkID']);
                $sep = " AND ";
            }
            if(isset($_POST['ClientID']) && $_POST['ClientID'])
            {
                $args['where'] .= $sep."t1.ClientID=".$this->conn->quote($_POST['ClientID']);
                $sep = " AND ";
            }

            $columns = array('t1.ProductID', 't1.ProductNo', 't2.ModelDescription',  't3.ManufacturerName', 't2.ModelNumber', 't4.UnitTypeName');


            $data = $this->ServeDataTables($this->conn, $tables, $columns, $args);


            return  $data;
        }
        
    }
    
    public function count(){ return $this->matches; }

    public function AFN($dbField){
        return $this->advanced_search_fields[$dbField];
    }

    public function ValidateAdvancedJobForm(){
        $raw_js = '';
        
        foreach ($this->advanced_search_fields as $key => $f){
            $raw_js .= ($raw_js == '' ? '' : ' && ') . '$("#' . $key. '").val() == "" ';
        }
        
        # $this->controller->log('JobBooking Model : ' . $raw_js);
        
        return ($raw_js == '' ? 'false' :$raw_js);
    }
    
   
    /**
     * This chck witht the configuration file if there is alternative name
     * 
     * @return void
     *
     * @author Simon Tsang s.tsang@pccsuk.com	 
     ********************************************/
    private function UpdateAlternativeFieldsNames(){
        
        #go through the config Product fields
        foreach($this->controller->config['Product'] as $key => $f){
            if( isset($this->display_columns[$key]) ){
                $this->display_columns[$key] = $f;
            }
            if( isset($this->search_fields[$key]) ){
                $this->search_fields[$key] = $f;
            }
            if( isset($this->advanced_search_fields[$key]) ){
                $this->advanced_search_fields[$key] = $f;
            }            
        }
    }    
    
    /**
     * getIdFromRmaNo
     * 
     * Returns the PriductNo from the ProductID (if the product exists) 
     * 
     * @param $pNo  The Product No
     * 
     * @return integer  The ID of the Product
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getIdFromProductNo($pNo) {
        $sql = "
                SELECT
			`ProductID`
		FROM
			`product`
		WHERE
			`ProductNo` = $pNo
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['ProductID']);                                    /* Product exists so return Product ID */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    /**
     * getProductNoFromJobId
     * 
     * Returns the Product Number from the Job ID  
     * 
     * @param $pId  The Product ID
     * 
     * @return integer  The ID of the Service Centre
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getProductNoFromId($pId) {
        $sql = "
                SELECT
			`ProductNo`
		FROM
			`product`
		WHERE
			`ProductID` = $pId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['ProductNo']);                                    /* Product exists so return Product ID */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }

    
    
    public function setClientProductTurnaround($days, $unitID, $clientID) {
	
	$q = "	UPDATE	unit_client_type 
		SET	UnitTurnaroundTime = :days 
		WHERE	UnitTypeID = :unitID AND
			ClientID = :clientID
	     ";
	if($days == "") {
	    $days = null;
	}
	$values = ["days" => $days, "unitID" => $unitID, "clientID" => $clientID];
	$result = $this->execute($this->conn, $q, $values);
	
    }
    
    
    
}

?>
