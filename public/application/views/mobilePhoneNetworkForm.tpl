
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
 <script type="text/javascript">   
     
 $(document).ready(function() {
 
    
     $( "#CountryID" ).combobox(); 
    
   }); 
   
  </script>  
    
    <div id="MNFormPanel" class="SystemAdminFormPanel" >
    
                <form id="MNForm" name="MNForm" method="post"  action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
                         <p>
                            <label class="fieldLabel" for="MobilePhoneNetworkName" >{$page['Labels']['name']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="MobilePhoneNetworkName" value="{$datarow.MobilePhoneNetworkName|escape:'html'}" id="MobilePhoneNetworkName" >
                        
                         </p>
                         
                          
                          <p>
                            <label class="fieldLabel" for="CountryID" >{$page['Labels']['country']|escape:'html'}:</label>

                            &nbsp;&nbsp; <select name="CountryID" id="CountryID" class="text" >
                                    <option value=""  {if $datarow.CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $CountriesList as $Country}

                                   
                                        <option value="{$Country.CountryID}"   {if $datarow.CountryID eq $Country.CountryID}selected="selected"{/if}>{$Country.Name|escape:'html'}</option>
                                   
                                    {/foreach}

                                </select>

                          </p>
                          
                          
                          
                           <p>
                            <label class="fieldLabel" for="PhoneNetworkType" >{$page['Labels']['type']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                

                                 <input  type="radio" name="PhoneNetworkType"  value="Operator" {if $datarow.PhoneNetworkType neq 'MVNO'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['operator']|escape:'html'}</span> 
                                 <input  type="radio" name="PhoneNetworkType"  value="MVNO" {if $datarow.PhoneNetworkType eq 'MVNO'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['mvno']|escape:'html'}</span> 
   
                                    


                          </p>
                          
                          
                          
                          
                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    


                          </p>
                         
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="MobilePhoneNetworkID"  value="{$datarow.MobilePhoneNetworkID|escape:'html'}" >
                                   
                                    {if $datarow.MobilePhoneNetworkID neq '' && $datarow.MobilePhoneNetworkID neq '0'}
                                        
                                        <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        <br>
                                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
