{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Brands}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
        <style type="text/css" >
            .ui-combobox-input {
                 width:300px;
             }  
        </style>
    {/block}
    {block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
    <script type="text/javascript" src="{$_subdomain}/js/ajaxfileupload.js"></script>


    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
                    
                 
    var $clients = new Array();
       
        {foreach from=$allNetworkClients item=nClient key=networkId}
           
           $clients["{$networkId}"] = [        
            
            {foreach from=$nClient item=client} 
                   ["{$client.ClientID}", "{$client.ClientName|escape:'html'}"],
            {/foreach}
                
           ]
           
        {/foreach}              
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    function inactiveRow(nRow, aData)
    {
          
        if (aData[3]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(3)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
            $('td:eq(3)', nRow).html( $statuses[0][0] );
        }
    }
    
   
   
    function ajaxFileUpload()
    {
           
            
            $.ajaxFileUpload
            (
                    {
                            url:'{$_subdomain}/OrganisationSetup/brands/uploadImage/'+urlencode($("#UploadedBrandLogo").val()),
                            secureuri:false,
                            fileElementId:'BrandLogo',
                            dataType: 'json',
                            data:{ name:'logan', id:'id' }
                            /*
                            ,
                            success: function (data, status)
                            {
                                    if(typeof(data.error) != 'undefined')
                                    {
                                            if(data.error != '')
                                            {
                                                    alert(data.error);
                                                   // return false;
                                            }
                                            else
                                            {
                                                //  alert(data.msg);
                                                 //$("#UploadedBrandLogo").val(data.msg);
                                                // alert($("#UploadedBrandLogo").val());
                                                // return true;
                                                   // alert(data.msg);
                                            }
                                    }
                            },
                            error: function (data, status, e)
                            {
                                    alert(e);
                                    //return false;
                            }*/
                    }
            )

            return true;

    }
    
   
 

    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                if($("#nId").val() != "") {
                    $(location).attr('href', '{$_subdomain}/OrganisationSetup/brands/'+urlencode($("#nId").val()));
                }
            }
        });
        $("#cId").combobox({
            change: function() {
                if($("#cId").val() != "") {
                    $(location).attr('href', '{$_subdomain}/OrganisationSetup/brands/'+urlencode($("#nId").val())+'/'+urlencode($("#cId").val()));
                }
            }
        });



                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/organisationSetup');

                                });




                     /* Add a change handler to the network dropdown - strats here*/
                        /*$(document).on('change', '#nId', 
                            function() {

                                $(location).attr('href', '{$_subdomain}/OrganisationSetup/brands/'+urlencode($("#nId").val())); 
                            }      
                        );*/
                      /* Add a change handler to the network dropdown - ends here*/
              

                      /* Add a change handler to the client dropdown - strats here*/
                        /*$(document).on('change', '#cId', 
                            function() {

                                $(location).attr('href', '{$_subdomain}/OrganisationSetup/brands/'+urlencode($("#nId").val())+'/'+urlencode($("#cId").val())); 
                            }      
                        );*/
                    /* Add a change handler to the client dropdown - ends here*/
               
                       
                      
                   //change handler for network dropdown box.
                   /*$(document).on('change', '#NetworkID', 
                                function() {
                                    
                                  
                                    $clientDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                                
                                    var $NetworkID = $("#NetworkID").val();
                                
                                    if($NetworkID && $NetworkID!='')
                                        {
                                            $networkClients = $clients[$NetworkID];
                                           
                                            if($networkClients)
                                                {
                                                    for(var $i=0;$i<$networkClients.length;$i++)
                                                    {
                                                       
                                                        $clientDropDownList += '<option value="'+$networkClients[$i][0]+'" >'+$networkClients[$i][1]+'</option>';
                                                    }
                                                }
                                                
                                          $("#ClientID").html($clientDropDownList);     
                                        }
                                    

                                });*/          

                                
                                


                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );     
                        
                        
                      $(document).on('click', ".TrackerRadioButton", function() { 
               
                 
                            if ($(this).is(':checked') && $(this).val()=='Tracker') 
                            {
                               $("#TrackerURLElement").show();
                            } 
                            else 
                            {
                               $("#TrackerURLElement").hide();

                            } 


                    });   
                        



                     {if $SupderAdmin eq true} 

                        var  displayButtons = "UA";
                     
                     {else}

                         var displayButtons =  "U";

                     {/if} 
                     


                    
                    $('#BrandsResults').PCCSDataTable( {
                              "aoColumns": [ 
                                                        /* BrandID */  null,    
                                                        /* BrandName */   null,
                                                        /* Acronym */   null,
                                                        /* Status */  null
                                                ],
                            
                            "aaSorting": [[ 1, "asc" ]],
                            
                            
                            
                               
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/OrganisationSetup/brands/insert/',
                            createDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/Brands/',
                            createFormFocusElementId:'NetworkID',
                            formInsertButton:'insert_save_btn',
                            
                            
                            frmErrorRules:   {
                                                    
                                                    NetworkID:
                                                        {
                                                            required: true
                                                        },
                                                    ClientID:
                                                        {
                                                            required: true
                                                        },
                                                    BrandName:
                                                        {
                                                            required: true
                                                        },
                                                    Acronym:		
                                                        { 
                                                            required: true 
                                                        },                
                                                    BrandLogo:
                                                        {
                                                            accept: "gif|png|jpg|jpeg|bmp"
                                                            
                                                        },
                                                   TrackerURL:		
                                                        { 
                                                            required: function (element) {
                                                                if($("#TrackerEmailType").is(':checked'))
                                                                {
                                                                    
                                                                    return true;
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    return false;
                                                                }
                                                            } 
                                                        }             
                                                        
                                                        
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    NetworkID:
                                                        {
                                                            required: "{$page['Errors']['service_network']|escape:'html'}"
                                                        },
                                                    ClientID:
                                                        {
                                                            required: "{$page['Errors']['client']|escape:'html'}"
                                                        },
                                                    BrandName:
                                                        {
                                                            required: "{$page['Errors']['name']|escape:'html'}"
                                                        },
                                                    Acronym:		
                                                        { 
                                                            required: "{$page['Errors']['acronym']|escape:'html'}" 
                                                        },    
                                                    BrandLogo:
                                                        {
                                                            accept: "{$page['Errors']['logo']|escape:'html'}"
                                                        },
                                                    TrackerURL:		
                                                       { 
                                                           required: "{$page['Errors']['tracker_url']|escape:'html'}" 
                                                       }            
                                                     
                                              },                     
                            
                            popUpFormWidth:  720,
                            popUpFormHeight: 430,
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/OrganisationSetup/brands/update/',
                            updateDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/Brands/',
                            formUpdateButton:'update_save_btn',
                            updateFormFocusElementId:'NetworkID',
                            
                            colorboxFormId:  "BrandsForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'BrandsResultsPanel',
                            htmlTableId:     'BrandsResults',
                            fetchDataUrl:    '{$_subdomain}/OrganisationSetup/ProcessData/Brands/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$cId}"),
                            formCancelButton:'cancel_btn',
                            dblclickCallbackMethod: 'gotoEditPage',
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            sDom: 'ft<"#dataTables_command">rpli',
                            bottomButtonsDivId:'dataTables_command',
                            insertBeforeSendMethod: "ajaxFileUpload",
                            updateBeforeSendMethod: "ajaxFileUpload"


                        });
                      



                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/organisationSetup" >{$page['Text']['organisation_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="BrandsTopForm" name="BrandsTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="BrandsResultsPanel" >
                    
                    {if $SupderAdmin eq true} 
                        <form id="nIdForm" class="nidCorrections">
                        {$page['Labels']['service_network_label']|escape:'html'}
                        <select name="nId" id="nId" >
                            <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                            {foreach $networks as $network}

                                <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        <br /><span style="padding-right: 59px;">{$page['Labels']['client_label']|escape:'html'}</span>
                        <select name="cId" id="cId" >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $nClients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        </form>
                    {/if} 
                    
                    <table id="BrandsResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        <th width="20%" title="{$page['Text']['brandid']|escape:'html'}" >{$page['Text']['brandid']|escape:'html'}</th> 
                                        <th width="52%" title="{$page['Text']['name']|escape:'html'}" >{$page['Text']['name']|escape:'html'}</th>
                                        <th width="8%" title="{$page['Text']['acronym']|escape:'html'}" >{$page['Text']['acronym']|escape:'html'}</th>
                                        <th width="20%" title="{$page['Text']['status']|escape:'html'}"  >{$page['Text']['status']|escape:'html'}</th>
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


               

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                        
                        



{/block}



