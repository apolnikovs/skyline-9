<?php

/**
 * APISettings.class.php
 * 
 * Database access routines for the Products API
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.0
 */

require_once('CustomModel.class.php');

class APIProducts extends CustomModel {
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
    
    /**
      * Description
      * 
      * Skyline API  Skyline.GetProductDetails
      * API Spec Section 1.1.4.16
      *  
      * Download trade account settings
      * 
      * @param $ClientId     ClientID (should be got from authorisation)                 
      * 
      * @return getSettings   Associative array containing recordset
      * 
      * @author Andrew Williams <a.williams@pccsuk.com>  
      */
    
    public function getProductDetails($UserID, $ProductNo) {
        $sql = "
                SELECT 
			p.`ProductNo` AS `ProductNumber`,
			mdl.`ModelNumber` AS `ModelNumber`,
			mft.`ManufacturerName` AS `ManufacturerName`,
			ut.`UnitTypeName` AS `UnitType`
		FROM
			`client_branch` cb,	
			`manufacturer` mft,
			`model` mdl, 
			`product` p,
			`user` u,
			`unit_type` ut
		WHERE
			p.`ClientID` = cb.`ClientID`				-- Client has branch
			  AND cb.`BranchID` = u.`BranchID`			-- Branch is Branch user
			AND p.`ModelID` = mdl.`ModelID`				-- Product has model
			  AND mdl.`ManufacturerID` = mft.`ManufacturerID`	-- Model has manufacturer
			  AND mdl.`UnitTypeID` = ut.`UnitTypeID`		-- Model has unit type
                        AND u.`UserID` = $UserID
			AND p.`ProductNo` = '$ProductNo'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }

}

?>