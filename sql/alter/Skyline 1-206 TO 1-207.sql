# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.206');

# ---------------------------------------------------------------------- #
# Modify table "audit_new"                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE audit_new CHANGE COLUMN Value OldValue TEXT NULL AFTER FieldName, ADD COLUMN NewValue TEXT NULL AFTER OldValue;




# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.207');
