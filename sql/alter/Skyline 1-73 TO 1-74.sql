# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-08-23 16:45                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.73');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` DROP FOREIGN KEY `county_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `country_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `user_TO_service_provider`;

ALTER TABLE `job` DROP FOREIGN KEY `network_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `branch_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `client_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `customer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `product_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `manufacturer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `job_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `status_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `model_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job_ModifiedUser`;

ALTER TABLE `customer` DROP FOREIGN KEY `county_TO_customer`;

ALTER TABLE `customer` DROP FOREIGN KEY `country_TO_customer`;

ALTER TABLE `customer` DROP FOREIGN KEY `customer_title_TO_customer`;

ALTER TABLE `completion_status` DROP FOREIGN KEY `network_TO_completion_status`;

ALTER TABLE `completion_status` DROP FOREIGN KEY `user_TO_completion_status`;

ALTER TABLE `network_service_provider` DROP FOREIGN KEY `service_provider_TO_network_service_provider`;

ALTER TABLE `user` DROP FOREIGN KEY `service_provider_TO_user`;

ALTER TABLE `audit` DROP FOREIGN KEY `job_TO_audit`;

ALTER TABLE `part` DROP FOREIGN KEY `job_TO_part`;

ALTER TABLE `appointment` DROP FOREIGN KEY `job_TO_appointment`;

ALTER TABLE `contact_history` DROP FOREIGN KEY `job_TO_contact_history`;

ALTER TABLE `unit_pricing_structure` DROP FOREIGN KEY `completion_status_TO_unit_pricing_structure`;

ALTER TABLE `status_history` DROP FOREIGN KEY `job_TO_status_history`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `service_provider_TO_central_service_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `service_provider_TO_town_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `customer_TO_town_allocation`;

ALTER TABLE `client` DROP FOREIGN KEY `service_provider_TO_client`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `service_provider_TO_postcode_allocation`;

# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD COLUMN `AdminSupervisor` VARCHAR(40);

ALTER TABLE `service_provider` ADD COLUMN `ServiceManager` VARCHAR(40);

ALTER TABLE `service_provider` ADD COLUMN `Accounts` VARCHAR(40);

ALTER TABLE `service_provider` MODIFY `AdminSupervisor` VARCHAR(40) AFTER `ContactEmail`;

ALTER TABLE `service_provider` MODIFY `ServiceManager` VARCHAR(40) AFTER `AdminSupervisor`;

ALTER TABLE `service_provider` MODIFY `Accounts` VARCHAR(40) AFTER `ServiceManager`;

# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` ADD COLUMN `WarrantyNotes` VARCHAR(50);

ALTER TABLE `job` ADD COLUMN `NewFirmwareVersion` VARCHAR(20);

ALTER TABLE `job` ADD COLUMN `ClaimBillNumber` VARCHAR(15);

# ---------------------------------------------------------------------- #
# Modify table "customer"                                                #
# ---------------------------------------------------------------------- #

ALTER TABLE `customer` ADD COLUMN `Password` VARCHAR(256);

ALTER TABLE `customer` ADD COLUMN `Username` VARCHAR(100);

ALTER TABLE `customer` ADD COLUMN `LastLoggedIn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `customer` ADD COLUMN `SecurityQuestionID` INTEGER NOT NULL;

ALTER TABLE `customer` ADD COLUMN `SecurityQuestionAnswer` VARCHAR(256) NOT NULL;

ALTER TABLE `customer` ADD COLUMN `EmailConfirmed` TINYINT(1) NOT NULL DEFAULT 0;

ALTER TABLE `customer` ADD COLUMN `EmailAuthKey` VARCHAR(256);

# ---------------------------------------------------------------------- #
# Modify table "completion_status"                                       #
# ---------------------------------------------------------------------- #

DROP INDEX `IDX_completion_status_NetworkID_FK` ON `completion_status`;

ALTER TABLE `completion_status` DROP COLUMN `NetworkID`;

ALTER TABLE `completion_status` DROP COLUMN `CompletionStatusName`;

ALTER TABLE `completion_status` ADD COLUMN `CompletionStatus` VARCHAR(20);

ALTER TABLE `completion_status` ADD COLUMN `Description` VARCHAR(50);

ALTER TABLE `completion_status` ADD COLUMN `Sort` INTEGER;

ALTER TABLE `completion_status` ADD COLUMN `BrandID` INTEGER;

ALTER TABLE `completion_status` MODIFY `CompletionStatus` VARCHAR(20) AFTER `CompletionStatusID`;

ALTER TABLE `completion_status` MODIFY `Description` VARCHAR(50) AFTER `CompletionStatus`;

ALTER TABLE `completion_status` MODIFY `Sort` INTEGER AFTER `Description`;

ALTER TABLE `completion_status` MODIFY `BrandID` INTEGER AFTER `Sort`;

CREATE INDEX `IDX_completion_status_BrandID_FK` ON `completion_status` (`BrandID`);

# ---------------------------------------------------------------------- #
# Add table "samsung_claim_response"                                     #
# ---------------------------------------------------------------------- #

CREATE TABLE `samsung_claim_response` (
    `SamsungClaimResponseID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER NOT NULL,
    `ErrorCode` VARCHAR(18),
    `ErrorDescription` VARCHAR(50),
    CONSTRAINT `samsung_claim_responseID` PRIMARY KEY (`SamsungClaimResponseID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_samsung_claim_response_JobID_FK` ON `samsung_claim_response` (`JobID`);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD CONSTRAINT `county_TO_service_provider` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `country_TO_service_provider` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `user_TO_service_provider` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `network_TO_job` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `job` ADD CONSTRAINT `branch_TO_job` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `job` ADD CONSTRAINT `product_TO_job` 
    FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`);

ALTER TABLE `job` ADD CONSTRAINT `service_type_TO_job` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `manufacturer_TO_job` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `job` ADD CONSTRAINT `job_type_TO_job` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `status_TO_job` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `job` ADD CONSTRAINT `model_TO_job` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job` 
    FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job_ModifiedUser` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `customer` ADD CONSTRAINT `county_TO_customer` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `customer` ADD CONSTRAINT `country_TO_customer` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `customer` ADD CONSTRAINT `customer_title_TO_customer` 
    FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`);

ALTER TABLE `completion_status` ADD CONSTRAINT `user_TO_completion_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `completion_status` ADD CONSTRAINT `brand_TO_completion_status` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `samsung_claim_response` ADD CONSTRAINT `job_TO_samsung_claim_response` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `service_provider_TO_network_service_provider` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `user` ADD CONSTRAINT `service_provider_TO_user` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `audit` ADD CONSTRAINT `job_TO_audit` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `part` ADD CONSTRAINT `job_TO_part` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `job_TO_appointment` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `contact_history` ADD CONSTRAINT `job_TO_contact_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `completion_status_TO_unit_pricing_structure` 
    FOREIGN KEY (`CompletionStatusID`) REFERENCES `completion_status` (`CompletionStatusID`);

ALTER TABLE `status_history` ADD CONSTRAINT `job_TO_status_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `service_provider_TO_central_service_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `service_provider_TO_town_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `customer_TO_town_allocation` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `client` ADD CONSTRAINT `service_provider_TO_client` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `service_provider_TO_postcode_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.74');
